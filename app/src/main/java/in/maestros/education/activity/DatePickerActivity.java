package in.maestros.education.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import in.maestros.education.R;

public class DatePickerActivity extends Activity {

    DatePicker picker;
    Button sel_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_picker);

        picker=(DatePicker)findViewById(R.id.datePicker1);
        sel_date = (Button)findViewById(R.id.sel_date);
        sel_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int d = picker.getDayOfMonth();
                int m = picker.getMonth()+1;
                int yr = picker.getYear();

                String reslt = d+"/"+m+"/"+yr;
                StProfileActivity.et_dob.setText(reslt);
                finish();
            }
        });




    }
}
