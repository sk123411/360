package in.maestros.education.activity;

/**
 * Created by Ravi on 29/07/15.
 */
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;


public class FriendsFragment22 extends Fragment {

    private LinearLayoutManager lLayout;

    private WebView wv1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home1, container, false);

      /*  List<TechItemObject> rowListItem = getAllItemList();
        lLayout = new LinearLayoutManager(getActivity());

        RecyclerView rView = (RecyclerView)rootView.findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        TechMainActivityRecyclerViewAdapter rcAdapter = new TechMainActivityRecyclerViewAdapter(getActivity(), rowListItem);
        rView.setAdapter(rcAdapter);*/

        wv1=(WebView)rootView.findViewById(R.id.webView);
        wv1.setWebViewClient(new MyBrowser());
        String url = HttpUrlPath.tc;

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.loadUrl(url);

        return rootView;
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    private List<TechItemObject> getAllItemList(){

        List<TechItemObject> allItems = new ArrayList<TechItemObject>();
        allItems.add(new TechItemObject("",R.drawable.dd1));
        allItems.add(new TechItemObject("",R.drawable.dd2));
        allItems.add(new TechItemObject("",R.drawable.dd3));
        allItems.add(new TechItemObject("",R.drawable.dd1));
        allItems.add(new TechItemObject("",R.drawable.dd2));
        allItems.add(new TechItemObject("",R.drawable.dd3));


        return allItems;
    }
}
