package in.maestros.education.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

public class SignupActivity extends AppCompatActivity {
    List<String> categories = new ArrayList<String>();
    List<String> categories1 = new ArrayList<String>();
    static JSONArray contactsstate1 = null;
    Button register;
    EditText password, email, lname, fname,password56;
    Spinner ss,spin;
    String s_ss, s_password, s_email, s_lname, Name11,school_name,str_spin,FirstName;
    ImageView back;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);













        Toast.makeText(this, s_ss, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, str_spin, Toast.LENGTH_SHORT).show();

        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        progressBar.setVisibility(View.GONE);

        fname = (EditText) findViewById(R.id.fname);
        lname = (EditText) findViewById(R.id.lname);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        password56 = (EditText) findViewById(R.id.password56);

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        password.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                if (password.getText().toString().length() == 0) {
                    password.setError("Enter your password..!");
                } else if (password.getText().toString().length() < 5) {
                    password.setError("weak..!");
                } else if (password.getText().toString().length() > 5 && password.getText().toString().length() < 8) {
                    password.setError("medium..!");
                } else if (password.getText().toString().length() > 5 && password.getText().toString().length() >= 8) {

                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

        });


        ss = (Spinner) findViewById(R.id.ss);
        spin = (Spinner) findViewById(R.id.spin);


        if (NetConnection.isConnected(SignupActivity.this)) {

            SignUpjsontask11 task = new SignUpjsontask11();
            task.execute();

        } else {

            Toast.makeText(SignupActivity.this, "Internet connection error", Toast.LENGTH_LONG).show();
        }




        if(NetConnection.isConnected(SignupActivity.this)){
            SignUpjsontask12 task = new SignUpjsontask12();
            task.execute();
        }else {

            Toast.makeText(SignupActivity.this, "Internet connection error", Toast.LENGTH_SHORT).show();
        }


      /*  categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");*/

        // Creating adapter for spinner

        ss.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                String item = adapterView.getItemAtPosition(i).toString();

                // Showing selected spinner item
                Toast.makeText(SignupActivity.this, "Selected: " + item, Toast.LENGTH_LONG).show();

                s_ss = categories.get(i);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                String item = adapterView.getItemAtPosition(i).toString();

                // Showing selected spinner item
                Toast.makeText(SignupActivity.this, "Selected: " + item, Toast.LENGTH_LONG).show();

                str_spin = categories1.get(i);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Name11 = fname.getText().toString().trim();
                s_lname = lname.getText().toString().trim();
                s_email = email.getText().toString().trim();
                s_password = password.getText().toString().trim();
                school_name = password56.getText().toString().trim();


                boolean isError = false;

                if (Name11.equals("")) {
                    isError = true;
                    fname.requestFocus();
                    Toast.makeText(SignupActivity.this, "Please enter first name", Toast.LENGTH_SHORT).show();

                } else if (s_lname.equals("")) {
                    isError = true;
                    lname.requestFocus();
                    Toast.makeText(SignupActivity.this, "Please enter last name", Toast.LENGTH_SHORT).show();
                } else if (s_email.equals("")) {
                    isError = true;
                    email.requestFocus();
                    Toast.makeText(SignupActivity.this, "Please Re_enter email", Toast.LENGTH_SHORT).show();
                } else if (s_password.equals("")) {
                    isError = true;
                    password.requestFocus();
                    Toast.makeText(SignupActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                } else if (s_ss.equals("")) {
                    isError = true;
                    ss.requestFocus();
                    Toast.makeText(SignupActivity.this, "Please enter class", Toast.LENGTH_SHORT).show();
                } else if (str_spin.equals("")) {
                    isError = true;
                    spin.requestFocus();
                    Toast.makeText(SignupActivity.this, "Please enter school", Toast.LENGTH_SHORT).show();
                }else if (!isError) {
                    if (NetConnection.isConnected(SignupActivity.this)) {
                        progressBar.setVisibility(View.VISIBLE);
                        SignUpjsontask task = new SignUpjsontask();
                        task.execute();

                    } else {

                        Toast.makeText(SignupActivity.this, "Internet connection error", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }

    public class SignUpjsontask extends AsyncTask<String, Void, String> {

        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result = "";
        String u_email;
        String u_pass;
        String u_lat;
        String u_lon;
        String u_name;

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);
        }

        protected String doInBackground(String... arg0) {

            HttpClient htppclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(HttpUrlPath.urlPath + HttpUrlPath.actionSignup);
            try {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();


                nameValuePair.add(new BasicNameValuePair("fname", Name11));
                nameValuePair.add(new BasicNameValuePair("lname", s_lname));
                nameValuePair.add(new BasicNameValuePair("email", s_email));
                nameValuePair.add(new BasicNameValuePair("password", s_password));
                nameValuePair.add(new BasicNameValuePair("school_id", str_spin));


                nameValuePair.add(new BasicNameValuePair("class", s_ss));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));


                HttpResponse response = htppclient.execute(httppost);
                String object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);
                JSONObject js = new JSONObject(object);

                if (js.has("user_id")) {
                    UserRegisterID = js.getString("user_id");
                    //u_name = js.getString("username");
                    // u_email = js.getString("email");
                    //u_pass = js.getString("password");

                    Errormessage = js.getString("result");
                    //user_mobile=js.getString("number");

                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }


        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            // loader.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            //Toast.makeText(Register.this,UserRegisterID+ "rohit "+Errormessage,Toast.LENGTH_LONG).show();
            if (!IsError) {

                if (Errormessage.equals("successful")) {

                    if (UserRegisterID != null) {
                        if (!UserRegisterID.equals("")) {


                            //  Toast.makeText(SignupActivity.this, "Your login has been "+Errormessage,Toast.LENGTH_LONG).show();


                            // Intent intent=new Intent(SignupActivity.this, LoginActivity.class);
                            //  startActivity(intent);
                            // finish();

                            //----------------------start AlertDialog


                            AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
                            //Uncomment the below code to Set the message and title from the strings.xml file
                            //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);

                            //Setting message manually and performing action on button click
                            // builder.setTitle("360 Educating");
                            builder.setMessage("Your Registration have done\nActivate from register email box")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //  Action for 'NO' Button
                                            finish();
                                        }
                                    });

                            //Creating dialog box
                            AlertDialog alert = builder.create();
                            //Setting the title manually
                            alert.setTitle("Alert");
                            alert.show();


                            //----------------------end AlertDialog


                        }

                    }
                } else {
                    Toast.makeText(SignupActivity.this, Errormessage, Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(SignupActivity.this, "please try again...", Toast.LENGTH_SHORT).show();


            }
        }

    }


    //-------------------------start------------------------


    public class SignUpjsontask11 extends AsyncTask<String, Void, String> {


        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result = "";
        String p_user_login;
        String p_user_nicename;
        String p_display_name;
        String p_password;
        String s_latitude;
        String a1 = "", a2 = "", a3 = "", a4 = "", a5 = "";
        String object;

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);


        }

        protected String doInBackground(String... arg0) {

            HttpClient htppclient = new DefaultHttpClient();
            //HttpPost httppost=new HttpPost("http://maestros.co.in/WORDPRESS/jolly_shop/webservice/?action=login");
            //  HttpPost httppost=new HttpPost("http://maestrossoftware.com/WEBSOFT/education/Android/process.php?action=classes");
            HttpPost httppost = new HttpPost(HttpUrlPath.urlPath + HttpUrlPath.actionClasses);
            try {

                HttpResponse response = htppclient.execute(httppost);
                object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);

                contactsstate1 = new JSONArray(object);

                for (int i = 0; i < contactsstate1.length(); i++) {
                    JSONObject c = contactsstate1.getJSONObject(i);
                    a1 = c.getString("id");
                    a2 = c.getString("classname");
                    categories.add(a2);



                }
               /* JSONObject js = new JSONObject(object);
                if (js.has("ID")) {
                    UserRegisterID = js.getString("ID");
                    p_user_login = js.getString("user_login");
                    p_user_nicename = js.getString("user_nicename");
                    p_display_name = js.getString("display_name");
                    Errormessage = js.getString("result");
                   // p_mobilenumber = js.getString("number");
                   // s_latitude=js.getString("latitude");
                    //s_longitude=js.getString("longitude");

                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }*/

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            // if (pDialog.isShowing())
            //  pDialog.dismiss();
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignupActivity.this, android.R.layout.simple_spinner_item, categories);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            ss.setAdapter(dataAdapter);
            //Toast.makeText(getActivity(),""+object,Toast.LENGTH_LONG).show();

            // adapter=new DynamicListviewAdapter(getActivity());
            // lv.setAdapter(adapter);

            // loader.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            //Toast.makeText(getActivity(),a3+a4+a5,Toast.LENGTH_LONG).show();
         /*   if(!IsError){


                if(a1.equals("Y")){


                    if (a2.equals("success")) {

                        Toast.makeText(getActivity(),"login has been "+a2,Toast.LENGTH_SHORT).show();


                        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                        editor.putString(AppsContants.USER_ID, a3);
                        editor.putString(AppsContants.USER_LOGIN, a4);
                        editor.putString(AppsContants.NICENAME, a5);
                        editor.putString(AppsContants.DISPLAYNAME, a5);
                        editor.commit();
                        Intent i=new Intent(getActivity(),MainActivitynavigation.class);
                        startActivity(i);
                        getActivity().finish();
                    }


                }else {
                    Toast.makeText(getActivity(), a2,Toast.LENGTH_SHORT).show();


                }


            }else {
                Toast.makeText(getActivity(), "please try again...",Toast.LENGTH_SHORT).show();


            }*/

        }


    }


    //------------------------------end---------------------------------



    public class SignUpjsontask12 extends AsyncTask<String, Void, String> {


        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result = "";
        String p_user_login;
        String p_user_nicename;
        String p_display_name;
        String p_password;
        String s_latitude;
        String a1 = "", a2 = "", a3 = "", a4 = "", a5 = "";
        String object;

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);


        }

        protected String doInBackground(String... arg0) {

            HttpClient htppclient = new DefaultHttpClient();
            //HttpPost httppost=new HttpPost("http://maestros.co.in/WORDPRESS/jolly_shop/webservice/?action=login");
            //  HttpPost httppost=new HttpPost("http://maestrossoftware.com/WEBSOFT/education/Android/process.php?action=classes");
            HttpPost httppost = new HttpPost(HttpUrlPath.urlPath + HttpUrlPath.actionSchool);
            try {

                HttpResponse response = htppclient.execute(httppost);
                object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);

                contactsstate1 = new JSONArray(object);

                for (int i = 0; i < contactsstate1.length(); i++) {
                    JSONObject c = contactsstate1.getJSONObject(i);
                    a1 = c.getString("id");
                    a2 = c.getString("school_name");
                    categories1.add(a2);



                }
               /* JSONObject js = new JSONObject(object);
                if (js.has("ID")) {
                    UserRegisterID = js.getString("ID");
                    p_user_login = js.getString("user_login");
                    p_user_nicename = js.getString("user_nicename");
                    p_display_name = js.getString("display_name");
                    Errormessage = js.getString("result");
                   // p_mobilenumber = js.getString("number");
                   // s_latitude=js.getString("latitude");
                    //s_longitude=js.getString("longitude");

                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }*/

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            // if (pDialog.isShowing())
            //  pDialog.dismiss();
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignupActivity.this, android.R.layout.simple_spinner_item, categories1);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            spin.setAdapter(dataAdapter);
            //Toast.makeText(getActivity(),""+object,Toast.LENGTH_LONG).show();

            // adapter=new DynamicListviewAdapter(getActivity());
            // lv.setAdapter(adapter);

            // loader.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            //Toast.makeText(getActivity(),a3+a4+a5,Toast.LENGTH_LONG).show();
         /*   if(!IsError){


                if(a1.equals("Y")){


                    if (a2.equals("success")) {

                        Toast.makeText(getActivity(),"login has been "+a2,Toast.LENGTH_SHORT).show();


                        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                        editor.putString(AppsContants.USER_ID, a3);
                        editor.putString(AppsContants.USER_LOGIN, a4);
                        editor.putString(AppsContants.NICENAME, a5);
                        editor.putString(AppsContants.DISPLAYNAME, a5);
                        editor.commit();
                        Intent i=new Intent(getActivity(),MainActivitynavigation.class);
                        startActivity(i);
                        getActivity().finish();
                    }


                }else {
                    Toast.makeText(getActivity(), a2,Toast.LENGTH_SHORT).show();


                }


            }else {
                Toast.makeText(getActivity(), "please try again...",Toast.LENGTH_SHORT).show();


            }*/

        }


    }
}















































