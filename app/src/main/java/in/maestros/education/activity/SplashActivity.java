package in.maestros.education.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;


//import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.ndk.CrashlyticsNdk;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import in.maestros.education.FaceboookNewActivity;
import in.maestros.education.R;
import in.maestros.education.other.AppsContants;
//import io.fabric.sdk.android.Fabric;


public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 29;
    String Name11, Name12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_splash);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA,
                                Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.READ_CONTACTS, Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            } else {
                Log.d("Home", "Already granted access");

                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                Name11 = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
                final String sid = AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

                new Handler().postDelayed(new Runnable() {

                    public void run() {

                        {
                            if (Name11.equals("")||sid.equals("")) {

                                //  Intent i1 = new Intent(SplashActivity.this, LoginActivity.class);
                                Intent i1 = new Intent(SplashActivity.this, FaceboookNewActivity.class);
                                startActivity(i1);
                                finish();
                            } else {
                                Intent i1 = new Intent(SplashActivity.this, StudentDashBoard.class);
                                startActivity(i1);
                                finish();
                            }

                        }

                    }

                }, SPLASH_TIME_OUT);

                //initializeView(v);
            }
        } else {
            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
            Name11 = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
            final String sid = AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

            new Handler().postDelayed(new Runnable() {

                public void run() {

                    {
                        if (Name11.equals("")||sid.equals("")) {

                            //  Intent i1 = new Intent(SplashActivity.this, LoginActivity.class);
                            Intent i1 = new Intent(SplashActivity.this, FaceboookNewActivity.class);
                            startActivity(i1);
                            finish();
                        } else {
                            Intent i1 = new Intent(SplashActivity.this, StudentDashBoard.class);
                            startActivity(i1);
                            finish();
                        }

                    }

                }

            }, SPLASH_TIME_OUT);

        }

    }
        @Override
        public void onRequestPermissionsResult ( int requestCode, String permissions[],
        @NonNull int[] grantResults){
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                        Log.d("Home", "Permission Granted");

                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        Name11 = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");

                        new Handler().postDelayed(new Runnable() {

                            public void run() {

                                {
                                    if (Name11.equals("")) {

                                        //  Intent i1 = new Intent(SplashActivity.this, LoginActivity.class);
                                        Intent i1 = new Intent(SplashActivity.this, FaceboookNewActivity.class);
                                        startActivity(i1);
                                        finish();
                                    } else {
                                        Intent i1 = new Intent(SplashActivity.this, StudentDashBoard.class);
                                        startActivity(i1);
                                        finish();
                                    }

                                }

                            }

                        }, SPLASH_TIME_OUT);

                        //  initializeView(v);
                    } else {
                        Log.d("Home", "Permission Failed");
                        Toast.makeText(SplashActivity.this, "You must allow permission record audio to your mobile device.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }


                // Add additional cases for other permissions you may have asked for
            }
        }


    }

