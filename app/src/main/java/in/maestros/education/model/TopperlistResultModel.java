package in.maestros.education.model;

/**
 * Created by SONU on 25/09/15.
 */
public class TopperlistResultModel {

    // Getter and Setter model for recycler view items
    private String user_id;
    private String fname;
    private String lname;
    private String classs;
    private String test_time;
    private String score;




    public TopperlistResultModel(String user_id, String fname, String lname, String classs, String test_time, String score) {


        this.user_id=user_id;
        this.fname = fname;
        this.lname=lname;
        this.classs=classs;
        this.test_time=test_time;
        this.score=score;

    }
    public void setuser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getuser_id() {
        return user_id;
    }

    public void setfname(String fname) {
        this.fname = fname;
    }
    public String getfname() {
        return fname;
    }

    public void setlname(String lname) {
        this.lname = lname;
    }
    public String getlname() {
        return lname;
    }

    public void setclasss(String classs) {
        this.classs = classs;
    }
    public String getclasss() {
        return classs;
    }


    public void settest_time(String test_time) {
        this.test_time = test_time;
    }
    public String gettest_time() {
        return test_time;
    }
    public void setscore(String score) {
        this.score = score;
    }
    public String getscore() {
        return score;
    }

}
