package in.maestros.education.activity.StudentDeshBoard;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;
import in.maestros.education.result.ResultResponse;
import io.paperdb.Paper;

public class ResultDetailActivity extends AppCompatActivity {


    TextView studentName, className, subject,assigment,totalQuestions,correct,worng,testDate,testTime,score;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_detail);


        studentName = findViewById(R.id.studentNameText);
        className = findViewById(R.id.classText);
        subject = findViewById(R.id.subjectText);
        assigment = findViewById(R.id.assignmentText);
        totalQuestions = findViewById(R.id.questionText);
        correct = findViewById(R.id.correctAnswerText);
        worng = findViewById(R.id.wrongAnswerText);
        testDate = findViewById(R.id.testdateText);
        testTime = findViewById(R.id.testTimeText);
        score = findViewById(R.id.scoreText);

        Paper.init(getApplicationContext());

        ResultResponse resultResponse = Paper.book().read("result");






        studentName.setText(resultResponse.getFirst_name());
        className.setText(resultResponse.getClassName());
        subject.setText(resultResponse.getSubject());
        assigment.setText(resultResponse.getAssign_name());
        if (resultResponse.getTotal_qs()!=null) {
            totalQuestions.setText(resultResponse.getTotal_qs());
        }
        correct.setText(resultResponse.getCorrect_ans());
        worng.setText(resultResponse.getWrong_ans());
        testDate.setText(resultResponse.getTest_date());
        testTime.setText(resultResponse.getTest_time());
        score.setText(resultResponse.getScore());















    }


    void fetchResult(){
        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=student_view_detail_result")
                .addBodyParameter("id", AppsContants.getSharedpreferences().getString(AppsContants.L_ID, "")).build()
                .getAsObject(ResultDetailResponse.class, new ParsedRequestListener<ResultDetailResponse>() {
                    @Override
                    public void onResponse(ResultDetailResponse response) {



                        if (response!=null) {

                            studentName.setText(response.getFullName());
                            className.setText(response.getClassName());
                            subject.setText(response.getSubjectName());
                            assigment.setText(response.getAssignment());
                            if (response.getTotalQuestion()!=null) {
                                totalQuestions.setText(response.getTotalQuestion());
                            }
                            correct.setText(response.getCorrectAnswer());
                            worng.setText(response.getWrongAnswer());
                            testDate.setText(response.getTestDate());
                            testTime.setText(response.getTestTime());
                            score.setText(response.getScore());


                        }






                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }
}