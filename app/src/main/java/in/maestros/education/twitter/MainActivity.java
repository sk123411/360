package in.maestros.education.twitter;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import in.maestros.education.R;


public class MainActivity extends AppCompatActivity {

    private TwitterLoginButton loginButton;
    TextView screen_name,user_name,user_location,user_timezone,user_description;
    TwitterSession session;
    ImageView user_picture;
    String screenname,username,twitterImage,location,timeZone,description;


    // Add your Consumer Key and Consumer Secret Key generated while creating app on Twitter.
    //See "Keys and access Tokens" in your app on twitter to find these keys.
    private static final String TWITTER_KEY = "YSkMJSIBbeX9N1HMgFKPsfaMe";
    private static final String TWITTER_SECRET = "vLFUXxzSTtrw1ZMtHEVRQNZihsnXtYY4hydlk8Eoh1aLo4y0bU";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //For authentication, pass consumer key and consumer secret key
       /* TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));*/

        setContentView(R.layout.content_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
/*
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                session = Twitter.getSessionManager().getActiveSession();

                Twitter.getApiClient(session).getAccountService()
                        .verifyCredentials(true, false, new Callback<User>() {

                            @Override
                            public void success(Result<User> userResult) {

                                User user = userResult.data;

                                twitterImage = user.profileImageUrl;
                                screenname = user.screenName;
                                username = user.name;
                                location = user.location;
                                timeZone = user.timeZone;
                                description = user.description;

                                user_picture = (ImageView) findViewById(R.id.profile_pic);
                                Picasso.with(getApplicationContext()).load(twitterImage.toString())
                                        .into(user_picture);

                                screen_name = (TextView) findViewById(R.id.screen_name);
                                screen_name.setText("Username : " + screenname);

                                user_name = (TextView) findViewById(R.id.user_name);
                                user_name.setText("Name : "+username);

                                user_location = (TextView) findViewById(R.id.user_location);
                                user_location.setText("Location : "+location);

                                user_timezone = (TextView) findViewById(R.id.user_timezone);
                                user_timezone.setText("Timezone : "+timeZone);

                                user_description = (TextView) findViewById(R.id.user_description);
                                user_description.setText("Description : "+description);

                            }

                            @Override
                            public void failure(TwitterException e) {

                            }

                        });

                loginButton.setVisibility(View.GONE);


            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }


        });*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Make sure that the loginButton hears the result from any
        // Activity that it triggered.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


