package in.maestros.education.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import in.maestros.education.R;

public class TechRecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView countryName;
    public ImageView countryPhoto;

    public TechRecyclerViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        //countryName = (TextView)itemView.findViewById(R.id.tc);
        countryPhoto = (ImageView)itemView.findViewById(R.id.tc);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Country Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}