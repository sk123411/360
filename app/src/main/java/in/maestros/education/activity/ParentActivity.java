package in.maestros.education.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.ParentDeshBoard;
import in.maestros.education.other.AppsContants;

public class ParentActivity extends AppCompatActivity {

    EditText fname,lname,et_add,et_email,eml_cont;
    String s_fname,s_lname,s_et_add,s_et_email,s_eml_cont,s_state;
    Spinner state;
    Button nxt,pre;
    String L_ID="",Name11="",Name12="",Name13="";
    private ProgressDialog pDialog;
    ImageView back;

    String[] country = { "Abuja",
            "Anambra",
            " Bayelsa",
            "Benue",
           "Borno" ,
           "Cross River" ,
           "Delta" ,
           "Ebonyi" ,
           "Enugu" ,
            "Gombe",
            "Imo",
            "Jigawa",
            "Kaduna",

            "Kano",
            "Katsina",
            "Kebbi",
            "Kogi",
            "Kwara",
            "Lagos",
            "Nasarawa",
            "Niger",
            "Ogun",
            "Ondo",
            "Osun",
            "Oyo",
            "Plateau",
            "Rivers",
            "Sokoto",
            "Taraba",
            "Yobe",
            "Zamfara",
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");

        Log.e("dhgfyidj",L_ID);


        fname = (EditText) findViewById(R.id.fname);
        lname = (EditText) findViewById(R.id.lname);
        et_add = (EditText) findViewById(R.id.et_add);
        et_email = (EditText) findViewById(R.id.et_email);
        eml_cont = (EditText) findViewById(R.id.eml_cont);

        state = (Spinner) findViewById(R.id.state);

        back = (ImageView)findViewById(R.id.back);


        fname.setText(Name11);
        lname.setText(Name12);
        et_email.setText(Name13);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        state.setAdapter(aa);

        state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                s_state = adapterView.getItemAtPosition(i).toString();
               // Toast.makeText(ParentActivity.this,""+s_state,Toast.LENGTH_LONG).show();
                
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        nxt = (Button)findViewById(R.id.nxt);
        pre = (Button)findViewById(R.id.pre);



        pre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Intent i = new Intent(ParentActivity.this, TeachcerActivity.class);
                //startActivity(i);

                s_fname=fname.getText().toString().trim();
                s_lname=lname.getText().toString().trim();
                s_et_add=et_add.getText().toString().trim();
                s_et_email=et_email.getText().toString().trim();
                s_eml_cont=eml_cont.getText().toString().trim();



                // Toast.makeText(getActivity(), MainActivityForFragment.s_latitude+"Please"+MainActivityForFragment.s_longitude, Toast.LENGTH_SHORT).show();
                boolean isError = false;
                if (s_fname.equals("")) {
                    isError = true;
                    fname.requestFocus();
                    Toast.makeText(ParentActivity.this, "Please enter first name", Toast.LENGTH_SHORT).show();

                }
                else if (s_lname.equals("")) {
                    isError = true;
                    lname.requestFocus();
                    Toast.makeText(ParentActivity.this, "Please enter last name", Toast.LENGTH_SHORT).show();

                }   else if (s_et_add.equals("")) {
                    isError = true;
                    et_add.requestFocus();
                    Toast.makeText(ParentActivity.this, "Please enter address", Toast.LENGTH_SHORT).show();

                }   else if (s_et_email.equals("")) {
                    isError = true;
                    et_email.requestFocus();
                    Toast.makeText(ParentActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();

                }

                else if (s_eml_cont.equals("")) {
                    isError = true;
                    eml_cont.requestFocus();
                    Toast.makeText(ParentActivity.this, "Please enter phone number", Toast.LENGTH_SHORT).show();

                }
              /*  else if (s_state.equals("")) {
                    isError = true;
                    state.requestFocus();
                    Toast.makeText(ParentActivity.this, "Please enter state", Toast.LENGTH_SHORT).show();

                }*/

                else if(!isError){

                    if (NetConnection.isConnected(ParentActivity.this)) {
                        // progressBar.setVisibility(View.VISIBLE);
                      LoginJsonTask task = new LoginJsonTask();
                        task.execute();

                    }
                    else {
                        Toast.makeText(ParentActivity.this, "Internet connection error", Toast.LENGTH_LONG).show();
                    }
                }

startActivity(new Intent(ParentActivity.this, ParentDeshBoard.class));



            }
        });

    }

    public class LoginJsonTask extends AsyncTask<String, Void, String> {

        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result="";


        String s_email,s_cls,s_name,s_lnam,s_gender,s_mobile,s_age,s_race;

        String s_longitude;
        String object;
        String semail, spwd;

        /*public LoginJsonTask(String ss_email, String ss_pwd)
        {
            this.semail = ss_email;
            this.spwd = ss_pwd;
        }*/

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);
            pDialog = new ProgressDialog(ParentActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }
        protected String doInBackground(String... arg0) {




            DefaultHttpClient htppclient=new DefaultHttpClient();
          //  HttpPost httppost=new HttpPost("http://maestrossoftware.com/WEBSOFT/education/Android/process.php?action=parents_profile");
            HttpPost httppost=new HttpPost(HttpUrlPath.urlPath+HttpUrlPath.actionParentsProfile);
            try{
                List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("user_id",L_ID));
                nameValuePair.add(new BasicNameValuePair("p_fname",s_fname));
                 nameValuePair.add(new BasicNameValuePair("p_lname",s_lname ));
                nameValuePair.add(new BasicNameValuePair("state",s_state));
                nameValuePair.add(new BasicNameValuePair("address",s_et_add));
                nameValuePair.add(new BasicNameValuePair("email1",s_et_email));
                nameValuePair.add(new BasicNameValuePair("phno",s_eml_cont));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                HttpResponse response = htppclient.execute(httppost);
                object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);
                JSONObject js = new JSONObject(object);


                // String s_email,s_gender,s_password,s_firstname,s_lastname,s_dob,s_mobile,s_country,s_city;



                if (js.has("user_id")) {
                    UserRegisterID = js.getString("user_id");
                    //s_name = js.getString("fname");
                    //s_lnam=js.getString("lname");
                    //s_email=js.getString("email");
                    //s_cls=js.getString("class");


                    //s_city=js.getString("city");
                    Errormessage = js.getString("result");


                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                    Log.e("iuyfjdfkjd",js.getString("result"));

                }

            }
            catch (Exception e){
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }



        protected void onPostExecute(String result1){
            super.onPostExecute(result1);
            if (pDialog.isShowing())
                pDialog.dismiss();
            // loader.setVisibility(View.GONE);
            //progressBar.setVisibility(View.GONE);
            //Toast.makeText(getActivity(),"f_n="+s_dob,Toast.LENGTH_LONG).show();
            if(!IsError){


                if(Errormessage.equals("success")){

                    if (UserRegisterID != null) {
                        if (!UserRegisterID.equals("")) {

                            Toast.makeText(ParentActivity.this,""+Errormessage,Toast.LENGTH_SHORT).show();

                            /*AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                            editor.putString(AppsContants.L_ID, UserRegisterID);
                            editor.putString(AppsContants.USER_NAME, s_name);
                            editor.putString(AppsContants.LNAME, s_lnam);
                            editor.putString(AppsContants.EMAIL, s_email);
                            editor.putString(AppsContants.CLASS, s_cls);
                            editor.commit();*/

                            Intent i= new Intent(ParentActivity.this,TeachcerActivity.class);
                            startActivity(i);


                        }

                    }
                }else {
                    Toast.makeText(ParentActivity.this, "please enter correct Detail",Toast.LENGTH_SHORT).show();


                }


            }else {
                Toast.makeText(ParentActivity.this, "please try again...",Toast.LENGTH_SHORT).show();


            }

        }

    }
}
