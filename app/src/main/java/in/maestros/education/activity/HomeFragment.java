package in.maestros.education.activity;

/**
 * Created by Ravi on 29/07/15.
 */
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;


public class HomeFragment extends Fragment {

    private GridLayoutManager lLayout;
    private Button topperlist;
    private TextView name,score,classs;
    String topper_name="",topper_class="",topper_score="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);


        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        topper_name = AppsContants.sharedpreferences.getString(AppsContants.TOPPERNAME, "");
        topper_class = AppsContants.sharedpreferences.getString(AppsContants.TOPPERCLASS, "");
        topper_score = AppsContants.sharedpreferences.getString(AppsContants.TOPPERSCORE, "");




        name=(TextView)rootView.findViewById(R.id.textname);
        name.setText(topper_name);

        score=(TextView)rootView.findViewById(R.id.textscore);
        score.setText(topper_score);

        classs=(TextView)rootView.findViewById(R.id.textclass);
        classs.setText(topper_class);




        List<TechItemObject> rowListItem = getAllItemList();
        lLayout = new GridLayoutManager(getActivity(), 2);

        RecyclerView rView = (RecyclerView)rootView.findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        TechMainActivityRecyclerViewAdapter rcAdapter = new TechMainActivityRecyclerViewAdapter(getActivity(), rowListItem);
        rView.setAdapter(rcAdapter);

        topperlist=(Button)rootView.findViewById(R.id.texttopperlist);
        topperlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent topper=new Intent(getActivity(),TopperListActivity.class);
                startActivity(topper);
            }
        });

        return rootView;
    }

    private List<TechItemObject> getAllItemList(){

        List<TechItemObject> allItems = new ArrayList<TechItemObject>();
        allItems.add(new TechItemObject("",R.drawable.dd1));
        allItems.add(new TechItemObject("",R.drawable.dd2));
        allItems.add(new TechItemObject("",R.drawable.dd3));
        allItems.add(new TechItemObject("",R.drawable.dd1));
        allItems.add(new TechItemObject("",R.drawable.dd2));
        allItems.add(new TechItemObject("",R.drawable.dd3));


        return allItems;
    }

}
