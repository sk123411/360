package in.maestros.education.activity.StudentDeshBoard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.StudentDashBoard;
import in.maestros.education.activity.StudentDeshBoard.QuestionAdapter.QuestionAdapter;
import in.maestros.education.activity.StudentDeshBoard.QuestionAdapter.QuestionGetSet;
import in.maestros.education.other.AppsContants;

public class Question extends AppCompatActivity {

    RecyclerView recyclerview1;
    List<QuestionGetSet> arrayList;
    QuestionAdapter adapter;
    ProgressBar progressBar;
    String strCatID = "", strCatName = "",strId="";
    TextView txtHintHeader;

    String subject ="",classId = "";
    RecyclerView.LayoutManager layoutManagerOfrecyclerView;
    RecyclerView.Adapter recyclerViewadapter;

    Spinner spinner,levelSpinner;
    Button submitButton;
    LinearLayoutManager linearLayoutManager;

String userid="",L_ID="",Name11="",Name12="",Name13="",strPageNo="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        //recyclerview1.setHasFixedSize(true);


        progressBar = findViewById(R.id.progressBar);
        spinner = findViewById(R.id.subjectSpinner);
        submitButton = findViewById(R.id.submitButton);
        levelSpinner = findViewById(R.id.LevelSpinner);


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);

        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");










        Log.e("rteityerui", strCatID);

        progressBar = findViewById(R.id.progressBar);


        recyclerview1 = findViewById(R.id.recyclerview1);




        ImageView backImage = findViewById(R.id.back);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(getApplicationContext(), StudentDashBoard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });





        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!subject.equals("")) {
                    startActivity(new Intent(getApplicationContext(), QuestionsListActivity.class));
                }else {

                    Toast.makeText(getApplicationContext(), "Please select level of questions",Toast.LENGTH_SHORT).show();
                }
            }
        });



          layoutManagerOfrecyclerView = new LinearLayoutManager(Question.this);
            recyclerview1.setLayoutManager(layoutManagerOfrecyclerView);


            showSubjects();
      //
       // ShowQuestion();
    }



    public void showSubjects(){
        progressBar.setVisibility(View.VISIBLE);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = AppsContants.getSharedpreferences().edit();


        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=show_subject")
                .addBodyParameter("std_id", AppsContants.getSharedpreferences().getString(AppsContants.L_ID,"51")).build()
        .getAsJSONArray(new JSONArrayRequestListener() {
            @Override
            public void onResponse(JSONArray response) {


                progressBar.setVisibility(View.GONE);
                Log.d("DDDDDDDDDDD", "sdsds"+response.toString());
                final ArrayList<String> subjectsIds = new ArrayList<>();
                ArrayList<String> subjectNames = new ArrayList<>();
                final ArrayList<String> classNames = new ArrayList<>();

                for (int i = 0; i <=response.length() ; i++) {

                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        subjectNames.add(jsonObject.getString("cat_name"));
                        subjectsIds.add(jsonObject.getString("id"));
                        classNames.add(jsonObject.getString("class_id"));




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                spinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, subjectNames));


                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        editor.putString(AppsContants.S_ID, subjectsIds.get(i));
                        editor.putString(AppsContants.C_LEVEL, classNames.get(i));

                        editor.commit();

                        subject = subjectsIds.get(i);
                        classId = classNames.get(i);

                        showLevelsSubjects();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });



            }

            @Override
            public void onError(ANError anError) {

            }
        });


    }
    public void showLevelsSubjects(){

        progressBar.setVisibility(View.VISIBLE);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = AppsContants.getSharedpreferences().edit();


        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=show_subject_level")
                .addBodyParameter("subject", subject)
                .addBodyParameter("class", classId)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {


                        progressBar.setVisibility(View.GONE);

                        ArrayList<String> levelNames = new ArrayList<>();
                        final ArrayList<String> levelIDs = new ArrayList<>();

                        for (int i = 0; i <=response.length() ; i++) {

                            try {
                                JSONObject jsonObject = response.getJSONObject(i);

                                levelNames.add(jsonObject.getString("sub_cat_name"));
                                levelIDs.add(jsonObject.getString("id"));





                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        levelSpinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, levelNames));


                        levelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                editor.putString(AppsContants.LEVEL_ID, levelIDs.get(i));

                                editor.commit();

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });



                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


    }

    public void ShowQuestion() {

        progressBar.setVisibility(View.VISIBLE);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");



Log.e("jgfjsdfgjhgfjhgf",L_ID);
        progressBar.setVisibility(View.VISIBLE);
        AndroidNetworking.post(" https://360educated.com/Android/process.php?action=show_questions")
                .addBodyParameter("subject", L_ID)
                .setTag("Categories")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {

                            arrayList = new ArrayList<>();
                            progressBar.setVisibility(View.GONE);
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                QuestionGetSet GetSet = new QuestionGetSet();
                                GetSet.setId(jsonObject.getString("id"));
                                GetSet.setQuestion(jsonObject.getString("question"));
                                arrayList.add(GetSet);
                                Log.e("dfhjhsjh", jsonObject.getString("id"));

                            }

                            adapter = new QuestionAdapter(arrayList, Question.this);
                            recyclerview1.setAdapter(adapter);
                            progressBar.setVisibility(View.GONE);



                        } catch (Exception ex) {

                            Log.e("tracing", ex.getMessage());
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("tracing", anError.toString());
                        progressBar.setVisibility(View.GONE);

                    }
                });

    }
}








