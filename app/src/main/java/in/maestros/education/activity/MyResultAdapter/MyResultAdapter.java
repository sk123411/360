package in.maestros.education.activity.MyResultAdapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

public class MyResultAdapter extends RecyclerView.Adapter<MyResultAdapter.ViewHolder> {



    Context context;

    private List<MyResultGetSet> dataAdapters;

    String strLike, strComment, strSharwe;


    String strUserID = "";
    String strSeeView="",userid="",L_ID="";

    String strId = "";
    List<MyResultGetSet> ListOfdataAdapter;

    RecyclerView.LayoutManager layoutManagerOfrecyclerView;

    RecyclerView.Adapter recyclerViewadapter;
    private ArrayList<MyResultGetSet> arraylist;


    public MyResultAdapter(List<MyResultGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
        this.arraylist = new ArrayList<MyResultGetSet>();
        this.arraylist.addAll(dataAdapters);
    }

    @Override

    public MyResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_result_adapter, parent, false);

        MyResultAdapter.ViewHolder viewHolder = new MyResultAdapter.ViewHolder(view);

        return viewHolder;


    }

    @Override
    public void onBindViewHolder(final MyResultAdapter.ViewHolder Viewholder, int position) {

        final MyResultGetSet dataAdapterOBJ = dataAdapters.get(position);


        AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");


        ListOfdataAdapter = new ArrayList<>();

        Viewholder.recyclerview.setHasFixedSize(true);

        layoutManagerOfrecyclerView = new LinearLayoutManager(context);

        // recyclerview1.setLayoutManager(layoutManagerOfrecyclerView);





        SHowResult(userid,Viewholder);



        Viewholder.studentName.setText(dataAdapterOBJ.getStudentName());








    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        public TextView studentName, tvText, tvMsgText, tvTime, txtReadMore, txtShow;
        public RecyclerView recyclerview;
        public RelativeLayout relComment;
        public EditText edtComment;
        public ImageView imgSend;
        ProgressBar bar;

        public ViewHolder(View itemView) {

            super(itemView);


            studentName = (TextView) itemView.findViewById(R.id.studentName);



        }

    }


    public void SHowResult(String userid,final MyResultAdapter. ViewHolder viewholder) {
        viewholder.bar.setVisibility(View.VISIBLE);

        AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);

        AndroidNetworking.post("https://360educated.com/Android/process.php?action=my_result")
                .addBodyParameter("user_id", userid)

                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String str = response.getString("result");
                            if (str.equals("success")) {


                                String strName= response.getString("student_name");
                                viewholder.studentName.setText(strName);


                                AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.USER_NAME, response.getString("views"));

                                editor.commit();
                                Toast.makeText(context, "Views posted", Toast.LENGTH_SHORT).show();


                                viewholder.bar.setVisibility(View.GONE);
                                viewholder.studentName.setVisibility(View.VISIBLE);


                            } else {
                                viewholder.studentName.setVisibility(View.VISIBLE);

                                Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {


                            Log.e("ggggjjjjj", ex.getMessage());


                        }
                    }


                    @Override
                    public void onError(ANError anError) {

                    }
                });


    }
}


