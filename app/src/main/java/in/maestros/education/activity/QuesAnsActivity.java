package in.maestros.education.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

public class QuesAnsActivity extends AppCompatActivity {


    String ss_aid = "", ss_user_id = "", ss_right_a = "", ss_wrong_a = "", ss_total_q = "", ss_time = "", subject_id = "", level_id = "", class_id = "",subject="",classs="",status="";
    private Button buttonStopTime;

    private TextView textViewShowTime;
    private CountDownTimer countDownTimer;

    private long totalTimeCountInMilliseconds;

    private long timeBlinkInMilliseconds;
    private boolean blink;
    Button previous, next;

    public static List<NatureQuesAns> products;
    ConnectionDetector cd;

    static JSONArray contactsstate = null;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ques_ans);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        ss_user_id = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        // ss_aid="18";

        level_id = AppsContants.sharedpreferences.getString(AppsContants.LEVEL_ID, "");
        subject_id= AppsContants.sharedpreferences.getString(AppsContants.subject_id, "");
        class_id= AppsContants.sharedpreferences.getString(AppsContants.CLASS_ID, "");






       // Toast.makeText(this, ss_aid+"nishhhh", Toast.LENGTH_SHORT).show();


        mViewPager = (ViewPager) findViewById(R.id.container);

        previous = (Button) findViewById(R.id.previous);
        next = (Button) findViewById(R.id.next);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.


        //------------------start----------------
        products = new ArrayList<NatureQuesAns>();
        //searchlocation = (SearchView)findViewById(R.id.searchmylocation);
        signUpClick("1");

        //-------------------------end---------------


        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(QuesAnsActivity.this, mViewPager.getCurrentItem() + "  " + products.size(), Toast.LENGTH_LONG).show();

                mViewPager.setCurrentItem(getItem(-1), true);

            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                   if (products.size() != 0) {
                       int ro = mViewPager.getCurrentItem() + 1;

                       if (ro == products.size()) {

                           int sum = 0;
                           for (int uk = 0; uk < products.size(); uk++) {

                               try {
                                   String yu = products.get(uk).getUserAns();
                                   int one = Integer.parseInt(yu);
                                   sum = sum + one;
                               }catch (NumberFormatException e){
                                  // Toast.makeText(QuesAnsActivity.this , "Please Fill Question Paper" , Toast.LENGTH_SHORT).show();
                               }


                           }


                           ss_right_a = sum + "";
                           ss_wrong_a = (products.size() - sum) + "";
                           ss_total_q = products.size() + "";

                           try {
                               String spli = textViewShowTime.getText().toString().trim();
                               ;
                               String extensionRemoved = spli.split(":")[0];
                               String extensionRemoved1 = spli.split(":")[1];


                               int i = Integer.parseInt(extensionRemoved);
                               int i21 = Integer.parseInt(extensionRemoved1);

                               int total_time = 29 - i;

                               int total_sec = 60 - i21;

                               ss_time = total_time + ":" + total_sec;


                           } catch (NumberFormatException ex) { // handle your exception
                               ss_time = textViewShowTime.getText().toString().trim();
                           }


                           // Toast.makeText(QuesAnsActivity.this,mViewPager.getCurrentItem()+"  "+products.size(),Toast.LENGTH_LONG).show();

                           AlertDialog.Builder builder = new AlertDialog.Builder(QuesAnsActivity.this);
                           //Uncomment the below code to Set the message and title from the strings.xml file
                           //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);

                           //Setting message manually and performing action on button click
                           // builder.setTitle("360 Educating");
                           builder.setMessage("Do you want to finish exam?" + "\nRight Ans : " + ss_right_a + "\nWrong Ans : " + ss_wrong_a + "\nTotal Que  : " + ss_total_q + "\nTime  : " + ss_time)
                                   .setCancelable(false)
                                   .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                       public void onClick(DialogInterface dialog, int id) {
                                           if (NetConnection.isConnected(QuesAnsActivity.this)) {

                                               SignUpjsontask task = new SignUpjsontask();
                                               task.execute();

                                           } else {

                                               Toast.makeText(QuesAnsActivity.this, "Internet connection error", Toast.LENGTH_LONG).show();
                                           }
                                       }
                                   })
                                   .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                       public void onClick(DialogInterface dialog, int id) {
                                           //  Action for 'NO' Button
                                           finish();
                                       }
                                   });

                           //Creating dialog box
                           AlertDialog alert = builder.create();
                           //Setting the title manually
                           alert.setTitle("Finish");
                           alert.show();


                       }


                   }


                mViewPager.setCurrentItem(getItem(+1), true);

            }
        });

        //--------------start-------------------


        buttonStopTime = (Button) findViewById(R.id.btnStopTime);
        textViewShowTime = (TextView) findViewById(R.id.tvTimeCount);
        //edtTimerValue = (EditText) findViewById(R.id.edtTimerValue);

        //buttonStartTime.setOnClickListener(this);
        buttonStopTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countDownTimer.cancel();
                finish();
            }
        });
        setTimer();
        startTimer();


        //-----------------------------end-----------------------

    }


    private void setTimer() {
        int time = 30;
        totalTimeCountInMilliseconds = 60 * time * 1000;
        timeBlinkInMilliseconds = 30 * 1000;
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(totalTimeCountInMilliseconds, 500) {

            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;

                if (leftTimeInMilliseconds < timeBlinkInMilliseconds) {
                    textViewShowTime.setTextAppearance(getApplicationContext(),
                            R.style.blinkText);


                    if (blink) {
                        textViewShowTime.setVisibility(View.VISIBLE);
                        // if blink is true, textview will be visible
                    } else {
                        textViewShowTime.setVisibility(View.INVISIBLE);
                    }

                    blink = !blink; // toggle the value of blink
                }

                textViewShowTime.setText(String.format("%02d", seconds / 60)
                        + ":" + String.format("%02d", seconds % 60));
                // format the textview to show the easily readable format

            }

            @Override
            public void onFinish() {
                // this function will be called when the timecount is finished
                textViewShowTime.setText("Time up!");
                finish();


                textViewShowTime.setVisibility(View.VISIBLE);

            }

        }.start();

    }


    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        int ro;

        public SectionsPagerAdapter(FragmentManager fm, int ro) {
            super(fm);
            this.ro = ro;

        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return ro;
        }

        @Override
        public CharSequence getPageTitle(int position) {
          /*  switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }*/
            return "rohit";
        }
    }


    //-----------------------start-----------------------------------------


    public void signUpClick(String str_com_id) {


        cd = new ConnectionDetector(QuesAnsActivity.this);
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(QuesAnsActivity.this, "noNetworkConnection", Toast.LENGTH_LONG).show();
            // displayData();


        } else {


            new GetContacts_commodity(str_com_id).execute();

        }

    }

    private class GetContacts_commodity extends AsyncTask<String, Void, String> {
        String jsonStr;
        String result = "";
        String jsoprofile;

        public GetContacts_commodity(String strprofile) {
            this.jsoprofile = strprofile;

            //  Toast.makeText(PopularActivity.this,jsoprofile,Toast.LENGTH_SHORT).show();
        }

        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            //pDialog = new ProgressDialog(getActivity());
            // pDialog.setMessage("Please wait...");
            // pDialog.setCancelable(false);
            //pDialog.show();
            //  progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... voids) {
            // TODO Auto-generated method stub

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://360educating.com/Android/process.php?action=select_question");

            try {


                //   http://360educating.com/Android/process.php?action=select_question&class=52&subject=66&level=74
                //    subject_id="",level_id="",class_id=""

                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("class", class_id));
                nameValuePair.add(new BasicNameValuePair("subject", subject_id));
                nameValuePair.add(new BasicNameValuePair("level", level_id));
                post.setEntity(new UrlEncodedFormEntity(nameValuePair));

                HttpResponse response = client.execute(post);
                jsonStr = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + jsonStr);
                contactsstate = new JSONArray(jsonStr);

                for (int i = 0; i < contactsstate.length(); i++) {
                    JSONObject c = contactsstate.getJSONObject(i);
                    String a1 = c.getString("id");
                    String a3 = c.getString("question_type");
                    String a4 = c.getString("question");
                    String a5 = c.getString("upload");
                    String a6 = c.getString("option1");
                    String a7 = c.getString("option1_img");
                    String a8 = c.getString("option2");
                    String a9 = c.getString("option2_img");
                    String a10 = c.getString("option3");
                    String a11 = c.getString("option3_img");
                    String a12 = c.getString("option4");
                    String a13 = c.getString("option4_img");
                    String a14 = c.getString("answer");
                    String a15 = c.getString("answer_img");
                    String a16 = c.getString("status");
                    String a17 = c.getString("result");


                    NatureQuesAns nature = new NatureQuesAns();
                    nature.setAid(a1);
                    nature.setQtype(a3);
                    nature.setQue(a4);
                    nature.setUpload(a5);
                    nature.setOption1(a6);
                    nature.setOption1img(a7);
                    nature.setOption2(a8);
                    nature.setOption2img(a9);
                    nature.setOption3(a10);
                    nature.setOption3img(a11);
                    nature.setOption4(a12);
                    nature.setOption4img(a13);
                    nature.setAns(a14);
                    nature.setAnsimg(a15);
                    nature.setUserAns(a16);

                    products.add(nature);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            //if (pDialog.isShowing())
            //  pDialog.dismiss();

            //  progressBar.setVisibility(View.GONE);

            //  mAdapter = new MyStoreAdapterText(getActivity(),products);
            // mRecyclerView.setAdapter(mAdapter);
            // mAdapter.notifyDataSetChanged();


            if (products.size() != 0) {


                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), products.size());

                // Set up the ViewPager with the sections adapter.

                mViewPager.setAdapter(mSectionsPagerAdapter);


            } else {

                Toast.makeText(QuesAnsActivity.this, jsonStr, Toast.LENGTH_LONG).show();
            }


        }
    }
    //------------------------------------end---------------------------

    //-----------------------------STRAT RESULT----------------------

    public class SignUpjsontask extends AsyncTask<String, Void, String> {

        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result = "";
        String u_email;
        String u_pass;
        String u_lat;
        String u_lon;
        String u_name;

        @Override

        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... arg0) {

            HttpClient htppclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://360educating.com/Android/process.php?action=submit_results");
            try {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("class", class_id));
                nameValuePair.add(new BasicNameValuePair("subject", subject_id));
                nameValuePair.add(new BasicNameValuePair("level", level_id));
                nameValuePair.add(new BasicNameValuePair("user_id", ss_user_id));
                nameValuePair.add(new BasicNameValuePair("correct_ans", ss_right_a));
                nameValuePair.add(new BasicNameValuePair("wrong_ans", ss_wrong_a));
                nameValuePair.add(new BasicNameValuePair("total_qs", ss_total_q));
                nameValuePair.add(new BasicNameValuePair("test_time", ss_time));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));


                HttpResponse response = htppclient.execute(httppost);
                String object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);
                JSONObject js = new JSONObject(object);

                if (js.has("result")) {
                    Errormessage = js.getString("result");

                }

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }


        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);

            //Toast.makeText(Register.this,UserRegisterID+ "rohit "+Errormessage,Toast.LENGTH_LONG).show();
            if (!IsError) {

                if (Errormessage.equals("success")) {


                    Toast.makeText(QuesAnsActivity.this, "Result submit " + Errormessage, Toast.LENGTH_LONG).show();
                    Intent iu = new Intent(QuesAnsActivity.this, NavigationActivity.class);
                    startActivity(iu);
                    finishAffinity();


                } else {
                    Toast.makeText(QuesAnsActivity.this, Errormessage, Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(QuesAnsActivity.this, "please try again...", Toast.LENGTH_SHORT).show();


            }
        }

    }


    //----------------------------------END RESULT-----------------------
}
