package in.maestros.education.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.adapter.ShowAllQuestionDetailsAdptor;
import in.maestros.education.other.AppsContants;

public class ShowAllQuestionDetailsActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    List<NatureMyLocationText> products;
    ConnectionDetector cd;
    private ProgressDialog pDialog;
    static JSONArray contactsstate = null;
    ProgressBar progressBar;
    String subject_id="";
private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all_question_details);


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        subject_id = AppsContants.sharedpreferences.getString(AppsContants.subject_id, "");

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view_myystore);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ShowAllQuestionDetailsActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

      //  progressBar=(ProgressBar)findViewById(R.id.progressBarmystore);
       // progressBar.getIndeterminateDrawable().setColorFilter(0xFF000000, android.graphics.PorterDuff.Mode.MULTIPLY);
    //    progressBar.setVisibility(View.GONE);

        products = new ArrayList<NatureMyLocationText>();
        //searchlocation = (SearchView)findViewById(R.id.searchmylocation);
        signUpClick("");

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
    public void signUpClick(String s){


        cd = new ConnectionDetector(ShowAllQuestionDetailsActivity.this);
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(ShowAllQuestionDetailsActivity.this, "noNetworkConnection", Toast.LENGTH_LONG).show();
            // displayData();


        }
        else {



            new GetContacts_commodity().execute();

        }

    }

    private class GetContacts_commodity extends AsyncTask<String, Void, String> {
        String jsonStr;
        String result = "";
        String jsoprofile;

        public GetContacts_commodity(String strprofile) {
            this.jsoprofile = strprofile;

            //  Toast.makeText(PopularActivity.this,jsoprofile,Toast.LENGTH_SHORT).show();
        }

        public GetContacts_commodity() {

        }

        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            //pDialog = new ProgressDialog(getActivity());
            // pDialog.setMessage("Please wait...");
            // pDialog.setCancelable(false);
            //pDialog.show();
//           // progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... voids) {
            // TODO Auto-generated method stub

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://360educating.com/Android/process.php?action=show_level");

            // HttpPost post = new HttpPost(HttpUrlPath.urlPath+HttpUrlPath.actionSubject);

            try {

                List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("subject_id",subject_id));
                post.setEntity(new UrlEncodedFormEntity(nameValuePair));

                HttpResponse response = client.execute(post);
                jsonStr = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + jsonStr);
                contactsstate = new JSONArray(jsonStr);

                for (int i = 0; i < contactsstate.length(); i++) {
                    JSONObject c = contactsstate.getJSONObject(i);


                    String a1=c.getString("level_id");
                    String a2=c.getString("level");
                    String a3=c.getString("subject_id");




                    NatureMyLocationText nature = new NatureMyLocationText();
                    nature.setLevel_id(a1);
                    nature.setLevel(a2);
                    nature.setSubject_id(a3);

                    products.add(nature);

                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            //if (pDialog.isShowing())
            //  pDialog.dismiss();

           // progressBar.setVisibility(View.GONE);
            // Toast.makeText(getActivity(), ""+jsonStr, Toast.LENGTH_SHORT).show();

            mAdapter = new ShowAllQuestionDetailsAdptor(ShowAllQuestionDetailsActivity.this,products);
            mRecyclerView.setAdapter(mAdapter);
            // mAdapter.notifyDataSetChanged();



        }
    }


}
