package in.maestros.education.activity.ParentDeshBoard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.HttpUrlPath;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentAdapter;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentGetSet;
import in.maestros.education.activity.ParentDeshBoard.AttendanceAdapter.AttendanceAdapter;
import in.maestros.education.activity.ParentDeshBoard.AttendanceAdapter.AttendanceGetSet;
import in.maestros.education.other.AppsContants;

import static in.maestros.education.activity.HttpUrlPath.actionParentSeeAttendance;

public class Attendance extends AppCompatActivity {

    String userid="",L_ID="",strName="",Name11="",Name12="",Name13="";




    TextView txtName,txtClass,txtSubject,txtDate,txtAction;



    RecyclerView recyclerview1;
    List<AttendanceGetSet> arrayList;
    AttendanceAdapter adapter;
    ProgressBar progressBar;


    ImageView back;

    RecyclerView.LayoutManager layoutManagerOfrecyclerView;
    RecyclerView.Adapter recyclerViewadapter;


    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);




        txtName= findViewById(R.id.txtName);
        txtClass= findViewById(R.id.txtClass);
        txtSubject= findViewById(R.id.txtSubject);
        txtDate= findViewById(R.id.txtDate);
        progressBar= findViewById(R.id.progressBar);
        // txtAction= findViewById(R.id.txtAction);
        recyclerview1 = (RecyclerView) findViewById(R.id.recyclerview1);

       /* txtAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Result.this, MyResultDetails.class));
            }
        });*/

        back= findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Attendance.this,ParentDeshBoard.class));
            }
        });
        ShowAttendance();


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");



        layoutManagerOfrecyclerView = new LinearLayoutManager(Attendance.this);
        recyclerview1.setLayoutManager(layoutManagerOfrecyclerView);

        Log.e("gfjhgdfjgdfjh",L_ID);







    }


    public void ShowAttendance() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");


        progressBar.setVisibility(View.VISIBLE);
        Log.e("jsdiuiytwtjg",L_ID);
        AndroidNetworking.post( HttpUrlPath.urlPath + HttpUrlPath.actionParentSeeAttendance)
                .addBodyParameter("user_id", L_ID)
                .setTag("Categories")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {



                            arrayList = new ArrayList<>();

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Log.e("jasgterrr", response.getJSONObject(i) + "");


                                AttendanceGetSet GetSet = new AttendanceGetSet();
                                GetSet.setId(jsonObject.getString("id"));
                                GetSet.setStudentName(jsonObject.getString("student_name"));
                                arrayList.add(GetSet);
                                Log.e("dfhjhsjh", jsonObject.getString("id"));






                            }
                            adapter = new AttendanceAdapter(arrayList, Attendance.this);
                            recyclerview1.setAdapter(adapter);
                             progressBar.setVisibility(View.GONE);


                        } catch (Exception ex) {

                            Log.e("tracing", ex.getMessage());
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("tracing", anError.toString());
                         progressBar.setVisibility(View.GONE);

                    }
                });

    }
}
