package in.maestros.education.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import in.maestros.education.R;

public class TwitterLogin extends AppCompatActivity {


    TwitterLoginButton twitterLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Twitter.initialize(this);

        setContentView(R.layout.activity_twitter_login);
        //getSupportActionBar().hide();


        twitterLoginButton=findViewById(R.id.twitter_login_button);
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                TwitterSession session= TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken=session.getAuthToken();
                String token=authToken.token;
                String secret=authToken.secret;

                Login(session);

            }

            @Override
            public void failure(TwitterException exception) {

                Log.e("ritye78te8r",exception.getMessage());

            }
        });

    }


    public void Login(TwitterSession session){




        String Name=session.getUserName();
        long userId=session.getUserId();

        Intent intent= new Intent(TwitterLogin.this,TwitHome.class);
        intent.putExtra("name",Name);
        startActivity(intent);






    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        twitterLoginButton.onActivityResult(requestCode,resultCode,data);
    }
}
