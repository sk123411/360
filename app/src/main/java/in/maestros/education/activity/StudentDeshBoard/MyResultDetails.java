package in.maestros.education.activity.StudentDeshBoard;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.MyResultAdapter.MyResultAdapter;
import in.maestros.education.activity.MyResultAdapter.MyResultGetSet;
import in.maestros.education.other.AppsContants;

public class MyResultDetails extends AppCompatActivity {

EditText fname,edt_Class,et_Subject,et_Assignment,et_TotalQuestion,correct_answer,et_WrongAnswer,
        et_TestDate,et_TestTime,et_Score;

String userid="",L_ID="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_result);


        fname = findViewById(R.id.fname);
        edt_Class = findViewById(R.id.edt_Class);
        et_Subject = findViewById(R.id.et_Subject);
        et_Assignment = findViewById(R.id.et_Assignment);
        et_TotalQuestion = findViewById(R.id.et_TotalQuestion);
        correct_answer = findViewById(R.id.correct_answer);
        et_WrongAnswer = findViewById(R.id.et_WrongAnswer);
        et_TestDate = findViewById(R.id.et_TestDate);
        et_TestTime = findViewById(R.id.et_TestTime);
        et_Score = findViewById(R.id.et_Score);
        ShowResultDetails();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");



    }




    public void ShowResultDetails(){
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

        AndroidNetworking.post("https://360educated.com/Android/process.php?action=view_result_detail")
                .addBodyParameter("id","2")
                .setTag("text")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            String str= response.getString("result");
                            Log.e("ghjdfgh",str);

                            if (str.equals("success")){



                                Log.e("fgjhfgjhfdkjghfjgh",response.getString("result"));

                                String strClass= response.getString("class");
                                String strSubject= response.getString("subject");
                                String strName= response.getString("student_name");
                                String strDate= response.getString("test_date");
                                String strAssign= response.getString("assign_name");
                                String strTotalQuestion= response.getString("total_qs");
                                String strCorrectAnswer= response.getString("correct_ans");
                                String strWrongAnswer= response.getString("wrong_ans");
                                String strTestTime= response.getString("test_time");
                                String strScore= response.getString("score");










                                fname.setText(strName);
                                edt_Class.setText(strClass);
                                et_Subject.setText(strSubject);
                                et_Assignment.setText(strAssign);
                                et_TotalQuestion.setText(strTotalQuestion);
                                correct_answer.setText(strCorrectAnswer);
                                et_WrongAnswer.setText(strWrongAnswer);
                                et_TestDate.setText(strDate);
                                et_TestTime.setText(strTestTime);
                                et_Score.setText(strScore);

                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.USER_NAME, response.getString("subject"));
                                editor.putString(AppsContants.L_ID, response.getString("id"));


                                editor.commit();




                                // studentName.setVisibility(View.VISIBLE);


                            } else {
                                // studentName.setVisibility(View.VISIBLE);

                                // Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {


                            Log.e("ggggjjjjj", ex.getMessage());





                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });






    }
}

