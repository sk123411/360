package in.maestros.education.activity;

/**
 * Created by Ravi on 29/07/15.
 */
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;


public class MessagesFragment extends Fragment {

    private LinearLayoutManager lLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home11, container, false);

        List<TechItemObject> rowListItem = getAllItemList();
        lLayout = new LinearLayoutManager(getActivity());

        RecyclerView rView = (RecyclerView)rootView.findViewById(R.id.recycler_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        SchoolRecyclerViewAdapter rcAdapter = new SchoolRecyclerViewAdapter(getActivity(), rowListItem);
        rView.setAdapter(rcAdapter);

        return rootView;
    }

    private List<TechItemObject> getAllItemList(){

        List<TechItemObject> allItems = new ArrayList<TechItemObject>();
        allItems.add(new TechItemObject("Maths",R.drawable.s1));
        allItems.add(new TechItemObject("English",R.drawable.s2));
        allItems.add(new TechItemObject("Sciences",R.drawable.s3));
        allItems.add(new TechItemObject("Computer",R.drawable.s3));

        return allItems;
    }
}
