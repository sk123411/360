package in.maestros.education.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import in.maestros.education.R;

public class SchoolViewHolders extends RecyclerView.ViewHolder {

    public TextView countryName;
    public ImageView countryPhoto;
    public ImageView tc;
    public RelativeLayout relative;

    public SchoolViewHolders(View itemView) {
        super(itemView);
        countryName = (TextView)itemView.findViewById(R.id.tc1);
        countryPhoto = (ImageView)itemView.findViewById(R.id.tc);
        tc = (ImageView)itemView.findViewById(R.id.tc);
        relative=(RelativeLayout)itemView.findViewById(R.id.realtive_subjects);

    }
}