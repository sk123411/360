package in.maestros.education;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.stream.JsonReader;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Session;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import in.maestros.education.activity.LoginActivity;
import in.maestros.education.activity.NavigationActivity;
import in.maestros.education.activity.ParentActivity;
import in.maestros.education.activity.ParentDeshBoard.ParentDeshBoard;
import in.maestros.education.activity.StProfileActivity;
import in.maestros.education.activity.StudentDashBoard;
import in.maestros.education.activity.TeachcerActivity;
import in.maestros.education.activity.TwitHome;
import in.maestros.education.other.AppsContants;

import static com.facebook.internal.CallbackManagerImpl.RequestCodeOffset.Login;
import static in.maestros.education.other.AppsContants.Social_id;

public class FaceboookNewActivity extends AppCompatActivity {
    private Button signin;
    CallbackManager callbackManager;
   // Button share, details;
    ShareDialog shareDialog;
    LoginButton login;
  //  ProfilePictureView profile;
    Dialog details_dialog;
    TextView details_txt;
    Button adi;
    String firstLast = "", email = "";

    String firstName, lastName, social_id;
    ProgressDialog pDialog;

    private Button twitterbtn;

    private TwitterLoginButton loginButton;
    TwitterSession session;

    String screenname,username,twitterImage,location,timeZone,description;
    ImageView user_picture;
    TextView screen_name,user_name,user_location,user_timezone,user_description;

    private static final String TWITTER_KEY = "YSkMJSIBbeX9N1HMgFKPsfaMe";
    private static final String TWITTER_SECRET = "vLFUXxzSTtrw1ZMtHEVRQNZihsnXtYY4hydlk8Eoh1aLo4y0bU";
    String L_ID="",Name11="",Name12="",Name13="", U_ID="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        Twitter.initialize(this);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        // Fabric.with(this, new Twitter(authConfig));

        setContentView(R.layout.activity_facebook);


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");
        U_ID=AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");

        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");


        if (!L_ID.equals("")){

            startActivity(new Intent(getApplicationContext(), StudentDashBoard.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));

        }else if (!U_ID.equals("")){

            startActivity(new Intent(getApplicationContext(), ParentDeshBoard.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));


        }



      /*  try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "in.maestros.education",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/

        callbackManager = CallbackManager.Factory.create();
        login = (LoginButton) findViewById(R.id.login_button);
        //profile = (ProfilePictureView) findViewById(R.id.picture);


        twitterbtn = (Button) findViewById(R.id.twitterbtn);

        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);

        shareDialog = new ShareDialog(this);
        // share = (Button) findViewById(R.id.share);
        // details = (Button) findViewById(R.id.details);
        login.setReadPermissions("public_profile email");
        //  share.setVisibility(View.INVISIBLE);
        //  details.setVisibility(View.INVISIBLE);
        details_dialog = new Dialog(this);
        details_dialog.setContentView(R.layout.dialog_details);
        details_dialog.setTitle("Details");
        details_txt = (TextView) details_dialog.findViewById(R.id.details);


        signin = (Button) findViewById(R.id.signinbtn);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sign = new Intent(FaceboookNewActivity.this, LoginActivity.class);
                startActivity(sign);
            }

        });

        twitterbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Intent intent = new Intent(FaceboookNewActivity.this , MainActivity.class);
                //  startActivity(intent);

            }
        });

        if (AccessToken.getCurrentAccessToken() != null) {
            RequestData();

            //  share.setVisibility(View.VISIBLE);
            // details.setVisibility(View.VISIBLE);
        }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    // share.setVisibility(View.INVISIBLE);
                    //  details.setVisibility(View.INVISIBLE);
                    //profile.setProfileId(null);
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AccessToken.getCurrentAccessToken() != null) {
                    Login(session);
                    //share.setVisibility(View.INVISIBLE);
                    //details.setVisibility(View.INVISIBLE);
                    // profile.setProfileId(null);
                }
            }
        });



     /*   share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareLinkContent content = new ShareLinkContent.Builder().build();
                shareDialog.show(content);

            }
        });*/


      /*  loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                session = Twitter.getSessionManager().getActiveSession();

                Twitter.getApiClient(session).getAccountService()
                        .verifyCredentials(true, false, new Callback<User>() {

                            @Override
                            public void success(Result<User> userResult) {


                                if (AccessToken.getCurrentAccessToken() != null) {
                                    RequestTwitterData(session);
                                    // share.setVisibility(View.VISIBLE);
                                    // details.setVisibility(View.VISIBLE);
                                }




                                User user = userResult.data;

                                twitterImage = user.profileImageUrl;
                                int  screenname = (int) user.id;
                                username = user.name;
                                location = user.location;
                               // int  locationii = (int) user.id;
                                timeZone = user.timeZone;
                                description = user.description;

                                user_picture = (ImageView) findViewById(R.id.profile_pic);
                                Picasso.with(getApplicationContext()).load(twitterImage.toString())
                                        .into(user_picture);

                                screen_name = (TextView) findViewById(R.id.screen_name);
                                screen_name.setText("Username : " + screenname);

                                user_name = (TextView) findViewById(R.id.user_name);
                                user_name.setText("Name : "+username);

                                user_location = (TextView) findViewById(R.id.user_location);
                                user_location.setText("Location : "+location);

                                user_timezone = (TextView) findViewById(R.id.user_timezone);
                                user_timezone.setText("Timezone : "+timeZone);

                                user_description = (TextView) findViewById(R.id.user_description);
                                user_description.setText("Description : "+description);

                                try {

                                    String[] separated = username.split(" ");
                                    firstName = separated[0];
                                    lastName = separated[1];
                                    social_id = separated[2];

                                }
                                catch (ArrayIndexOutOfBoundsException e){
                                    firstName=  username;
                                    lastName = username;
                                    social_id = username;

                                }

                                SocialTwitterJsonTask socialJsonTask = new SocialTwitterJsonTask(firstName, lastName,screenname, "primary1");
                                socialJsonTask.execute();

                            }

                            @Override
                            public void failure(TwitterException e) {

                                Log.e("fghjiyyy",e.getMessage());

                            }

                        });

                //loginButton.setVisibility(View.GONE);


            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }


        });

*/


        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;


                Login(session);


            }


            @Override
            public void failure(TwitterException exception) {

                Log.e("ritye78te8r", exception.getMessage());

            }
        });


        login.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                if (AccessToken.getCurrentAccessToken() != null) {
                    RequestData();
                   // share.setVisibility(View.VISIBLE);
                   // details.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {
            }
        });

    }


    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
                try {
                    if (json != null) {
                        String text = "<b>Name :</b> " + json.getString("name") + "<br><br><b>Email :</b> "  ;

                        firstLast = json.getString("name");


                        String[] separated = firstLast.split(" ");
                        firstName = separated[0];
                        lastName = separated[1];

                       // email = json.getString("email");
                       /* details_txt.setText(Html.fromHtml(text));
                        profile.setProfileId(json.getString("id"));*/


                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                        editor.putString(AppsContants.FACEBOOK_PROFILE, json.getString("id"));
                        editor.commit();

                        SocialJsonTask socialJsonTask = new SocialJsonTask(firstName, lastName, email,social_id, "primary1");
                        socialJsonTask.execute();


                        // startActivity(new Intent(FaceboookNewActivity.this, NavigationActivity.class));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.e("ghjdhgfjdgf",e.getMessage());
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }


    public void  Login(TwitterSession session) {





        String Name = session.getUserName();
        long userId = session.getUserId();
        session.getUserId();

        //  email.setText(Html.fromHtml(text));


        SocialTwitterJsonTask socialTwitterJsonTask = new SocialTwitterJsonTask(firstName, lastName, email,social_id, "primary1");
        socialTwitterJsonTask.execute();

      /*Intent intent = new Intent(FaceboookNewActivity.this, TwitHome.class);
        intent.putExtra("name", Name);
        intent.putExtra("social_id", userId);
        startActivity(intent);*/


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);




         callbackManager.onActivityResult(requestCode, resultCode, data);

        loginButton.onActivityResult(requestCode, resultCode, data);
    }




    public class SocialTwitterJsonTask extends AsyncTask<String, Void, String> {




        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result = "";



        String s_email, s_cls, s_name, s_lnam, s_gender, s_mobile, s_age, s_race;

        String s_p_fname = "",
                s_t_fname = "",
                s_dt = "";
        String object;
        String semail = "", spwd = "";

        String fname, email,social_id,lname,classs;

        public SocialTwitterJsonTask(String social_id, String email, String lastName, String fname, String lname) {
            this.fname = fname;
            this.email = email;
            this.social_id = social_id;
        }

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);
            pDialog = new ProgressDialog(FaceboookNewActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        protected String doInBackground(String... arg0) {

            DefaultHttpClient htppclient = new DefaultHttpClient();
            //   HttpPost httppost=new HttpPost(HttpUrlPath.urlPath+HttpUrlPath.actionLogin);
            HttpPost httppost = new HttpPost("https://360educated.com/Android/process.php?action=social_signup");
            try {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("email", email));
                nameValuePair.add(new BasicNameValuePair("oauth_provider", "fgjdf"));
                nameValuePair.add(new BasicNameValuePair("social_id",Social_id));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                HttpResponse response = htppclient.execute(httppost);
                object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);
                JSONObject js = new JSONObject(object);


              /*  AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.FACEBOOK_PROFILE, js.getString("id"));
                editor.commit();*/
                // String s_email,s_gender,s_password,s_firstname,s_lastname,s_dob,s_mobile,s_country,s_city;



                if (js.has("id")) {
                    UserRegisterID = js.getString("id");
                    s_name = js.getString("fname");
                    s_lnam=js.getString("lname");
                    s_email=js.getString("email");
                    s_cls=js.getString("class");
                    s_p_fname = js.getString("p_fname");
                    s_t_fname=js.getString("t_fname");
                    s_dt=js.getString("dt");





                    Errormessage = js.getString("result");


                    Log.e("gkkfghsvhf",js.getString("result"));
                    Log.e("fgjjhgjhgjfgh",js.getString("id"));


                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }


        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            if (pDialog.isShowing())
                pDialog.dismiss();
            if (!IsError) {


                if (Errormessage.equals("successful")) {

                    if (UserRegisterID != null) {
                        if (!UserRegisterID.equals("")) {

                            Toast.makeText(FaceboookNewActivity.this,"your login has been "+Errormessage,Toast.LENGTH_SHORT).show();

                            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                            editor.putString(AppsContants.L_ID, UserRegisterID);
                            editor.putString(AppsContants.USER_NAME, s_name);
                            editor.putString(AppsContants.LNAME, s_lnam);
                            editor.putString(AppsContants.EMAIL, s_email);

                            editor.commit();





                            if (s_dt.equals("")){
                                Intent i= new Intent(FaceboookNewActivity.this,StProfileActivity.class);
                                startActivity(i);

                                finish();

                            }else {


                                if (s_p_fname.equals("")){
                                    Intent i= new Intent(FaceboookNewActivity.this,ParentActivity.class);
                                    startActivity(i);
                                    finish();

                                }else {

                                    if (s_t_fname.equals("")){
                                        Intent i= new Intent(FaceboookNewActivity.this,TeachcerActivity.class);
                                        startActivity(i);
                                        finish();

                                    }else {
                                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                                        editor1.putString(AppsContants.U_ID, UserRegisterID);

                                        editor1.commit();
                                        Intent i= new Intent(FaceboookNewActivity.this,NavigationActivity.class);
                                        startActivity(i);
                                        finishAffinity();

                                    }
                                }



                            }

                        }

                    }
                }
                else {

                    Toast.makeText(FaceboookNewActivity.this,  Errormessage, Toast.LENGTH_SHORT).show();

                }


            } else {
                Toast.makeText(FaceboookNewActivity.this, "please try again...", Toast.LENGTH_SHORT).show();


            }

        }


    }



    public class SocialJsonTask extends AsyncTask<String, Void, String> {

        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result = "";


        String s_email, s_cls, s_name, s_lnam, s_gender, s_mobile, s_age, s_race;

        String s_p_fname = "",
                s_t_fname = "",
                s_dt = "";
        String object;
        String semail = "", spwd = "";

        String fname, lname, email, classs,social_id;

        public SocialJsonTask(String fname, String lname, String email, String classs,String social_id) {
            this.fname = fname;
            this.lname = lname;
            this.email = email;
            this.classs = classs;
            this.social_id = social_id;
        }

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);
            pDialog = new ProgressDialog(FaceboookNewActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        protected String doInBackground(String... arg0) {

            DefaultHttpClient htppclient = new DefaultHttpClient();
            //   HttpPost httppost=new HttpPost(HttpUrlPath.urlPath+HttpUrlPath.actionLogin);
            HttpPost httppost = new HttpPost("https://360educated.com/Android/process.php?action=social_signup");
            try {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("email", email));
                nameValuePair.add(new BasicNameValuePair("oauth_provider", fname));
                nameValuePair.add(new BasicNameValuePair("lname", lname));
                nameValuePair.add(new BasicNameValuePair("class", classs));
                nameValuePair.add(new BasicNameValuePair("social_id", social_id));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                HttpResponse response = htppclient.execute(httppost);
                object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);
                JSONObject js = new JSONObject(object);


                // String s_email,s_gender,s_password,s_firstname,s_lastname,s_dob,s_mobile,s_country,s_city;



                if (js.has("id")) {
                    UserRegisterID = js.getString("id");
                    s_name = js.getString("fname");
                    s_lnam=js.getString("lname");
                    s_email=js.getString("email");
                    s_cls=js.getString("class");
                    s_p_fname = js.getString("p_fname");
                    s_t_fname=js.getString("t_fname");
                    s_dt=js.getString("dt");

                    Errormessage = js.getString("result");


                    Log.e("gkkfghsvhf",js.getString("result"));
                    Log.e("fgjjhgjhgjfgh",js.getString("id"));


                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }


        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            if (pDialog.isShowing())
                pDialog.dismiss();
            if (!IsError) {


                if (Errormessage.equals("successful")) {

                    if (UserRegisterID != null) {
                        if (!UserRegisterID.equals("")) {

                            Toast.makeText(FaceboookNewActivity.this,"your login has been "+Errormessage,Toast.LENGTH_SHORT).show();

                            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                            editor.putString(AppsContants.L_ID, UserRegisterID);
                            editor.putString(AppsContants.USER_NAME, s_name);
                            editor.putString(AppsContants.LNAME, s_lnam);
                            editor.putString(AppsContants.EMAIL, s_email);
                            editor.putString(AppsContants.CLASS, s_cls);
                            editor.commit();


                            if (s_dt.equals("")){
                                Intent i= new Intent(FaceboookNewActivity.this,StProfileActivity.class);
                                startActivity(i);
                                finish();

                            }else {


                                if (s_p_fname.equals("")){
                                    Intent i= new Intent(FaceboookNewActivity.this,ParentActivity.class);
                                    startActivity(i);
                                    finish();

                                }else {

                                    if (s_t_fname.equals("")){
                                        Intent i= new Intent(FaceboookNewActivity.this,TeachcerActivity.class);
                                        startActivity(i);
                                        finish();

                                    }else {
                                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                                        editor1.putString(AppsContants.U_ID, UserRegisterID);

                                        editor1.commit();
                                        Intent i= new Intent(FaceboookNewActivity.this,NavigationActivity.class);
                                        startActivity(i);
                                        finishAffinity();

                                    }
                                }

                            }

                        }

                    }
                }
                else {

                    Toast.makeText(FaceboookNewActivity.this,  Errormessage, Toast.LENGTH_SHORT).show();

                }


            } else {
                Toast.makeText(FaceboookNewActivity.this, "please try again...", Toast.LENGTH_SHORT).show();


            }

        }


    }


}

// url 'https://maven.fabric.io/public'
 //classpath 'io.fabric.tools:gradle:1.+'