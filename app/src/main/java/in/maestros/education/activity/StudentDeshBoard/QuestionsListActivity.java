package in.maestros.education.activity.StudentDeshBoard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import in.maestros.education.R;
import in.maestros.education.activity.StudentDeshBoard.QuizAdapter.QuizAdapter;
import in.maestros.education.activity.StudentDeshBoard.QuizAdapter.QuizGetSet;
import in.maestros.education.other.AppsContants;

public class QuestionsListActivity extends AppCompatActivity {


    private String subject="",classText="",level="";
    public static RecyclerView questionPager;
    public  TextView txtMinutes;
    Button btnSubmit;
    public static String completedTimeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions_list);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        txtMinutes = findViewById(R.id.txtMinutes);
        btnSubmit = findViewById(R.id.btnSubmit);

        subject = AppsContants.getSharedpreferences().getString(AppsContants.S_ID,"");
        classText = AppsContants.getSharedpreferences().getString(AppsContants.C_LEVEL,"");
        level = AppsContants.getSharedpreferences().getString(AppsContants.LEVEL_ID,"");

        questionPager = findViewById(R.id.questionList);


        LinearLayoutManager manager =new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL,false) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        };

        questionPager.setLayoutManager(manager);

        ShowLessionQues();


        ImageView backImage = findViewById(R.id.back);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        new CountDownTimer(672000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {


                txtMinutes.setText(""+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

//
                Long elapsedTime = 672000 - millisUntilFinished;

                completedTimeText = ""+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( elapsedTime),
                        TimeUnit.MILLISECONDS.toSeconds(elapsedTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(elapsedTime)));


            }

            public void onFinish() {
                txtMinutes.setText("done!");
            }
        }.start();



        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   InsertQuiz();

                finish();

            }
        });


    }


    public void ShowLessionQues() {


        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=show_question")
                .addBodyParameter("subject", subject)
                .addBodyParameter("class", classText)
                .addBodyParameter("level", level)

                .setTag("Categories")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {



                        try {
                            JSONArray jsonArray = response.getJSONArray("qq_id");

                            Log.d("TESTTTTTTTTTTT", "::"+response.toString());




                            ArrayList<QuizGetSet> arrayList = new ArrayList<>();

                            if (jsonArray.length()>0) {

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Log.e("jasgterrr", jsonArray.getJSONObject(i) + "");


                                    QuizGetSet GetSet = new QuizGetSet();
                                    GetSet.setId(jsonObject.getString("id"));
                                    GetSet.setQuestion(jsonObject.getString("question"));
                                    GetSet.setOption1(jsonObject.getString("option1"));
                                    GetSet.setOption2(jsonObject.getString("option2"));
                                    GetSet.setOption3(jsonObject.getString("option3"));
                                    GetSet.setOption4(jsonObject.getString("option4"));
                                    arrayList.add(GetSet);
                                    Log.e("dfhjhsjh", jsonObject.getString("id"));


                                    // progressBar.setVisibility(View.GONE);
                                }

                                QuizAdapter adapter = new QuizAdapter(arrayList, QuestionsListActivity.this, QuestionsListActivity.this);
                                questionPager.setAdapter(adapter);

                            }else {

                                finish();
                                Toast.makeText(getApplicationContext(),"No question available",Toast.LENGTH_LONG).show();

                            }
                            } catch (Exception ex) {

                            Log.e("tracing", ex.getMessage());
                            //progressBar.setVisibility(View.GONE);
                        }







                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });



    }

}