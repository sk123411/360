package in.maestros.education.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import in.maestros.education.R;

/**
 * Created by Maestros on 1/25/2017.
 */

public class PlaceholderFragment extends Fragment {
    EditText et_ans;


    String fina_ans="";


    TextView qes;
    LinearLayout ll11,ll22;
    RadioGroup radioGroup;
    TextView a_text,b_text,c_text,d_text;
    ImageView a_img,b_img,c_img,d_img,img_q;


        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_questions, container, false);

            qes=(TextView) rootView.findViewById(R.id.qes);
            et_ans=(EditText) rootView.findViewById(R.id.et_ans);






            a_text=(TextView) rootView.findViewById(R.id.a_text);
            b_text=(TextView) rootView.findViewById(R.id.b_text);
            c_text=(TextView) rootView.findViewById(R.id.c_text);
            d_text=(TextView) rootView.findViewById(R.id.d_text);

            a_img=(ImageView) rootView.findViewById(R.id.a_img);
            b_img=(ImageView) rootView.findViewById(R.id.b_img);
            c_img=(ImageView) rootView.findViewById(R.id.c_img);
            d_img=(ImageView) rootView.findViewById(R.id.d_img);
            img_q=(ImageView) rootView.findViewById(R.id.img_q);


            ll11=(LinearLayout) rootView.findViewById(R.id.ll11);
            ll22=(LinearLayout) rootView.findViewById(R.id.ll22);

            radioGroup =(RadioGroup) rootView.findViewById(R.id.radioGroup);


            final int a=getArguments().getInt(ARG_SECTION_NUMBER);

            String ques=QuesAnsActivity.products.get(a).getQue();

            String ques_img=QuesAnsActivity.products.get(a).getUpload();

            String type=QuesAnsActivity.products.get(a).getQtype();


            String o1=QuesAnsActivity.products.get(a).getOption1();
            String o2=QuesAnsActivity.products.get(a).getOption2();
            String o3=QuesAnsActivity.products.get(a).getOption3();
            String o4=QuesAnsActivity.products.get(a).getOption4();


            String o1img=QuesAnsActivity.products.get(a).getOption1img();
            String o2img=QuesAnsActivity.products.get(a).getOption2img();
            String o3img=QuesAnsActivity.products.get(a).getOption3img();
            String o4img=QuesAnsActivity.products.get(a).getOption4img();


            final String ans_server=QuesAnsActivity.products.get(a).getAns();


            if(type.equals("multi")){
                ll11.setVisibility(View.VISIBLE);
                ll22.setVisibility(View.GONE);
                radioGroup.setVisibility(View.VISIBLE);

            }else if (type.equals("input")){

                ll11.setVisibility(View.GONE);
                ll22.setVisibility(View.VISIBLE);
                radioGroup.setVisibility(View.GONE);
            }




            qes.setText("Ques "+(a+1)+" : "+ques);
            a_text.setText(o1);
            b_text.setText(o2);
            c_text.setText(o3);
            d_text.setText(o4);





            if (ques_img.equals("")){
                img_q.setVisibility(View.GONE);

            }else {

                Picasso.with(getActivity())
                        .load(ques_img)
                        .into(img_q);

            }




            if (o1img.equals("")){
                a_img.setVisibility(View.GONE);

            }else {

                Picasso.with(getActivity())
                        .load(o1img)
                        .into(a_img);

            }

            if (o2img.equals("")){
                b_img.setVisibility(View.GONE);

            }else {
                Picasso.with(getActivity())
                        .load(o2img)
                        .into(b_img);

            }

            if (o3img.equals("")){
                c_img.setVisibility(View.GONE);

            }else {

                Picasso.with(getActivity())
                        .load(o3img)
                        .into(c_img);

            }

            if (o4img.equals("")){
                d_img.setVisibility(View.GONE);

            }else {
                Picasso.with(getActivity())
                        .load(o4img)
                        .into(d_img);

            }



            //--------------------------------get result--------------------------





            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    if (checkedId == R.id.radioButton1) {

                         fina_ans="option1";


                        if (fina_ans.equals(ans_server)){


                            QuesAnsActivity.products.get(a).setUserAns("1");


                        }else {

                            QuesAnsActivity.products.get(a).setUserAns("0");
                        }

                        String matchyy=QuesAnsActivity.products.get(a).getUserAns();
                        Toast.makeText(getActivity(),matchyy,Toast.LENGTH_LONG).show();


                    } else  if (checkedId == R.id.radioButton2) {


                        fina_ans="option2";
                        if (fina_ans.equals(ans_server)){


                            QuesAnsActivity.products.get(a).setUserAns("1");


                        }else {

                            QuesAnsActivity.products.get(a).setUserAns("0");
                        }

                        String matchyy=QuesAnsActivity.products.get(a).getUserAns();
                        Toast.makeText(getActivity(),matchyy,Toast.LENGTH_LONG).show();
                    }
                    else  if (checkedId == R.id.radioButton3) {


                        fina_ans="option3";
                        if (fina_ans.equals(ans_server)){


                            QuesAnsActivity.products.get(a).setUserAns("1");


                        }else {

                            QuesAnsActivity.products.get(a).setUserAns("0");
                        }

                        String matchyy=QuesAnsActivity.products.get(a).getUserAns();
                        Toast.makeText(getActivity(),matchyy,Toast.LENGTH_LONG).show();
                    }
                    else  if (checkedId == R.id.radioButton4) {


                        fina_ans="option4";
                        if (fina_ans.equals(ans_server)){


                            QuesAnsActivity.products.get(a).setUserAns("1");


                        }else {

                            QuesAnsActivity.products.get(a).setUserAns("0");
                        }

                        String matchyy=QuesAnsActivity.products.get(a).getUserAns();
                        Toast.makeText(getActivity(),matchyy,Toast.LENGTH_LONG).show();
                    }

                }
            });






            //----------------------------------end result---------------------


            et_ans.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    // TODO Auto-generated method stub
                    if (et_ans.getText().toString().equals(ans_server)) {

                        QuesAnsActivity.products.get(a).setUserAns("1");

                    } else {

                        QuesAnsActivity.products.get(a).setUserAns("0");
                    }
                    String matchyy=QuesAnsActivity.products.get(a).getUserAns();
                    Toast.makeText(getActivity(),matchyy,Toast.LENGTH_LONG).show();

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    // TODO Auto-generated method stub

                }

            });















            return rootView;
        }
    }

