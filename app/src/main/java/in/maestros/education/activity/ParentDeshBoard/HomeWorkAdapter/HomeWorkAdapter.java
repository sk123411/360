package in.maestros.education.activity.ParentDeshBoard.HomeWorkAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.ViewAttendanceAdapter.ViewAttendanceAdapter;
import in.maestros.education.activity.ParentDeshBoard.ViewAttendanceAdapter.ViewAttendanceGetSet;

public class HomeWorkAdapter extends RecyclerView.Adapter<HomeWorkAdapter.ViewHolder> {

    Context context;


    List<HomeWorkGetSet> dataAdapters;


    String strCatID="",strId="",strCatName="";









    public HomeWorkAdapter(List<HomeWorkGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public HomeWorkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homework_adapter, parent, false);



        HomeWorkAdapter.ViewHolder viewHolder = new HomeWorkAdapter.ViewHolder(view);











        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HomeWorkAdapter.ViewHolder Viewholder, int position) {

        final HomeWorkGetSet dataAdapterOBJ = dataAdapters.get(position);







        Viewholder.txtName.setText(dataAdapterOBJ.getName());
        Viewholder.txtClass.setText(dataAdapterOBJ.getHMClass());
        Viewholder.txtSubject.setText(dataAdapterOBJ.getSubject());
        Viewholder.txtHomeWork.setText(dataAdapterOBJ.getHomeWork());
        Viewholder.txtDate.setText(dataAdapterOBJ.getHMDate());








    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }





    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtName,txtClass,txtSubject,txtHomeWork,txtDate;




        public ViewHolder(View itemView) {

            super(itemView);



            txtName = itemView.findViewById(R.id.txtName);
            txtClass = itemView.findViewById(R.id.txtClass);
            txtSubject = itemView.findViewById(R.id.txtSubject);
            txtHomeWork = itemView.findViewById(R.id.txtHomeWork);
            txtDate = itemView.findViewById(R.id.txtDate);



        }
    }
}