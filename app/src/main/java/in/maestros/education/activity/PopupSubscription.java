package in.maestros.education.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.maestros.education.R;

public class PopupSubscription extends Activity {
    TextView sub_close;
    Button subs_b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_subscription);
        sub_close=(TextView)findViewById(R.id.sub_close) ;
        sub_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        subs_b=(Button)findViewById(R.id.subs_b) ;
        subs_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(PopupSubscription.this,LevelActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
