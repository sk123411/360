package in.maestros.education.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

/**
 * Created by Ravi on 29/07/15.
 */
public class AFragment extends Fragment {

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    List<NatureMyLocationText> products;
    ConnectionDetector cd;
    private ProgressDialog pDialog;
    static JSONArray contactsstate = null;
    ProgressBar progressBar;
    String CLASS_ID="";



    public AFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_friends, container, false);

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        CLASS_ID = AppsContants.sharedpreferences.getString(AppsContants.CLASS_ID, "");



        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.recycler_view_myystore);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        progressBar=(ProgressBar)rootView.findViewById(R.id.progressBarmystore);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFF000000, android.graphics.PorterDuff.Mode.MULTIPLY);
        progressBar.setVisibility(View.GONE);

        products = new ArrayList<NatureMyLocationText>();
        //searchlocation = (SearchView)findViewById(R.id.searchmylocation);
        signUpClick("1");



        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        Intent i = new Intent(getActivity(), NavigationActivity.class);
        startActivity(i);
    }

    public void signUpClick(String str_com_id){


        cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getActivity(), "noNetworkConnection", Toast.LENGTH_LONG).show();
            // displayData();


        }
        else {



            new GetContacts_commodity(str_com_id).execute();

        }

    }

    private class GetContacts_commodity extends AsyncTask<String, Void, String> {
        String jsonStr;
        String result = "";
        String jsoprofile;

        public GetContacts_commodity(String strprofile) {
            this.jsoprofile = strprofile;

            //  Toast.makeText(PopularActivity.this,jsoprofile,Toast.LENGTH_SHORT).show();
        }

        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            //pDialog = new ProgressDialog(getActivity());
            // pDialog.setMessage("Please wait...");
            // pDialog.setCancelable(false);
            //pDialog.show();
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected String doInBackground(String... voids) {
            // TODO Auto-generated method stub

            HttpClient client = new DefaultHttpClient();
              HttpPost post = new HttpPost("http://360educating.com/Android/process.php?action=show_subject");

           // HttpPost post = new HttpPost(HttpUrlPath.urlPath+HttpUrlPath.actionSubject);

            try {

                List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("class",CLASS_ID));
                post.setEntity(new UrlEncodedFormEntity(nameValuePair));

                HttpResponse response = client.execute(post);
                jsonStr = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + jsonStr);
                contactsstate = new JSONArray(jsonStr);

                for (int i = 0; i < contactsstate.length(); i++) {
                    JSONObject c = contactsstate.getJSONObject(i);

                    String a2=c.getString("subject_id");
                    String a3=c.getString("subject");
                    String a4=c.getString("class");
                    String a5=c.getString("status");

                    NatureMyLocationText nature = new NatureMyLocationText();
                    nature.setSubject_id(a2);
                    nature.setSubject(a3);
                    nature.setClasss(a4);
                    nature.setstatus(a5);
                    products.add(nature);
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return result;
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            //if (pDialog.isShowing())
            //  pDialog.dismiss();

            progressBar.setVisibility(View.GONE);
           // Toast.makeText(getActivity(), ""+jsonStr, Toast.LENGTH_SHORT).show();

            mAdapter = new MyStoreAdapterText(getActivity(),products);
            mRecyclerView.setAdapter(mAdapter);
            // mAdapter.notifyDataSetChanged();

        }
    }

}
