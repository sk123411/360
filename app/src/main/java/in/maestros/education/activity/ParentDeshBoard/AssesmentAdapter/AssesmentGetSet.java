package in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter;

public class AssesmentGetSet {


    public String Id;
    public String StudentName;
    public String Assesment;
    public String TeacherName;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getAssesment() {
        return Assesment;
    }

    public void setAssesment(String assesment) {
        Assesment = assesment;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String SchoolName;


}
