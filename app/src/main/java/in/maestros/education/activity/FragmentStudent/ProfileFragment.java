package in.maestros.education.activity.FragmentStudent;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.maestros.education.R;
import in.maestros.education.activity.HttpUrlPath;
import in.maestros.education.other.AppsContants;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {


    CircleImageView imageProfile;
    ImageView select_Img;
    EditText sname,dob,class1,gender,email;

    String strId="",strUserName="",Name11="";
    String urlString = "";
Button btn_Update;

File file;

    String Name13="",strName="",L_ID,strDob="",strClass="",strGender="",Name12="";

    public static List<String> mDetials123 = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile2, container, false);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        btn_Update=(Button)view.findViewById(R.id.btn_Update);
        sname=(EditText) view.findViewById(R.id.sname);
        dob=(EditText)view.findViewById(R.id.dob);
        class1=(EditText)view.findViewById(R.id.class1);
        gender=(EditText)view.findViewById(R.id.gender);
        email=(EditText)view.findViewById(R.id.email);
        imageProfile=(CircleImageView) view.findViewById(R.id.imageProfile);
        select_Img=(ImageView) view.findViewById(R.id.select_Img);



        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);



        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");
        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");


        sname.setText(Name11);

        email.setText(Name13);
        mDetials123 = new ArrayList<>();
        Log.e("hgdjfgdjfgj",Name11);



      /*  AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strId = AppsContants.sharedpreferences.getString(AppsContants., "");
        strUserName = AppsContants.sharedpreferences.getString(UserName, "");
        strUserType = AppsContants.sharedpreferences.getString(AppsContants.UserType, "");

        Log.e("tyiueryiu", UserName);
*/


        /*   Glide.with(updateProfile.this).load(str).into(imageProfile);*/





        mDetials123 = new ArrayList<>();



        select_Img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();

            }
        });

        btn_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Name11 = sname.getText().toString().trim();
                strDob = dob.getText().toString().trim();
                strClass = class1.getText().toString().trim();
                 strGender= gender.getText().toString().trim();
                Name13 = email.getText().toString().trim();

                /*  progress.setVisibility(View.VISIBLE);*/
                new Thread(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void run() {

                        UpdateProfile(mDetials123,HttpUrlPath.urlPath + HttpUrlPath.actionUpdateProfile);



                    }
                }).start();
            }
        });







        return  view;



    }

    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bitmap;
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);

                    imageProfile.setImageBitmap(bitmap);
                    String path = android.os.Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                mDetials123.add(picturePath);
                c.close();
                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                imageProfile.setImageBitmap(thumbnail);

            }
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void UpdateProfile(final List<String> mDetials, String url) {
        List<File> mFiles = new ArrayList<>();
        for (int i = 0; i < mDetials.size(); i++) {
            String mFileLocation = (mDetials.get(i));
            File file = new File(mFileLocation);
            mFiles.add(file);
        }
        urlString = url;

        try {

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            List<FileBody> mFileBody = new ArrayList<>();
            for (int i = 0; i < mFiles.size(); i++) {
                FileBody bin1 = new FileBody(mFiles.get(i));
                mFileBody.add(bin1);
            }
            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
            reqEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            for (int i = 0; i < mFileBody.size(); i++) {
                reqEntity.addPart("image", mFileBody.get(i));
            }


            reqEntity.addTextBody("user_id", String.valueOf(L_ID), ContentType.TEXT_PLAIN);
            //reqEntity.addTextBody("mobile", strName, ContentType.TEXT_PLAIN.withCharset("UTF-8"));

            if (!Name11.equals("")) {
                reqEntity.addTextBody("name", Name11, ContentType.TEXT_PLAIN.withCharset("UTF-8"));
            }
            if (Name13.equals("")) {
                reqEntity.addTextBody("email", Name13, ContentType.TEXT_PLAIN.withCharset("UTF-8"));
            }


            post.setEntity(reqEntity.build());
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);
            Log.e("iurehteiurhg", response_str);
            if (resEntity != null) {
                Log.i("RESPONSE", response_str);
               getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        try {

                            /* progress.setVisibility(View.GONE);*/
                            JSONObject js = new JSONObject(response_str);

                            if (js.getString("result").equalsIgnoreCase("successfully")) {

                                sname.setText(js.getString("p_fname"));
                                dob.setText(js.getString("dob"));
                                class1.setText(js.getString("class"));
                                gender.setText(js.getString("gender"));
                                email.setText(js.getString("email"));

                                Log.e("eruhfdsa", js.getString("result"));





                                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.L_ID, js.getString("id"));
                                editor.putString(AppsContants.USER_NAME, js.getString("p_fname"));
                                editor.putString(AppsContants.Dob, js.getString("dob"));
                                editor.putString(AppsContants.CLASS, js.getString("class"));
                                editor.putString(AppsContants.Gender, js.getString("gender"));
                                editor.putString(AppsContants.EMAIL, js.getString("email"));
                                editor.commit();
                                email.setText(Name13);

                                Toast.makeText(getActivity(), "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), "" + js.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Log.e("Debug", "user_id " + e.getMessage(), e);
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception ex) {

            Log.e("Debug", "error:" + ex.getMessage(), ex);

            // dialog.dismiss();
        }
    }


}




























