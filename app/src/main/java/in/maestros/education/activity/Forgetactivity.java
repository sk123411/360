package in.maestros.education.activity;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


import in.maestros.education.R;


public class Forgetactivity extends Activity {
   TextView requestbtn,canclebtn;
    String s_emaillogin;
    ImageView  cross;
    EditText emailforget;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetactivity);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        canclebtn=(TextView)findViewById(R.id.canclebtn);
        requestbtn=(TextView)findViewById(R.id.requestbtn);
      //  cross=(ImageView)findViewById(R.id.cross);
//        cross.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });



        emailforget=(EditText)findViewById(R.id.emailforget);
        requestbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Toast.makeText(getApplication(),"hello",Toast.LENGTH_SHORT).show();

                s_emaillogin = emailforget.getText().toString().trim();

                if (NetConnection.isConnected(Forgetactivity.this)) {
                    //progressBar.setVisibility(View.VISIBLE);


                    forgatejsontask task = new forgatejsontask();
                    task.execute();

                } else {


                    Toast.makeText(Forgetactivity.this, "Internet connection error", Toast.LENGTH_LONG).show();

                }
            }
        });
        canclebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }




    public class forgatejsontask extends AsyncTask<String, Void, String> {

        String Errormessage;
        //String UserRegisterID;
        boolean IsError = false;
        String result = "";
        String u_email;
        String u_pass;
        String u_lat;
        String u_lon;
        String u_name;

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            // progressBarforget.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);
        }

        protected String doInBackground(String... arg0) {

            HttpClient htppclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("https://360educated.com/Android/process.php?action=forget_psw");
            try {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();


                nameValuePair.add(new BasicNameValuePair("email", s_emaillogin));
                //nameValuePair.add(new BasicNameValuePair("password",s_loginpassword));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));


                HttpResponse response = htppclient.execute(httppost);
                String object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);
                JSONObject js = new JSONObject(object);

                /*if (js.has("id")) {
                    UserRegisterID = js.getString("id");
                    u_email = js.getString("email");
                    u_pass = js.getString("password");
                    u_name = js.getString("username");

                    Errormessage = js.getString("result");
                    //user_mobile=js.getString("number");

                } else */
                if (js.has("result")) {
                    Errormessage = js.getString("result");

                }

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }


        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            // loader.setVisibility(View.GONE);
            //progressBarforget.setVisibility(View.GONE);
            //Toast.makeText(Register.this,UserRegisterID+ "rohit "+Errormessage,Toast.LENGTH_LONG).show();
            if (!IsError) {


                if (Errormessage.equals("successful")) {

                    Toast.makeText(Forgetactivity.this, "Check your register Email " + Errormessage, Toast.LENGTH_SHORT).show();
                                    finish();
                           /* AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                            editor.putString(AppsContants.L_ID, UserRegisterID);
                            editor.putString(AppsContants.LEMAIL, u_email);
                                editor.putString(AppsContants.LPASSWORD, u_pass);
                            editor.putString(AppsContants.LNAME,  u_name);
                            editor.putString(AppsContants.LLATITUDE,  u_lat);
                            editor.putString(AppsContants.LLONGITUDE,  u_lon);
                            editor.commit();
                            Toast.makeText(getActivity(),"Thanks",Toast.LENGTH_LONG).show();*/

                    //Toast.makeText(LoginActivity.this, "Your login has been"+Errormessage,Toast.LENGTH_LONG).show();


                    // Intent intent=new Intent(LoginActivity.this, HomeActivity.class);
                    //  startActivity(intent);
                    //finish();

                          /*  Fragment fragment = null;
            		        fragment = new MainFragment();
            		        //FragmentManager manager = getSupportFragmentManager();
            		        FragmentTransaction transaction = getFragmentManager().beginTransaction();
            		        transaction.replace(R.id.output, fragment);
            		        transaction.commit();*/


                } else {
                    Toast.makeText(Forgetactivity.this, "please enter correct email", Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(Forgetactivity.this, "please try again...", Toast.LENGTH_SHORT).show();


            }
        }

    }

}


































/*

    TextView requestbtn,canclebtn;
    String s_emaillogin;
    ImageView  cross;
    EditText emailforget;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetactivity);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        canclebtn=(TextView)findViewById(R.id.canclebtn);
        requestbtn=(TextView)findViewById(R.id.requestbtn);
        //  cross=(ImageView)findViewById(R.id.cross);
//        cross.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });



        emailforget=(EditText)findViewById(R.id.emailforget);
        requestbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(getApplication(),"hello",Toast.LENGTH_SHORT).show();

                s_emaillogin = emailforget.getText().toString().trim();

                if (NetConnection.isConnected(Forgetactivity.this)) {
                    //progressBar.setVisibility(View.VISIBLE);


                    forgatejsontask task = new forgatejsontask();
                    task.execute();

                } else {


                    Toast.makeText(Forgetactivity.this, "Internet connection error", Toast.LENGTH_LONG).show();

                }
            }
        });
        canclebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }




public class forgatejsontask extends AsyncTask<String, Void, String> {

    String Errormessage;
    //String UserRegisterID;
    boolean IsError = false;
    String result = "";
    String u_email;
    String u_pass;
    String u_lat;
    String u_lon;
    String u_name;

    @Override

    protected void onPreExecute() {
        super.onPreExecute();
        // progressBarforget.setVisibility(View.VISIBLE);
        // loader.setVisibility(View.VISIBLE);
    }

    protected String doInBackground(String... arg0) {

        HttpClient htppclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://360educating.com/Android/process.php?action=forget");
        try {
            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();


            nameValuePair.add(new BasicNameValuePair("email", s_emaillogin));
            //nameValuePair.add(new BasicNameValuePair("password",s_loginpassword));

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));


            HttpResponse response = htppclient.execute(httppost);
            String object = EntityUtils.toString(response.getEntity());
            System.out.println("#####object registration=" + object);
            JSONObject js = new JSONObject(object);

                */
/*if (js.has("id")) {
                    UserRegisterID = js.getString("id");
                    u_email = js.getString("email");
                    u_pass = js.getString("password");
                    u_name = js.getString("username");

                    Errormessage = js.getString("result");
                    //user_mobile=js.getString("number");

                } else *//*

            if (js.has("result")) {
                Errormessage = js.getString("result");

            }

        } catch (Exception e) {
            IsError = true;
            Log.v("22", "22" + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


    protected void onPostExecute(String result1) {
        super.onPostExecute(result1);
        // loader.setVisibility(View.GONE);
        //progressBarforget.setVisibility(View.GONE);
        //Toast.makeText(Register.this,UserRegisterID+ "rohit "+Errormessage,Toast.LENGTH_LONG).show();
        if (!IsError) {


            if (Errormessage.equals("successful")) {

                Toast.makeText(Forgetactivity.this, "Check your register Email " + Errormessage, Toast.LENGTH_SHORT).show();
                finish();
                           */
/* AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                            editor.putString(AppsContants.L_ID, UserRegisterID);
                            editor.putString(AppsContants.LEMAIL, u_email);
                                editor.putString(AppsContants.LPASSWORD, u_pass);
                            editor.putString(AppsContants.LNAME,  u_name);
                            editor.putString(AppsContants.LLATITUDE,  u_lat);
                            editor.putString(AppsContants.LLONGITUDE,  u_lon);
                            editor.commit();
                            Toast.makeText(getActivity(),"Thanks",Toast.LENGTH_LONG).show();*//*


                //Toast.makeText(LoginActivity.this, "Your login has been"+Errormessage,Toast.LENGTH_LONG).show();


                // Intent intent=new Intent(LoginActivity.this, HomeActivity.class);
                //  startActivity(intent);
                //finish();

                          */
/*  Fragment fragment = null;
            		        fragment = new MainFragment();
            		        //FragmentManager manager = getSupportFragmentManager();
            		        FragmentTransaction transaction = getFragmentManager().beginTransaction();
            		        transaction.replace(R.id.output, fragment);
            		        transaction.commit();*//*



            } else {
                Toast.makeText(Forgetactivity.this, "please enter correct email", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(Forgetactivity.this, "please try again...", Toast.LENGTH_SHORT).show();


        }
    }

}

}
*/
