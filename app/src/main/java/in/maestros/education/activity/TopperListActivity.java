package in.maestros.education.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import in.maestros.education.R;
import in.maestros.education.adapter.TopperlistAdapter;
import in.maestros.education.model.TopperlistResultModel;
import in.maestros.education.other.AppsContants;

public class TopperListActivity extends AppCompatActivity {
    private ImageView backarrow;
    private RecyclerView mRecyclerView;
    private ArrayList<TopperlistResultModel> products;
    private ConnectionDetector cd;
    private ProgressDialog pDialog;
    static JSONArray contactsstate = null;
    private String a2 = "", a4 = "",a6="";
    private TopperlistAdapter cAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topper_list);

        products = new ArrayList<TopperlistResultModel>();
        mRecyclerView = (RecyclerView) findViewById(R.id.listschoolrecycle);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        mRecyclerView.setHasFixedSize(true);

        TopperlistResultModel("");

        backarrow = (ImageView) findViewById(R.id.back);
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                editor1.putString(AppsContants.TOPPERNAME, a2);
                editor1.putString(AppsContants.TOPPERCLASS, a4);
                editor1.putString(AppsContants.TOPPERSCORE, a6);
                editor1.commit();

                finish();
            }
        });

    }

    public void TopperlistResultModel(String str_com_id) {
        cd = new ConnectionDetector(TopperListActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(TopperListActivity.this, "noNetworkConnection", Toast.LENGTH_LONG).show();
        } else {
            new Followerjasontask(str_com_id).execute();
        }

    }

    private class Followerjasontask extends AsyncTask<String, Void, String> {
        String jsonStr;
        String result = "";
        String jsoprofile;


        public Followerjasontask(String strprofile) {
            this.jsoprofile = strprofile;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(TopperListActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);

        }


        @Override
        protected String doInBackground(String... voids) {
            // TODO Auto-generated method stub
            HttpClient client = new DefaultHttpClient();
            HttpGet post = new HttpGet("http://360educating.com/Android/process.php?action=top_score");


            try {

                HttpResponse response = client.execute(post);
                jsonStr = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + jsonStr);
                contactsstate = new JSONArray(jsonStr);

                for (int i = 0; i < contactsstate.length(); i++) {
                    JSONObject c = contactsstate.getJSONObject(i);
                    String a1 = c.getString("user_id");
                    String a2 = c.getString("fname");
                    String a3 = c.getString("lname");
                    String a4 = c.getString("class");
                    String a5 = c.getString("test_time");
                    String a6 = c.getString("score");

                    products.add(new TopperlistResultModel("", a2, "", a4, "", a6));
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

         //   Toast.makeText(TopperListActivity.this, jsonStr + "hello", Toast.LENGTH_SHORT).show();

            cAdapter = new TopperlistAdapter(TopperListActivity.this,products);
            mRecyclerView.setAdapter(cAdapter);

        }
    }

}
