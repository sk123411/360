package in.maestros.education.activity.ParentDeshBoard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.HttpUrlPath;
import in.maestros.education.activity.ParentDeshBoard.HomeWorkAdapter.HomeWorkAdapter;
import in.maestros.education.activity.ParentDeshBoard.HomeWorkAdapter.HomeWorkGetSet;
import in.maestros.education.activity.ParentDeshBoard.ViewAttendanceAdapter.ViewAttendanceAdapter;
import in.maestros.education.activity.ParentDeshBoard.ViewAttendanceAdapter.ViewAttendanceGetSet;
import in.maestros.education.other.AppsContants;

public class HomeWork extends AppCompatActivity {



    String userid="",L_ID="",strName="",Name11="",Name12="",Name13="";




    TextView txtName,txtClass,txtSubject,txtDate,txtAction;

    ProgressBar progressBar;

    RecyclerView recyclerview1;
    List<HomeWorkGetSet> arrayList;
    HomeWorkAdapter adapter;



    ImageView back;

    RecyclerView.LayoutManager layoutManagerOfrecyclerView;
    RecyclerView.Adapter recyclerViewadapter;


    LinearLayoutManager linearLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work);

        txtName= findViewById(R.id.txtName);
        txtClass= findViewById(R.id.txtClass);
        txtSubject= findViewById(R.id.txtSubject);
        txtDate= findViewById(R.id.txtDate);
        progressBar= findViewById(R.id.progressBar);
        // txtAction= findViewById(R.id.txtAction);
        recyclerview1 = (RecyclerView) findViewById(R.id.recyclerview1);

       /* txtAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Result.this, MyResultDetails.class));
            }
        });*/

        back= findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeWork.this,ParentDeshBoard.class));
            }
        });
        ShowHomeWork();


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");



        layoutManagerOfrecyclerView = new LinearLayoutManager(HomeWork.this);
        recyclerview1.setLayoutManager(layoutManagerOfrecyclerView);

        Log.e("gfjhgdfjgdfjh",L_ID);







    }


    public void ShowHomeWork() {

        progressBar.setVisibility(View.VISIBLE);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");



        Log.e("gjfghjhgkjhfgjkhrrr",L_ID);
        AndroidNetworking.post("https://360educated.com/Android/process.php?action=parnt_view_homewrk")
                .addBodyParameter("user_id", L_ID)
                .setTag("Categories")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {



                            arrayList = new ArrayList<>();
                            progressBar.setVisibility(View.GONE);

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Log.e("jasgterrr", response.getJSONObject(i) + "");


                                HomeWorkGetSet GetSet = new HomeWorkGetSet();
                                GetSet.setId(jsonObject.getString("id"));
                                GetSet.setHMClass(jsonObject.getString("class"));
                                GetSet.setSubject(jsonObject.getString("subject"));
                                GetSet.setName(jsonObject.getString("student_name"));
                                GetSet.setHomeWork(jsonObject.getString("home_work"));
                                GetSet.setHMDate(jsonObject.getString("hw_date"));
                                arrayList.add(GetSet);
                                Log.e("dfhjhsjh", jsonObject.getString("id"));






                            }
                            adapter = new HomeWorkAdapter(arrayList, HomeWork.this);
                            recyclerview1.setAdapter(adapter);
                            progressBar.setVisibility(View.GONE);


                        } catch (Exception ex) {
                            progressBar.setVisibility(View.GONE);

                            Log.e("tracing", ex.getMessage());
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressBar.setVisibility(View.GONE);

                        Log.e("tracing", anError.toString());


                    }
                });
    }
}
