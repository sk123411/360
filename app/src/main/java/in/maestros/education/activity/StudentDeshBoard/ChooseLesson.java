package in.maestros.education.activity.StudentDeshBoard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.Subject;

import in.maestros.education.R;
import in.maestros.education.activity.StudentDashBoard;
import in.maestros.education.other.AppsContants;

public class ChooseLesson extends AppCompatActivity {


    Spinner spin;
    String strLession;

    String strSubject = "", strId = "";
    Button choose_button;
    ProgressBar progressBar;

    ImageView back;

    List<String> arrayListId;
    List<String> arrayListLessonName;
    List<String> subjectListID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_lesson);
        spin = (Spinner) findViewById(R.id.spin);
        choose_button = (Button) findViewById(R.id.choose_button);
        progressBar = findViewById(R.id.progressBar);



        ShowLesson();

        ImageView backImage = findViewById(R.id.back);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(getApplicationContext(),StudentDashBoard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });


        choose_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(ChooseLesson.this, QuizActivity.class);

                startActivity(intent);


                // startActivity(new Intent(ChooseLesson.this,QuizActivity.class));


            }
        });

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                strId = arrayListId.get(i);
                strLession = arrayListLessonName.get(i);
                Log.e("ghhyghygyg", strLession);

                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.Id, strId);
                editor.putString(AppsContants.Subject, strLession);

                editor.commit();


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

                Toast.makeText(ChooseLesson.this, "Admin[position]", Toast.LENGTH_SHORT).show();

            }
        });


    }

    public void ShowLesson() {

        progressBar.setVisibility(View.VISIBLE);
        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=show_lesson")
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        arrayListId = new ArrayList<>();
                        arrayListLessonName = new ArrayList<>();


                        try {

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonobject = response.getJSONObject(i);

                                String strId = jsonobject.getString("id");
                                String strLession = jsonobject.getString("subject_name");

                                arrayListId.add(strId);
                                arrayListLessonName.add(strLession);

                            }

                            progressBar.setVisibility(View.GONE);


                            ArrayAdapter aa = new ArrayAdapter(ChooseLesson.this, android.R.layout.simple_spinner_item, arrayListLessonName);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            //Setting the ArrayAdapter data on the Spinner
                            spin.setAdapter(aa);


                        } catch (Exception e) {

                            Log.e("hhjkkkk", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressBar.setVisibility(View.GONE);

                        Log.e("hhhhfffffflllll", anError.getMessage());

                    }
                });
    }
}












































