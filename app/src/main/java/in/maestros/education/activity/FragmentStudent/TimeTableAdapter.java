package in.maestros.education.activity.FragmentStudent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;

public class TimeTableAdapter extends RecyclerView.Adapter<TimeTableAdapter.TableViewHolder> {

    private List<TimeTableResponse> tableResponses = new ArrayList<>();

    public TimeTableAdapter(List<TimeTableResponse> tableResponses) {
        this.tableResponses = tableResponses;
    }

    @NonNull
    @Override
    public TableViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.timetable_item,parent,false);
        return new TableViewHolder(view) ;
    }

    @Override
    public void onBindViewHolder(@NonNull TableViewHolder holder, int position) {


        TimeTableResponse timeTableResponse = tableResponses.get(position);

        holder.className.setText(timeTableResponse.getClass_());
        holder.day.setText(timeTableResponse.getDay());
        holder.startTime.setText(timeTableResponse.getStartTime());
        holder.endtime.setText(timeTableResponse.getEndTime());
        holder.teacher.setText(timeTableResponse.getTeacher());
        holder.subject.setText(timeTableResponse.getSubject());




    }

    @Override
    public int getItemCount() {
        return tableResponses.size();
    }

    public class TableViewHolder extends RecyclerView.ViewHolder {

        public TextView className, subject, startTime, endtime, teacher, day;

        public TableViewHolder(@NonNull View itemView) {
            super(itemView);


            className = itemView.findViewById(R.id.txtClass);
            subject = itemView.findViewById(R.id.txtSubject);
            startTime = itemView.findViewById(R.id.txtStartTime);
            endtime = itemView.findViewById(R.id.txtEndTime);
            teacher = itemView.findViewById(R.id.txtTeacher);
            day = itemView.findViewById(R.id.txtDay);





        }
    }
}