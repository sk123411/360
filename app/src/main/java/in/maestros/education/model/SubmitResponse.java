package in.maestros.education.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmitResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("sess_id")
    @Expose
    private String sessId;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("assign_name")
    @Expose
    private String assignName;
    @SerializedName("total_qs")
    @Expose
    private String totalQs;
    @SerializedName("correct_ans")
    @Expose
    private String correctAns;
    @SerializedName("wrong_ans")
    @Expose
    private String wrongAns;
    @SerializedName("test_date")
    @Expose
    private String testDate;
    @SerializedName("test_time")
    @Expose
    private String testTime;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("topper")
    @Expose
    private String topper;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("skipped")
    @Expose
    private String skipped;
    @SerializedName("score")
    @Expose
    private String score;
    @SerializedName("correct_answer")
    @Expose
    private Object correctAnswer;
    @SerializedName("Wrong_Answer")
    @Expose
    private Integer wrongAnswer;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("time")
    @Expose
    private String time;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessId() {
        return sessId;
    }

    public void setSessId(String sessId) {
        this.sessId = sessId;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAssignName() {
        return assignName;
    }

    public void setAssignName(String assignName) {
        this.assignName = assignName;
    }

    public String getTotalQs() {
        return totalQs;
    }

    public void setTotalQs(String totalQs) {
        this.totalQs = totalQs;
    }

    public String getCorrectAns() {
        return correctAns;
    }

    public void setCorrectAns(String correctAns) {
        this.correctAns = correctAns;
    }

    public String getWrongAns() {
        return wrongAns;
    }

    public void setWrongAns(String wrongAns) {
        this.wrongAns = wrongAns;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getTestTime() {
        return testTime;
    }

    public void setTestTime(String testTime) {
        this.testTime = testTime;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTopper() {
        return topper;
    }

    public void setTopper(String topper) {
        this.topper = topper;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSkipped() {
        return skipped;
    }

    public void setSkipped(String skipped) {
        this.skipped = skipped;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Object getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(Object correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Integer getWrongAnswer() {
        return wrongAnswer;
    }

    public void setWrongAnswer(Integer wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    @Override
    public String toString() {
        return "SubmitResponse{" +
                "id='" + id + '\'' +
                ", sessId='" + sessId + '\'' +
                ", _class='" + _class + '\'' +
                ", subject='" + subject + '\'' +
                ", assignName='" + assignName + '\'' +
                ", totalQs='" + totalQs + '\'' +
                ", correctAns='" + correctAns + '\'' +
                ", wrongAns='" + wrongAns + '\'' +
                ", testDate='" + testDate + '\'' +
                ", testTime='" + testTime + '\'' +
                ", level='" + level + '\'' +
                ", topper='" + topper + '\'' +
                ", userId='" + userId + '\'' +
                ", skipped='" + skipped + '\'' +
                ", score='" + score + '\'' +
                ", correctAnswer=" + correctAnswer +
                ", wrongAnswer=" + wrongAnswer +
                ", result='" + result + '\'' +
                '}';
    }
}