package in.maestros.education.activity.StudentDeshBoard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Vibrator;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import in.maestros.education.R;
import in.maestros.education.activity.StudentDeshBoard.QuestionAdapter.QuestionAdapter;
import in.maestros.education.activity.StudentDeshBoard.QuestionAdapter.QuestionGetSet;
import in.maestros.education.activity.StudentDeshBoard.QuizAdapter.QuizAdapter;
import in.maestros.education.activity.StudentDeshBoard.QuizAdapter.QuizGetSet;
import in.maestros.education.other.AppsContants;

public class QuizActivity extends AppCompatActivity {
ImageView back;



    Button btnSubmit;

    String Name11="",Name12="",Name13="",strPageNo="";

    public static RecyclerView questionPager;
    List<QuizGetSet> arrayList;
    QuizAdapter adapter;
    ProgressBar progressBar;
    String strId="",subjectID;

    String strLession="";

    RecyclerView.LayoutManager layoutManagerOfrecyclerView;
    RecyclerView.Adapter recyclerViewadapter;


    LinearLayoutManager linearLayoutManager;

    String userid="",L_ID="",strAnsert="";
    public  TextView txtMinutes;
    public static String completedTimeText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
//       recyclerview1.setHasFixedSize(true);

        questionPager= findViewById(R.id.questionList);
        back = findViewById(R.id.back);
        txtMinutes = findViewById(R.id.txtMinutes);
        btnSubmit = findViewById(R.id.btnSubmit);

        progressBar = findViewById(R.id.progressBar);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");
        strLession=AppsContants.sharedpreferences.getString(AppsContants.Subject, "");
        strId=AppsContants.sharedpreferences.getString(AppsContants.Id, "");
        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");
        subjectID=AppsContants.sharedpreferences.getString(AppsContants.Subject, "");


        LinearLayoutManager manager =new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL,false) {
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }
        };


        questionPager.setLayoutManager(manager);

        ShowLessionQues();


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //   InsertQuiz();

                finish();

            }
        });












        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(QuizActivity.this,ChooseLesson.class));
            }
        });

        new CountDownTimer(1800000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {


                txtMinutes.setText(""+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));




                Long elapsedTime = 1800000 - millisUntilFinished;

                completedTimeText = ""+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( elapsedTime),
                        TimeUnit.MILLISECONDS.toSeconds(elapsedTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(elapsedTime)));
            }

            public void onFinish() {
                txtMinutes.setText("done!");
            }
        }.start();



        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        v.vibrate(500);










       // recyclerview1.setLayoutManager(new GridLayoutManager(this, 1));

    }




    public void InsertQuiz() {

Log.e("hgdeyuahb",strId);
Log.e("sjdhfyiogjvhj","ghg");
        AndroidNetworking.post("https://360educated.com/Android/process.php?action=insert_quiz")
                .addBodyParameter("user_id", strId)
                .addBodyParameter("subject", subjectID)
                .addBodyParameter("ques_id", ",")
                .addBodyParameter("user_ans", ",")
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {


                            String str = response.getString("result");
                            if (str.equals("success")) {

                                Toast.makeText(QuizActivity.this, "success", Toast.LENGTH_SHORT).show();

                                finish();


                            } else {


                                Toast.makeText(QuizActivity.this, "Error" + str, Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception ex) {

                            Log.e("hjsdfahfasdjhfasd", ex.getMessage());

                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("hjsdfahfasdjhfasd", anError.toString());

                    }

                });
    }




    public void ShowLessionQues() {


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");
        strLession=AppsContants.sharedpreferences.getString(AppsContants.Subject, "");
        strId=AppsContants.sharedpreferences.getString(AppsContants.Id, "");


Log.e("dgfhdfghdfgjhgf",L_ID);
Log.e("dsfjgsdtujhdfjmdh",subjectID);



        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=lesson")
                .addBodyParameter("std_id", L_ID)
                .addBodyParameter("subject_id", strId)
                .setTag("Categories")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {

                            Log.d("TESTTTTTTTTTTT", "::"+response.toString());


                            arrayList = new ArrayList<>();

                            if (response.length()>0){

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Log.e("jasgterrr", response.getJSONObject(i) + "");


                                QuizGetSet GetSet = new QuizGetSet();
                                GetSet.setId(jsonObject.getString("id"));
                                GetSet.setQuestion(jsonObject.getString("question"));
                                GetSet.setOption1(jsonObject.getString("option1"));
                                GetSet.setOption2(jsonObject.getString("option2"));
                                GetSet.setOption3(jsonObject.getString("option3"));
                                GetSet.setOption4(jsonObject.getString("option4"));
                                arrayList.add(GetSet);
                                Log.e("dfhjhsjh", jsonObject.getString("id"));



                                // progressBar.setVisibility(View.GONE);
                            }
                            adapter = new QuizAdapter(arrayList, QuizActivity.this,QuizActivity.this);
                            questionPager.setAdapter(adapter);



                        }else {

                            finish();
                            Toast.makeText(getApplicationContext(),"No question available",Toast.LENGTH_LONG).show();

                        }
                        } catch (Exception ex) {

                            Log.e("tracing", ex.getMessage());
                           //progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("tracing", anError.toString());
                       // progressBar.setVisibility(View.GONE);

                    }
                });



    }
}
