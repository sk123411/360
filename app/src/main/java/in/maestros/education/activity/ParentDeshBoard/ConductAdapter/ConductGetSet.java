package in.maestros.education.activity.ParentDeshBoard.ConductAdapter;

public class ConductGetSet {

    public String Id;
    public String TeacherName;
    public String StudentName;
    public String ConductDate;
    public String Discipline;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getConductDate() {
        return ConductDate;
    }

    public void setConductDate(String conductDate) {
        ConductDate = conductDate;
    }

    public String getDiscipline() {
        return Discipline;
    }

    public void setDiscipline(String discipline) {
        Discipline = discipline;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String Reason;
}
