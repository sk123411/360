package in.maestros.education.activity.ParentDeshBoard.MessageAdapter;

public class MessageGetSet {

    public String Id;
    public String Message;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSentbyTeacher() {
        return SentbyTeacher;
    }

    public void setSentbyTeacher(String sentbyTeacher) {
        SentbyTeacher = sentbyTeacher;
    }

    public String SentbyTeacher;
}
