package in.maestros.education.activity.StudentDeshBoard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;

import org.json.JSONObject;


import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.Result;
import in.maestros.education.other.AppsContants;
import in.maestros.education.result.ResultAdapter;
import in.maestros.education.result.ResultResponse;

public class MyResult2 extends AppCompatActivity {


    String userid = "", L_ID = "", strName = "", Name11 = "", Name12 = "", Name13 = "";


    TextView txtName, txtClass, txtSubject, txtDate, txtAction;
    RecyclerView recyclerView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_result2);

        txtName = findViewById(R.id.txtName);
        txtClass = findViewById(R.id.txtClass);
        txtSubject = findViewById(R.id.txtSubject);
        txtDate = findViewById(R.id.txtDate);
        txtAction = findViewById(R.id.txtAction);
        progressBar = findViewById(R.id.progressBar);


        recyclerView = findViewById(R.id.resultList);
        txtAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyResult2.this, MyResultDetails.class));
            }
        });




        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID = AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");
        ShowResult(L_ID);


        ImageView backImage = findViewById(R.id.back);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }


    public void ShowResult(String id) {
        progressBar.setVisibility(View.VISIBLE);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID = AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");


        Log.e("jsdiuiytwtjg", L_ID);
        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=student_view_result")
                .addBodyParameter("student_id", id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build().getAsObjectList(ResultResponse.class, new ParsedRequestListener<List<ResultResponse>>() {
            @Override
            public void onResponse(List<ResultResponse> response) {


                if (response != null) {
                    progressBar.setVisibility(View.GONE);


                    if (!response.isEmpty()) {
                        recyclerView.setLayoutManager(new LinearLayoutManager(MyResult2.this));
                        recyclerView.setAdapter(new ResultAdapter(response));
                    }
                }else {
                    ShowResult("51");
                    Toast.makeText(MyResult2.this,"Your result is not available, Please try later",Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onError(ANError anError) {
                progressBar.setVisibility(View.GONE);

            }
        });


    }
}
