package in.maestros.education.activity.StudentDeshBoard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeWorkResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("pass")
    @Expose
    private String pass;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("school_level")
    @Expose
    private Object schoolLevel;
    @SerializedName("p_fname")
    @Expose
    private String pFname;
    @SerializedName("p_lname")
    @Expose
    private String pLname;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("email1")
    @Expose
    private String email1;
    @SerializedName("phno")
    @Expose
    private String phno;
    @SerializedName("t_fname")
    @Expose
    private String tFname;
    @SerializedName("t_lname")
    @Expose
    private String tLname;
    @SerializedName("state1")
    @Expose
    private String state1;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("email2")
    @Expose
    private String email2;
    @SerializedName("phno1")
    @Expose
    private String phno1;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("dt")
    @Expose
    private String dt;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("subscription_code")
    @Expose
    private String subscriptionCode;
    @SerializedName("social_id")
    @Expose
    private String socialId;
    @SerializedName("oauth_provider")
    @Expose
    private String oauthProvider;
    @SerializedName("oauth_token")
    @Expose
    private String oauthToken;
    @SerializedName("oauth_secret_token")
    @Expose
    private String oauthSecretToken;
    @SerializedName("oauth_name")
    @Expose
    private String oauthName;
    @SerializedName("verify")
    @Expose
    private String verify;
    @SerializedName("app_id")
    @Expose
    private String appId;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("regid")
    @Expose
    private String regid;
    @SerializedName("class_name")
    @Expose
    private String className;
    @SerializedName("assign_name")
    @Expose
    private Object assignName;
    @SerializedName("subjects")
    @Expose
    private Object subjects;
    @SerializedName("document")
    @Expose
    private Object document;
    @SerializedName("date")
    @Expose
    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public Object getSchoolLevel() {
        return schoolLevel;
    }

    public void setSchoolLevel(Object schoolLevel) {
        this.schoolLevel = schoolLevel;
    }

    public String getpFname() {
        return pFname;
    }

    public void setpFname(String pFname) {
        this.pFname = pFname;
    }

    public String getpLname() {
        return pLname;
    }

    public void setpLname(String pLname) {
        this.pLname = pLname;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getPhno() {
        return phno;
    }

    public void setPhno(String phno) {
        this.phno = phno;
    }

    public String gettFname() {
        return tFname;
    }

    public void settFname(String tFname) {
        this.tFname = tFname;
    }

    public String gettLname() {
        return tLname;
    }

    public void settLname(String tLname) {
        this.tLname = tLname;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getPhno1() {
        return phno1;
    }

    public void setPhno1(String phno1) {
        this.phno1 = phno1;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getSubscriptionCode() {
        return subscriptionCode;
    }

    public void setSubscriptionCode(String subscriptionCode) {
        this.subscriptionCode = subscriptionCode;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getOauthProvider() {
        return oauthProvider;
    }

    public void setOauthProvider(String oauthProvider) {
        this.oauthProvider = oauthProvider;
    }

    public String getOauthToken() {
        return oauthToken;
    }

    public void setOauthToken(String oauthToken) {
        this.oauthToken = oauthToken;
    }

    public String getOauthSecretToken() {
        return oauthSecretToken;
    }

    public void setOauthSecretToken(String oauthSecretToken) {
        this.oauthSecretToken = oauthSecretToken;
    }

    public String getOauthName() {
        return oauthName;
    }

    public void setOauthName(String oauthName) {
        this.oauthName = oauthName;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getRegid() {
        return regid;
    }

    public void setRegid(String regid) {
        this.regid = regid;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Object getAssignName() {
        return assignName;
    }

    public void setAssignName(Object assignName) {
        this.assignName = assignName;
    }

    public Object getSubjects() {
        return subjects;
    }

    public void setSubjects(Object subjects) {
        this.subjects = subjects;
    }

    public Object getDocument() {
        return document;
    }

    public void setDocument(Object document) {
        this.document = document;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
