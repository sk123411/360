package in.maestros.education.activity.ParentDeshBoard;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import in.maestros.education.R;
import in.maestros.education.activity.SplashActivity;
import in.maestros.education.activity.StudentDashBoard;
import in.maestros.education.activity.UpdateStudentProfile;
import in.maestros.education.other.AppsContants;

public class ParentDeshBoard extends AppCompatActivity {

    CardView cardResult,cardAssignment,cardConduct,cardAttendance,cardAssesment,cardMsg,cardHomeWork;
String L_ID="",Name11="",Name12="",Name13="";

ImageView imgOption;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_desh_board);

        cardResult = findViewById(R.id.cardResult);
        cardAssignment = findViewById(R.id.cardAssignment);
        cardConduct = findViewById(R.id.cardConduct);
        cardAttendance = findViewById(R.id.cardAttendance);
        cardAssesment = findViewById(R.id.cardAssesment);
        cardMsg = findViewById(R.id.cardMsg);
        imgOption = findViewById(R.id.imgOption);
        cardHomeWork = findViewById(R.id.cardHomeWork);




        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");


        cardHomeWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentDeshBoard.this,HomeWork.class));
            }
        });
        cardMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentDeshBoard.this,Message.class));
            }
        });
        cardAssesment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentDeshBoard.this,Assesment.class));
            }
        });

        cardResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentDeshBoard.this,Result.class));
            }
        });

        cardAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentDeshBoard.this,Assignment.class));
            }
        });
        cardConduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentDeshBoard.this,Conduct.class));
            }
        });
        cardAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentDeshBoard.this,Attendance.class));
            }
        });


        imgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final PopupMenu popup = new PopupMenu(ParentDeshBoard.this,imgOption);

                popup.getMenuInflater().inflate(R.menu.home_menu,popup.getMenu());
                /*  popup.getMenuInflater().inflate(R.menu.contact_fragment_menues, popup.getMenu());*/

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(final MenuItem item) {


                        if (item.getItemId() == R.id.update_profile) {

                            startActivity(new Intent(ParentDeshBoard.this, UpdateParentProfile.class));


                        }  else if (item.getItemId() == R.id.logout) {





                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(ParentDeshBoard.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(ParentDeshBoard.this);
                            }
                            builder.setTitle(ParentDeshBoard.this.getResources().getString(R.string.app_name))
                                    .setMessage("Confirm logout?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(final DialogInterface dialog, int which) {

                                            final ProgressDialog progressDialog = new ProgressDialog(ParentDeshBoard.this);
                                            progressDialog.setMessage("Logging You Out.....");
                                            progressDialog.show();
                                            final Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                                @Override
                                                public void run() {

                                                    if (progressDialog.isShowing()) {
                                                        progressDialog.dismiss();
                                                    }
                                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                    editor.putString(AppsContants.U_ID, "");
                                                    editor.putString(AppsContants.L_ID, "");
                                                    editor.putString(AppsContants.USER_NAME, "");
                                                    editor.putString(AppsContants.LNAME, "");
                                                    editor.putString(AppsContants.EMAIL, "");
                                                    editor.putString(AppsContants.CLASS, "");
                                                    editor.commit();

                                                    Intent i = new Intent(ParentDeshBoard.this, SplashActivity.class);
                                                    startActivity(i);
                                                    finishAffinity();

                                                }
                                            }, 2000);
                                            dialog.dismiss();

                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();

                                        }
                                    })
                                    .show();





                        }

                        return true;
                    };
                });
                popup.show();
            };
        });

    }
}
