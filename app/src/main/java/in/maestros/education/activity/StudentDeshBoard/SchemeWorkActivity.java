package in.maestros.education.activity.StudentDeshBoard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.model.WorkResponse;
import in.maestros.education.other.AppsContants;

public class SchemeWorkActivity extends AppCompatActivity {

    private RecyclerView workList;
    private String sid;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheme_work);

        workList = findViewById(R.id.workList);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        progressBar = findViewById(R.id.progressBar);





        sid = AppsContants.sharedpreferences.getString(AppsContants.L_ID, "51");



        getWorkList();

        ImageView backImage = findViewById(R.id.back);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void getWorkList() {

        progressBar.setVisibility(View.VISIBLE);
        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=student_schemeof_work")
                .addBodyParameter("std_id",sid)
                .build()
                .getAsObjectList(WorkResponse.class, new ParsedRequestListener<List<WorkResponse>>() {
                    @Override
                    public void onResponse(List<WorkResponse> response) {


                        progressBar.setVisibility(View.GONE);

                        workList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        workList.setAdapter(new SchemeWorkAdapter(getApplicationContext(),response));


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }
}