package in.maestros.education.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

public class ResultRecyclerViewAdapter extends RecyclerView.Adapter<SchoolViewHolders> {

    private List<TechItemObject> itemList;
    private Context context;

    public ResultRecyclerViewAdapter(Context context, List<TechItemObject> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public SchoolViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_result, null);
        SchoolViewHolders rcv = new SchoolViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(SchoolViewHolders holder, final int position) {
       holder.countryName.setText(itemList.get(position).getName());
        holder.countryPhoto.setImageResource(itemList.get(position).getPhoto());
        holder.countryPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s=itemList.get(position).getName();

                AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.CAT_NAME, s);

                editor.commit();




          //     Intent i=new Intent(context,LevelActivity.class);
                Intent i=new Intent(context,PopupSubscription.class);
               context.startActivity(i);
            }
        });



    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}
