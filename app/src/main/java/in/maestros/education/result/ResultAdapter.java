package in.maestros.education.result;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.StudentDeshBoard.ResultDetailActivity;
import io.paperdb.Paper;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ResultViewHolder> {


    private List<ResultResponse> resultResponses;


    public ResultAdapter(List<ResultResponse> resultResponses){
        this.resultResponses = resultResponses;

    }

    @Override
    public ResultViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {

       View v =  LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.result_item,viewGroup,false);

        return new ResultViewHolder(v);
    }

    @Override
    public void onBindViewHolder( ResultViewHolder resultViewHolder, int i) {

        final ResultResponse resultResponse = resultResponses.get(i);

        resultViewHolder.classText.setText(resultResponse.getClassName());
        resultViewHolder.subjectText.setText(resultResponse.getSubject());
        resultViewHolder.nameText.setText(resultResponse.getClassName());


        resultViewHolder.dateText.setText(resultResponse.getTest_date());



        resultViewHolder.actionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Paper.init(view.getContext());
                Paper.book().write("result", resultResponse);

                view.getContext().startActivity(new Intent(view.getContext(), ResultDetailActivity.class));


            }
        });




    }

    @Override
    public int getItemCount() {
        return resultResponses.size();
    }

    class ResultViewHolder extends RecyclerView.ViewHolder{

        TextView classText, subjectText, nameText,dateText,actionText;


    public ResultViewHolder( View itemView) {
        super(itemView);

        classText = itemView.findViewById(R.id.txtClass);
        subjectText = itemView.findViewById(R.id.txtSubject);
        nameText = itemView.findViewById(R.id.txtName);
        dateText = itemView.findViewById(R.id.txtDate);
        actionText = itemView.findViewById(R.id.txtAction);

    }

}
}
