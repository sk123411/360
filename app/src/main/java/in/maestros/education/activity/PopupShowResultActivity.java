package in.maestros.education.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

public class PopupShowResultActivity extends Activity {
    private ImageView cross_icon;
    String s_total_question = "", s_correct_answere = "", s_wrong_answere = "", s_skipped_question = "", s_score = "";
    private TextView total_question,correctanswere,wronganswere,skkiped,score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_show_result);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        s_total_question = AppsContants.sharedpreferences.getString(AppsContants.TOTAL_QUESTIONS, "");
        s_correct_answere = AppsContants.sharedpreferences.getString(AppsContants.CORRECT_ANSWERE, "");
        s_wrong_answere = AppsContants.sharedpreferences.getString(AppsContants.WRONG_ANSWERE, "");
        s_skipped_question = AppsContants.sharedpreferences.getString(AppsContants.SKIPPED_QUESTION, "");
        s_score = AppsContants.sharedpreferences.getString(AppsContants.SCORE, "");

        cross_icon = (ImageView) findViewById(R.id.cross_icon);
        cross_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
