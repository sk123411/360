package in.maestros.education.activity.ParentDeshBoard.ResultAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.StudentDeshBoard.MyResultDetails;
import in.maestros.education.activity.StudentDeshBoard.QuizAdapter.QuizAdapter;
import in.maestros.education.activity.StudentDeshBoard.QuizAdapter.QuizGetSet;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder> {

    Context context;


    List<ResultGetSet> dataAdapters;


    String strCatID="",strId="",strCatName="";









    public ResultAdapter(List<ResultGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public ResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_adapter, parent, false);



        ResultAdapter.ViewHolder viewHolder = new ResultAdapter.ViewHolder(view);











        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ResultAdapter.ViewHolder Viewholder, int position) {

        final ResultGetSet dataAdapterOBJ = dataAdapters.get(position);




      //  Toast.makeText(context, ""+dataAdapterOBJ.getQuestion(), Toast.LENGTH_SHORT).show();


        Viewholder.txtClass.setText(dataAdapterOBJ.getClas());
        Viewholder.txtSubject.setText(dataAdapterOBJ.getSubject());
        Viewholder.txtName.setText(dataAdapterOBJ.getStudentName());
        Viewholder.txtDate.setText(dataAdapterOBJ.getTestDate());



Viewholder.txtAction.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        context.startActivity(new Intent(context, MyResultDetails.class));


    }
});
        //Viewholder.txtQuestion.setText(dataAdapterOBJ.getQuestion());
    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }





    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtClass,txtSubject,txtName,txtDate,txtAction;




        public ViewHolder(View itemView) {

            super(itemView);



            txtClass = itemView.findViewById(R.id.txtClass);
            txtSubject = itemView.findViewById(R.id.txtSubject);
            txtName = itemView.findViewById(R.id.txtName);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtAction = itemView.findViewById(R.id.txtAction);


        }
    }



}
