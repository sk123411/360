package in.maestros.education.model;

/**
 * Created by SONU on 25/09/15.
 */
public class SchoollistModel {

    // Getter and Setter model for recycler view items
    private String sno;
    private String schoolname;
    private String action;

    public SchoollistModel(String sno, String schoolname, String action) {


        this.sno=sno;
        this.schoolname = schoolname;
        this.action=action;

    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getSchoolname() {
        return schoolname;
    }

    public void setSchoolname(String schoolname) {
        this.schoolname = schoolname;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
