package in.maestros.education.activity;

/**
 * Created by Exoguru on 23/01/2015.
 */
public class NatureQuesAns {
    private String mAid;
    private String mAname;
    private String mQtype;
    private String mQue;
    private String mUpload;
    private String mOption1;
    private String mOption2;
    private String mOption3;
    private String mOption4;


    private String mOption1img;
    private String mOption2img;
    private String mOption3img;
    private String mOption4img;

    private String mAns;
    private String mAnsimg;
    private String mUserAns;




    public String getAid() {
        return mAid;
    }

    public void setAid(String aid) {
        this.mAid = aid;
    }


    public String getAname() {
        return mAname;
    }

    public void setAname(String aname) {
        this.mAname = aname;
    }



    public String getQtype() {
        return mQtype;
    }

    public void setQtype(String qtype) {
        this.mQtype = qtype;
    }

    public String getQue() {
        return mQue;
    }

    public void setQue(String que) {
        this.mQue = que;
    }



    public String getUpload() {
        return mUpload;
    }
    public void setUpload(String Upload) {
        this.mUpload = Upload;
    }


    public String getOption1() {
        return mOption1;
    }
    public void setOption1(String Option1) {
        this.mOption1 = Option1;
    }

    public String getOption2() {
        return mOption2;
    }
    public void setOption2(String Option2) {
        this.mOption2 = Option2;
    }

    public String getOption3() {
        return mOption3;
    }
    public void setOption3(String Option3) {
        this.mOption3 = Option3;
    }
    public String getOption4() {
        return mOption4;
    }
    public void setOption4(String Option4) {
        this.mOption4 = Option4;
    }





    public String getOption1img() {
        return mOption1img;
    }
    public void setOption1img(String Option1img) {
        this.mOption1img = Option1img;
    }

    public String getOption2img() {
        return mOption2img;
    }
    public void setOption2img(String Option2img) {
        this.mOption2img = Option2img;
    }

    public String getOption3img() {
        return mOption3img;
    }

    public void setOption3img(String Option3img) {
        this.mOption3img = Option3img;
    }

    public String getOption4img() {
        return mOption4img;
    }

    public void setOption4img(String Option4img) {
        this.mOption4img = Option4img;
    }



    public String getAns() {
        return mAns;
    }

    public void setAns(String Ans) {
        this.mAns = Ans;}


    public String getAnsimg() {
        return mAnsimg;
    }
    public void setAnsimg(String Ansimg) {
        this.mAnsimg = Ansimg;}


    public String getUserAns() {
        return mUserAns;
    }
    public void setUserAns(String UserAns) {
        this.mUserAns = UserAns;}










}
