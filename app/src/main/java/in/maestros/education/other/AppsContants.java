package in.maestros.education.other;

import android.content.SharedPreferences;

public class AppsContants {


    public static final String S_ID = "sssssssssssid";
    public static final String C_LEVEL = "ccccccccc_level" ;
    public static SharedPreferences sharedpreferences;
    public static final String L_ID = "lidKey";

    public static final String U_ID = "uidKey";
    public static final String USER_NAME = "usernamelKey";
    public static final String PROFILEIMAGE = "PROFILEIMAGEKey";
    public static final String CORRECT_ANSWERE = "CORRECT_ANSWEREKey";
    public static final String TOTAL_QUESTIONS = "TOTAL_QUESTIONSKey";
    public static final String WRONG_ANSWERE = "WRONG_ANSWEREKey";
    public static final String SKIPPED_QUESTION = "SKIPPED_QUESTIONSKey";
    public static final String SCORE = "SCOREKey";
    public static final String TOPPERNAME = "TOPPERNAMEKey";
    public static final String TOPPERCLASS = "TOPPERCLASSKey";
    public static final String TOPPERSCORE = "TOPPERSCOREKey";
    public static final String subject_id = "subject_idKey";
    public static final String LEVEL_ID = "LEVEL_IDKey";
    public static final String LEVEL = "LEVELKey";
    public static final String Dob = "Dob";
    public static final String Image = "Image";
    public static final String FName = "FName";
    public static final String Subject = "Subject";
    public static final String Id = "Id";
    public static final String AttendanceId = "AttendanceId";

    public static SharedPreferences getSharedpreferences() {
        return sharedpreferences;
    }

    public static void setSharedpreferences(SharedPreferences sharedpreferences) {
        AppsContants.sharedpreferences = sharedpreferences;
    }

    public static String getlId() {
        return L_ID;
    }

    public static String getuId() {
        return U_ID;
    }

    public static String getUserName() {
        return USER_NAME;
    }

    public static String getPROFILEIMAGE() {
        return PROFILEIMAGE;
    }

    public static String getCorrectAnswere() {
        return CORRECT_ANSWERE;
    }

    public static String getTotalQuestions() {
        return TOTAL_QUESTIONS;
    }

    public static String getWrongAnswere() {
        return WRONG_ANSWERE;
    }

    public static String getSkippedQuestion() {
        return SKIPPED_QUESTION;
    }

    public static String getSCORE() {
        return SCORE;
    }

    public static String getTOPPERNAME() {
        return TOPPERNAME;
    }

    public static String getTOPPERCLASS() {
        return TOPPERCLASS;
    }

    public static String getTOPPERSCORE() {
        return TOPPERSCORE;
    }

    public static String getSubject_id() {
        return subject_id;
    }

    public static String getLevelId() {
        return LEVEL_ID;
    }

    public static String getLEVEL() {
        return LEVEL;
    }

    public static String getDob() {
        return Dob;
    }

    public static String getGender() {
        return Gender;
    }

    public static String getSTATUS() {
        return STATUS;
    }

    public static String getSUBJECT() {
        return SUBJECT;
    }

    public static String getID() {
        return ID;
    }

    public static String getLNAME() {
        return LNAME;
    }

    public static String getMyPREFERENCES() {
        return MyPREFERENCES;
    }

    public static String getCatId() {
        return CAT_ID;
    }

    public static String getSubjectName() {
        return SUBJECT_NAME;
    }

    public static String getSubjectId() {
        return SUBJECT_ID;
    }

    public static String getCatName() {
        return CAT_NAME;
    }

    public static String getCLASS() {
        return CLASS;
    }

    public static String getSubId() {
        return SUB_ID;
    }

    public static String getSubName() {
        return SUB_NAME;
    }

    public static String getSubAdd() {
        return SUB_ADD;
    }

    public static String getSubImage() {
        return SUB_IMAGE;
    }

    public static String getSCHOOLNAME() {
        return SCHOOLNAME;
    }

    public static String getSCHOOLLEVEL() {
        return SCHOOLLEVEL;
    }

    public static String getSTATENAME() {
        return STATENAME;
    }

    public static String getAssiId() {
        return ASSI_ID;
    }

    public static String getClassId() {
        return CLASS_ID;
    }

    public static String getEMAIL() {
        return EMAIL;
    }

    public static String getCONTACT() {
        return CONTACT;
    }

    public static String getFacebookProfile() {
        return FACEBOOK_PROFILE;
    }

    public static final String Gender = "Gender";


    public static final String STATUS = "STATUSKey";
    public static final String SUBJECT = "SUBJECTKey";
    public static final String ID = "IDKey";
    public static final String LNAME = "lnameKey";
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String CAT_ID = "catidKey";
    public static final String SUBJECT_NAME = "subject_nameKey";
    public static final String SUBJECT_ID = "SUBJECT_IDKey123";
    public static final String CAT_NAME = "catnameKey";
    public static final String CLASS = "clsKey";
    public static final String SUB_ID = "subidKey";
    public static final String SUB_NAME = "subnameKey";
    public static final String SUB_ADD = "subaddKey";
    public static final String SUB_IMAGE = "subimageKey";
    public static final String SCHOOLNAME = "SCHOOLNAMEKey";
    public static final String SCHOOLLEVEL = "SCHOOLLEVELKey";
    public static final String STATENAME = "STATENAMEKey";
    public static final String ASSI_ID = "ASSI_IDKey";
    public static final String CLASS_ID = "CLASS_ID";

    public static final String EMAIL = "emailKey";
    public static final String CONTACT = "contactsKey";
    public static final String Social_id = "Social_id";
    public static final String Status = "Status";

    public static final String FACEBOOK_PROFILE = "FACEBOOK_PROFILEKEY";




}
