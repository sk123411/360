package in.maestros.education.activity;

/**
 * Created by Ravi on 29/07/15.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.adapter.ResultAdapter;
import in.maestros.education.model.ResultModel;
import in.maestros.education.other.AppsContants;


public class ResultFragment extends Fragment {

    private LinearLayoutManager lLayout;
    private ImageView backarrow;
    private RecyclerView mRecyclerView;
    private ArrayList<ResultModel> products;
    private ConnectionDetector cd;
    private ProgressDialog pDialog;
    static JSONArray contactsstate = null;
    private String Name11 = "", schoollevel = "";
    private ResultAdapter cAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_result, container, false);


        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        Name11 = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");



        products = new ArrayList<ResultModel>();
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.listschoolrecycle);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        mRecyclerView.setHasFixedSize(true);

        ResultModel("");

        return rootView;
    }

    private List<TechItemObject> getAllItemList(){

        List<TechItemObject> allItems = new ArrayList<TechItemObject>();
        allItems.add(new TechItemObject("Maths",R.drawable.s1));
        allItems.add(new TechItemObject("English",R.drawable.s2));
        allItems.add(new TechItemObject("Physics",R.drawable.physics));
        allItems.add(new TechItemObject("Chemistry",R.drawable.chemistry));
        allItems.add(new TechItemObject("Biology",R.drawable.dna_concept));
        allItems.add(new TechItemObject("Sociology",R.drawable.timthumb));


        return allItems;
    }

    public void ResultModel(String str_com_id) {
        cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "noNetworkConnection", Toast.LENGTH_LONG).show();
        } else {
            new Followerjasontask(str_com_id).execute();
        }

    }

    private class Followerjasontask extends AsyncTask<String, Void, String> {
        String jsonStr;
        String result = "";
        String jsoprofile;


        public Followerjasontask(String strprofile) {
            this.jsoprofile = strprofile;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);

        }


        @Override
        protected String doInBackground(String... voids) {
            // TODO Auto-generated method stub
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://360educating.com/Android/process.php?action=show_user_class");


            try {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

                nameValuePair.add(new BasicNameValuePair("id",Name11));



                post.setEntity(new UrlEncodedFormEntity(nameValuePair));


                HttpResponse response = client.execute(post);
                jsonStr = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + jsonStr);
                contactsstate = new JSONArray(jsonStr);

                for (int i = 0; i < contactsstate.length(); i++) {
                    JSONObject c = contactsstate.getJSONObject(i);
                    String a1 = c.getString("id");
                    String a2 = c.getString("cat_name");
                    String a3 = c.getString("status");

                    products.add(new ResultModel(a1, a2, a3));
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //   Toast.makeText(TopperListActivity.this, jsonStr + "hello", Toast.LENGTH_SHORT).show();

            cAdapter = new ResultAdapter(getActivity(),products);
            mRecyclerView.setAdapter(cAdapter);

        }
    }


}
