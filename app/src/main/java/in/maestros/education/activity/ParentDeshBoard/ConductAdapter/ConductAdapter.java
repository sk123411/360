package in.maestros.education.activity.ParentDeshBoard.ConductAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentAdapter;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentGetSet;

public class ConductAdapter extends RecyclerView.Adapter<ConductAdapter.ViewHolder> {

    Context context;


    List<ConductGetSet> dataAdapters;


    String strCatID="",strId="",strCatName="";









    public ConductAdapter(List<ConductGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public ConductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.conduct_adapter, parent, false);



        ConductAdapter.ViewHolder viewHolder = new ConductAdapter.ViewHolder(view);











        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ConductAdapter.ViewHolder Viewholder, int position) {

        final ConductGetSet dataAdapterOBJ = dataAdapters.get(position);




        // Toast.makeText(context, ""+dataAdapterOBJ.getQuestion(), Toast.LENGTH_SHORT).show();


        Viewholder.txtStudent.setText(dataAdapterOBJ.getStudentName());
        Viewholder.txtTeacherName.setText(dataAdapterOBJ.getTeacherName());
        Viewholder.txtDate.setText(dataAdapterOBJ.getConductDate());
        Viewholder.txtReason.setText(dataAdapterOBJ.getReason());
        Viewholder.txtDiscipline.setText(dataAdapterOBJ.getDiscipline());






    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }





    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtStudent,txtTeacherName,txtDate,txtReason,txtDiscipline;




        public ViewHolder(View itemView) {

            super(itemView);



            txtStudent = itemView.findViewById(R.id.txtStudent);
            txtTeacherName = itemView.findViewById(R.id.txtTeacherName);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtReason = itemView.findViewById(R.id.txtReason);
            txtDiscipline = itemView.findViewById(R.id.txtDiscipline);



        }
    }



}

