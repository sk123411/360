package in.maestros.education.activity.ParentDeshBoard.AssignmentAdapter;

public class AssignmentGetSet {

    public String Id;
    public String Subject;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getCllass() {
        return Cllass;
    }

    public void setCllass(String cllass) {
        Cllass = cllass;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getAssignmentName() {
        return AssignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        AssignmentName = assignmentName;
    }

    public String Cllass;
    public String StudentName;
    public String AssignmentName;
}
