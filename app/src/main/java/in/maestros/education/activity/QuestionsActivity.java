package in.maestros.education.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import in.maestros.education.R;

public class QuestionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
    }
}
