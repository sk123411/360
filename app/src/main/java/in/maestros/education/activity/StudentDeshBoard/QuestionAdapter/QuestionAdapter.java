package in.maestros.education.activity.StudentDeshBoard.QuestionAdapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {

    Context context;


    List<QuestionGetSet> dataAdapters;

    String strCatID="",strId="",strCatName="";


    public QuestionAdapter(List<QuestionGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_adapter, parent, false);


        ViewHolder viewHolder = new ViewHolder(view);






        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, int position) {





        final QuestionGetSet dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.txtNumbering.setText(String.valueOf(position+1));




         Viewholder.txtQuestion.setText(dataAdapterOBJ.getQuestion());
    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtQuestion,txtNumbering;
        public ImageView imgCat;
        public CardView cardCat;

        public ViewHolder(View itemView) {

            super(itemView);


            cardCat = itemView.findViewById(R.id.cardCat);
            txtQuestion = itemView.findViewById(R.id.txtQuestion);
            txtNumbering = itemView.findViewById(R.id.txtNumbering);

        }
    }
}
