package in.maestros.education.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;
import in.maestros.education.other.Base64Decode;


public class StProfileActivity extends AppCompatActivity {

    List<String> categories = new ArrayList<String>();
    List<String> categories1 = new ArrayList<String>();
    static JSONArray contactsstate1 = null;

    public  static EditText et_dob;

    EditText fname,lname,eml_id;
    String Name11,Name12,Name13,sgen,scls,sdob,L_ID;

    Button nxt;
    Spinner gen_spin,cls_spin;

    private ProgressDialog dialog = null;

    TextView tv_dob;
    ImageView img_img,back;
    ProgressBar progressBar;
    //------for image----
    String userImageBitmap = "";
    int imagegallery = 1;
    int imageCamera = 2;
    int CropreImage1 = 3;
    public Uri mImageCaptureUri;
    double currentLatitude = 22.7253;
    double currentLongitude = 75.8655;
    String pathi = "";
    private String upLoadServerUri = null;
    private int serverResponseCode = 0;
    String s_result="";

    private static final String CHATTING_TEMP_PATH_FROM_CAMERA = Environment
            .getExternalStorageDirectory()
            + File.separator
            + "/captured_temp_image.jpg";
    public static Bitmap photo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st_profile);

        progressBar=(ProgressBar)findViewById(R.id.progressBar1);
        progressBar.setVisibility(View.GONE);

        et_dob=(EditText) findViewById(R.id.et_dob);
        tv_dob=(TextView) findViewById(R.id.tv_dob);

        fname=(EditText) findViewById(R.id.fname);
        lname=(EditText) findViewById(R.id.lname);
        eml_id=(EditText) findViewById(R.id.eml_id);
        nxt=(Button) findViewById(R.id.nxt);
        gen_spin = (Spinner)findViewById(R.id.gen_spin);
        cls_spin = (Spinner)findViewById(R.id.cls_spin);
        img_img = (ImageView)findViewById(R.id.img_img);
        back = (ImageView)findViewById(R.id.back);




        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");


        Log.e("jfgieurgjd",L_ID);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (NetConnection.isConnected(StProfileActivity.this)) {

            SignUpjsontask11 task = new SignUpjsontask11();
            task.execute();

        }
        else {

            Toast.makeText(StProfileActivity.this,"Internet connection error",Toast.LENGTH_LONG).show();
        }


        cls_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                String item = adapterView.getItemAtPosition(i).toString();

                // Showing selected spinner item
                Toast.makeText(StProfileActivity.this, "Selected: " + item, Toast.LENGTH_LONG).show();

                scls=categories1.get(i);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


      // upLoadServerUri = "http://maestrossoftware.com/WEBSOFT/education/Android/process.php?action=personal_profile";
        upLoadServerUri=  HttpUrlPath.urlPath+HttpUrlPath.actionPersonalProfile;

        fname.setText(Name11);
        lname.setText(Name12);
        eml_id.setText(Name13);

        et_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent i = new Intent(StProfileActivity.this, DatePickerActivity.class);
                startActivity(i);
            }
        });

        img_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, imagegallery);
                //dialog.dismiss();

               // gotoAddProfileImage(imagegallery, imageCamera);
            }
        });
        nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             sgen = gen_spin.getSelectedItem().toString();
               // scls = cls_spin.getSelectedItem().toString();
                sdob = et_dob.getText().toString();

                boolean isError = false;

                if (sdob.equals("")) {
                    isError = true;
                    et_dob.requestFocus();
                    Toast.makeText(StProfileActivity.this, "Please enter DOB", Toast.LENGTH_SHORT).show();

                }/*else if (pathi.equals("")){

                    Toast.makeText(StProfileActivity.this, "Please select image", Toast.LENGTH_SHORT).show();
                }*/else {

                    dialog = ProgressDialog.show(StProfileActivity.this, "",
                            "Uploading file..."+pathi, true);

                    new Thread(new Runnable() {
                        public void run() {
                            uploadFile(pathi);
                        }
                    }).start();

                }



               Intent i = new Intent(StProfileActivity.this,StudentDashBoard.class);
                startActivity(i);
            }
        });
    }


//------------------start get and set image from camera and gallery---------------------
    public void gotoAddProfileImage(final int galleryvalue,
                                    final int Cameravalue) {
        final Dialog dialog = new Dialog(StProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.upload_pic_alert_view);
        dialog.setTitle("Upload Image");

        TextView gallerytext = (TextView) dialog
                .findViewById(R.id.upload_gallery);
        TextView camera = (TextView) dialog.findViewById(R.id.take_camera);

        gallerytext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, galleryvalue);
                dialog.dismiss();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    clearChacheFile();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    mImageCaptureUri = Uri.fromFile(new File(
                            CHATTING_TEMP_PATH_FROM_CAMERA));
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                    try {
                        intent.putExtra("return-data", true);
                        startActivityForResult(intent, Cameravalue);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                    dialog.dismiss();
                } catch (Exception e) {

                }

            }
        });

        dialog.show();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == imagegallery && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            img_img.setImageURI(selectedImage);
            Bitmap bm=((BitmapDrawable)img_img.getDrawable()).getBitmap();
            SaveImage(bm);




            //doCrop(CropreImage1, selectedImage);
        }
        if (requestCode == CropreImage1 && resultCode == RESULT_OK
                && null != data) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                photo = extras.getParcelable("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.PNG, 10, stream);
                byte[] byteArray = stream.toByteArray();
                userImageBitmap = Base64Decode.encodeBytes(byteArray);
                Bitmap circleBitmap = Bitmap.createBitmap(photo.getWidth(),
                        photo.getHeight(), Bitmap.Config.ARGB_8888);
                BitmapShader shader = new BitmapShader(photo, Shader.TileMode.CLAMP,
                        Shader.TileMode.CLAMP);
                Paint paint = new Paint();
                paint.setShader(shader);

                Canvas c = new Canvas(circleBitmap);
                c.drawCircle(photo.getWidth() / 2, photo.getHeight() / 2,
                        photo.getWidth() / 2, paint);

                img_img.setImageBitmap(circleBitmap);


                Toast.makeText(StProfileActivity.this, circleBitmap+"", Toast.LENGTH_LONG).show();

                Bitmap bm=((BitmapDrawable)img_img.getDrawable()).getBitmap();
                SaveImage(bm);


                String userImage = BitMapToString(photo);
            }
        }
        if (requestCode == imageCamera && resultCode == RESULT_OK) {
            doCrop(CropreImage1, mImageCaptureUri);

        }

    }

    private void clearChacheFile() {
        if (mImageCaptureUri != null) {

            File f = new File(CHATTING_TEMP_PATH_FROM_CAMERA);
            if (f.exists()) {
                Log.d("File_Deleted", "file deleted");
                f.delete();
            }

        }
    }

    private void doCrop(final int FROM_CAMERA_OR_GALLERY, final Uri uri) {
        final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
        /**
         * Open image crop app by starting an intent
         * ‘com.android.camera.action.CROP‘.
         */
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        /**
         * Check if there is image cropper app installed.
         */
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(
                intent, 0);

        int size = list.size();

        /**
         * If there is no image cropper app, display warning message
         */
        if (size == 0) {

            Toast.makeText(this, "Can not find image crop app",
                    Toast.LENGTH_SHORT).show();

            return;
        } else {
            /**
             * Specify the image path, crop dimension and scale
             */
            intent.setData(uri);

            intent.putExtra("outputX", 200);
            intent.putExtra("outputY", 200);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);
            intent.putExtra("return-data", true);
            /**
             * There is posibility when more than one image cropper app exist,
             * so we have to check for it first. If there is only one app, open
             * then app.
             */

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName,
                        res.activityInfo.name));

                startActivityForResult(i, FROM_CAMERA_OR_GALLERY);
            } else {
                /**
                 * If there are several app exist, create a custom chooser to
                 * let user selects the app.
                 */
                for (ResolveInfo res : list) {
                    final CropOption co = new CropOption();

                    co.title = getPackageManager().getApplicationLabel(
                            res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(
                            res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);

                    co.appIntent
                            .setComponent(new ComponentName(
                                    res.activityInfo.packageName,
                                    res.activityInfo.name));

                    cropOptions.add(co);
                }

               CropOptionAdapter adapter = new CropOptionAdapter(
                        getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Crop App");
                builder.setAdapter(adapter,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                startActivityForResult(
                                        cropOptions.get(item).appIntent,
                                        FROM_CAMERA_OR_GALLERY);
                            }
                        });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        if (uri != null) {
                            getContentResolver().delete(uri, null, null);
                            // uri = null;
                        }
                    }
                });

                AlertDialog alert = builder.create();

                alert.show();
            }
        }
    }

    public class CropOption {
        public CharSequence title;
        public Drawable icon;
        public Intent appIntent;
    }

    public class CropOptionAdapter extends ArrayAdapter<CropOption> {
        private ArrayList<CropOption> mOptions;
        private LayoutInflater mInflater;

        public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
            super(context, R.layout.crop_selector, options);

            mOptions = options;

            mInflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup group) {
            if (convertView == null)
                convertView = mInflater.inflate(R.layout.crop_selector, null);

            CropOption item = mOptions.get(position);

            if (item != null) {
                ((ImageView) convertView.findViewById(R.id.iv_icon))
                        .setImageDrawable(item.icon);
                ((TextView) convertView.findViewById(R.id.tv_name))
                        .setText(item.title);

                return convertView;
            }

            return null;
        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    //-----------------------------end---------------------------------------------------------------


    //-------------save image to path------------

    private void SaveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists()) file.delete ();
        pathi = root + "/saved_images/"+fname;
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //----------------------------------------end----------------------------


    //-------------------------upload-start-----------------------------------

    public int uploadFile(final String sourceFileUri) {


        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        if (!sourceFile.isFile()) {

            dialog.dismiss();

            Log.e("uploadFile", "Source File not exist :" + sourceFileUri);

            runOnUiThread(new Runnable() {
                public void run() {
                    // timerValue.setText("Source File not exist :" + sourceFileUri);
                }
            });

            return 0;

        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(
                        sourceFile);
                URL url = new URL(upLoadServerUri);
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type","multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", fileName);

                conn.setRequestProperty("rohit", "mahajan");


                dos = new DataOutputStream(conn.getOutputStream());
                //---------------------start-----------------


                dos.writeBytes(twoHyphens + boundary + lineEnd);

                String dob = sdob;

                // Send parameter #name
                dos.writeBytes("Content-Disposition: form-data; name=\"dob\"" + lineEnd);
                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + lineEnd);
                dos.writeBytes("Content-Length: " + dob.length() + lineEnd);
                dos.writeBytes(lineEnd);
                dos.writeBytes(dob + lineEnd);
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes(twoHyphens + boundary + lineEnd);


                String gender = sgen;
                // Send parameter #name
                dos.writeBytes("Content-Disposition: form-data; name=\"gender\"" + lineEnd);
                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + lineEnd);
                dos.writeBytes("Content-Length: " + gender.length() + lineEnd);
                dos.writeBytes(lineEnd);
                dos.writeBytes(gender + lineEnd);
                dos.writeBytes(twoHyphens + boundary + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);

               String classee = scls;
                // Send parameter #name
                dos.writeBytes("Content-Disposition: form-data; name=\"class\"" + lineEnd);
                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + lineEnd);
                dos.writeBytes("Content-Length: " + classee.length() + lineEnd);
                dos.writeBytes(lineEnd);
                dos.writeBytes(classee + lineEnd);
                dos.writeBytes(twoHyphens + boundary + lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);


                String user_id = L_ID;
                // Send parameter #name
                dos.writeBytes("Content-Disposition: form-data; name=\"user_id\"" + lineEnd);
                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + lineEnd);
                dos.writeBytes("Content-Length: " + user_id.length() + lineEnd);
                dos.writeBytes(lineEnd);
                dos.writeBytes(user_id + lineEnd);
                dos.writeBytes(twoHyphens + boundary + lineEnd);




                //--------------------------------------end

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""+ fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);


                // create a buffer of maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                final String serverResponseMessage = conn.getResponseMessage();

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);



                // close the streams //
                fileInputStream.close();
                dos.flush();



                InputStream is = conn.getInputStream();

                // retrieve the response from server
                int ch;

                StringBuffer b =new StringBuffer();
                while( ( ch = is.read() ) != -1 ){ b.append( (char)ch ); }
                final String s=b.toString();
                Log.i("Response",s);


                dos.close();

                if (serverResponseCode == 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {
                            //String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"									+ " c:/wamp/www/echo/uploads";
                            //	messageText.setText(msg);
                            //Toast.makeText(RecorderActivity.this,s, Toast.LENGTH_SHORT).show();

                            //timerValue.setText("File Upload Complete.");
                            JSONObject js = null;
                            try {
                                js = new JSONObject(s);
                                if (js.has("result")) {
                                    s_result = js.getString("result");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if(s_result.equals("success")){


                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();

                                editor.putString(AppsContants.CLASS, scls);
                                editor.commit();



                                Intent i = new Intent(StProfileActivity.this,ParentActivity.class);
                                startActivity(i);

                               // Toast.makeText(StProfileActivity.this,"Voice send to admin successful", Toast.LENGTH_SHORT).show();
                                // finish();

                            }
                        }
                    });
                }

            } catch (MalformedURLException ex) {

                dialog.dismiss();
                ex.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        // timerValue.setText("MalformedURLException Exception : check script url.");
                        Toast.makeText(StProfileActivity.this,
                                "MalformedURLException", Toast.LENGTH_SHORT)
                                .show();
                    }
                });

                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {

                dialog.dismiss();
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        //timerValue.setText("Got Exception : see logcat ");
                        Toast.makeText(StProfileActivity.this,
                                "Got Exception : see logcat ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
               //Log.e("Upload file to server Exception","Exception : " + e.getMessage(), e);
            }
            dialog.dismiss();
            return serverResponseCode;
        }
    }






    //-----------------------------------end------------------------------------




    //-------------------------start------------------------





    public class SignUpjsontask11 extends AsyncTask<String, Void, String> {


        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result="";
        String p_user_login;
        String p_user_nicename;
        String p_display_name;
        String p_password;
        String s_latitude;
        String a1="",a2="",a3="",a4="",a5="";
        String object;
        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);


        }
        protected String doInBackground(String... arg0) {

            HttpClient htppclient=new DefaultHttpClient();
            HttpPost httppost=new HttpPost(HttpUrlPath.urlPath+HttpUrlPath.actionClasses);
           // HttpPost httppost=new HttpPost("http://maestrossoftware.com/WEBSOFT/education/Android/process.php?action=classes");
            try{

                HttpResponse response = htppclient.execute(httppost);
                object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);

                contactsstate1 = new JSONArray(object);

                for (int i = 0; i < contactsstate1.length(); i++) {
                    JSONObject c = contactsstate1.getJSONObject(i);
                    a1=c.getString("id");
                    a2=c.getString("classname");
                    categories.add(a2);
                    categories1.add(a1);



                }
               /* JSONObject js = new JSONObject(object);
                if (js.has("ID")) {
                    UserRegisterID = js.getString("ID");
                    p_user_login = js.getString("user_login");
                    p_user_nicename = js.getString("user_nicename");
                    p_display_name = js.getString("display_name");
                    Errormessage = js.getString("result");
                   // p_mobilenumber = js.getString("number");
                   // s_latitude=js.getString("latitude");
                    //s_longitude=js.getString("longitude");

                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }*/

            }
            catch (Exception e){
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result1){
            super.onPostExecute(result1);

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(StProfileActivity.this, android.R.layout.simple_spinner_item, categories);

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            cls_spin.setAdapter(dataAdapter);


        }


    }






    //------------------------------end---------------------------------











}
