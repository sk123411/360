package in.maestros.education.model;

/**
 * Created by SONU on 25/09/15.
 */
public class ResultModel {

    // Getter and Setter model for recycler view items
    private String id;
    private String cat_name;
    private String status;




    public ResultModel(String id, String cat_name, String status) {


        this.id=id;
        this.cat_name = cat_name;
        this.status=status;


    }
    public void setid(String user_id) {
        this.id = id;
    }
    public String getid() {
        return id;
    }

    public void setcat_name(String cat_name) {
        this.cat_name = cat_name;
    }
    public String getcat_name() {
        return cat_name;
    }

    public void setstatus(String status) {
        this.status = status;
    }
    public String getstatus() {
        return status;
    }


}
