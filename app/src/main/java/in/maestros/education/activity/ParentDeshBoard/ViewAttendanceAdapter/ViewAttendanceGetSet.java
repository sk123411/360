package in.maestros.education.activity.ParentDeshBoard.ViewAttendanceAdapter;

public class ViewAttendanceGetSet {

    public String Id;
    public String AttendanceDate;
    public String AttendanceDay;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getAttendanceDate() {
        return AttendanceDate;
    }

    public void setAttendanceDate(String attendanceDate) {
        AttendanceDate = attendanceDate;
    }

    public String getAttendanceDay() {
        return AttendanceDay;
    }

    public void setAttendanceDay(String attendanceDay) {
        AttendanceDay = attendanceDay;
    }

    public String getAttendanceMonth() {
        return AttendanceMonth;
    }

    public void setAttendanceMonth(String attendanceMonth) {
        AttendanceMonth = attendanceMonth;
    }

    public String AttendanceMonth;
}
