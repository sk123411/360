package in.maestros.education.activity.StudentDeshBoard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.model.WorkResponse;

public class SchemeWorkAdapter extends RecyclerView.Adapter<SchemeWorkAdapter.MyViewHolder> {


    private Context context;
    private List<WorkResponse> workResponses;

    public SchemeWorkAdapter(Context context, List<WorkResponse> workResponses) {
        this.context = context;
        this.workResponses = workResponses;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.work_item_scheme,parent,false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {



        holder.bind(workResponses.get(position));
    }

    @Override
    public int getItemCount() {
        return workResponses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView slText, slDate, slTopic,slObjective,slLearningCount;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            slDate = itemView.findViewById(R.id.slDateTextView);
            slTopic = itemView.findViewById(R.id.slTopicTextView);
            slObjective = itemView.findViewById(R.id.slObjectiveTextView);
            slLearningCount = itemView.findViewById(R.id.slLearningCountTextView);
            slText = itemView.findViewById(R.id.slTextView);


        }





        public void bind(WorkResponse workResponse) {


            slText.setText(workResponse.getId());
            slTopic.setText(workResponse.getTopic());
            slLearningCount.setText(workResponse.getLearningOutcome());
            slObjective.setText(workResponse.getObjective());
            slDate.setText(workResponse.getSchemeDate());






        }
    }
}
