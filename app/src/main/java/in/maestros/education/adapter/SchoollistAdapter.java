package in.maestros.education.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.maestros.education.R;
import in.maestros.education.model.SchoollistModel;


/**
 * Created by w7 on 3/6/2017.
 */

public class SchoollistAdapter extends RecyclerView.Adapter<SchoollistAdapter.MyViewHolder> {
    private ArrayList<SchoollistModel> arrayList;
    Context c;
    String first = "";

    public SchoollistAdapter(Context c, ArrayList<SchoollistModel> arrayList) {
        this.arrayList = arrayList;
        this.c = c;


    }




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView sno, schoolname, action, productprice;
        ImageView profileimage;
        LinearLayout lineartimeleft;
        RatingBar ratinbar;

        public MyViewHolder(View view) {
            super(view);

            sno = (TextView) view.findViewById(R.id.sno);
            schoolname = (TextView) view.findViewById(R.id.schoolname);
            action = (TextView) view.findViewById(R.id.action);


        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listschoolitem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final SchoollistModel listModel1 = arrayList.get(position);
        holder.schoolname.setText(listModel1.getSchoolname());
        holder.action.setText(listModel1.getAction());
        holder.sno.setText(listModel1.getSno());




    }


    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }
}
