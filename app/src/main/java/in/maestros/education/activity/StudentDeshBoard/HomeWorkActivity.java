package in.maestros.education.activity.StudentDeshBoard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.google.android.material.appbar.AppBarLayout;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.StudentDashBoard;
import in.maestros.education.other.AppsContants;

public class HomeWorkActivity extends AppCompatActivity {


    private RecyclerView homeWorkList;
    private ProgressBar progressBar;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work2);



        homeWorkList = findViewById(R.id.homeWorkList);
        progressBar = findViewById(R.id.progressBar);
        back = findViewById(R.id.backImage);



        getHomeWork();

        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), StudentDashBoard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

            }
        });



    }

    private void getHomeWork() {


        progressBar.setVisibility(View.VISIBLE);
        String id = AppsContants.getSharedpreferences().getString(AppsContants.L_ID,"51");


        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=assignment")
                .addBodyParameter("student_id", id)
                .build().getAsObjectList(HomeWorkResponse.class, new ParsedRequestListener<List<HomeWorkResponse>>() {
            @Override
            public void onResponse(List<HomeWorkResponse> response) {





                if (response!=null){
                    progressBar.setVisibility(View.GONE);



                    homeWorkList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    homeWorkList.setAdapter(new HomeWorkAdapter(getApplicationContext(),response));

                }


            }

            @Override
            public void onError(ANError anError) {
                progressBar.setVisibility(View.GONE);

                Log.d("DDDDDDDDDDDDDDD", "::"+anError.toString());

                Toast.makeText(getApplicationContext(), "Server error occured",Toast.LENGTH_LONG).show();
            }
        });





    }
}