package in.maestros.education.activity.StudentDeshBoard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.model.WorkResponse;

public class HomeWorkAdapter extends RecyclerView.Adapter<HomeWorkAdapter.MyViewHolder> {


    private Context context;
    private List<HomeWorkResponse> workResponses;

    public HomeWorkAdapter(Context context, List<HomeWorkResponse> workResponses) {
        this.context = context;
        this.workResponses = workResponses;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_work_item,parent,false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {



        holder.bind(workResponses.get(position));
    }

    @Override
    public int getItemCount() {
        return workResponses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView slName, slCLass, slAssigment,slSubject,slDocument,slDate;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            slName = itemView.findViewById(R.id.stName);
            slCLass = itemView.findViewById(R.id.stClass);
            slAssigment = itemView.findViewById(R.id.sTassigName);
            slSubject = itemView.findViewById(R.id.stSubject);
            slDocument = itemView.findViewById(R.id.stDocument);
            slDate = itemView.findViewById(R.id.stDate);


        }





        public void bind(HomeWorkResponse workResponse) {


            slName.setText(workResponse.getFname());
            slCLass.setText(workResponse.getClassName());

            if (workResponse.getAssignName()!=null){

                slAssigment.setText(workResponse.getAssignName().toString());

            }else {
                slAssigment.setText("N.A");

            }

            if (workResponse.getSubjects()!=null){

                slSubject.setText(workResponse.getSubjects().toString());

            }else {
                slSubject.setText("N.A");

            }

            if (workResponse.getDocument()!=null){

                slDocument.setText(workResponse.getSubjects().toString());

            }else {

                slDocument.setText("N.A");

            }
            slDate.setText(workResponse.getDate());





        }
    }
}
