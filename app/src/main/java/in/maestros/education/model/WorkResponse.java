
package in.maestros.education.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("scheme_date")
    @Expose
    private String schemeDate;
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("objective")
    @Expose
    private String objective;
    @SerializedName("learning_outcome")
    @Expose
    private String learningOutcome;
    @SerializedName("teacher_id")
    @Expose
    private String teacherId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("class_id")
    @Expose
    private String classId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchemeDate() {
        return schemeDate;
    }

    public void setSchemeDate(String schemeDate) {
        this.schemeDate = schemeDate;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getLearningOutcome() {
        return learningOutcome;
    }

    public void setLearningOutcome(String learningOutcome) {
        this.learningOutcome = learningOutcome;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

}