package in.maestros.education.activity.StudentDeshBoard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LessonResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("assign_name")
    @Expose
    private String assignName;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("question_type")
    @Expose
    private String questionType;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("upload")
    @Expose
    private String upload;
    @SerializedName("option1")
    @Expose
    private String option1;
    @SerializedName("option1_img")
    @Expose
    private String option1Img;
    @SerializedName("option2")
    @Expose
    private String option2;
    @SerializedName("option2_img")
    @Expose
    private String option2Img;
    @SerializedName("option3")
    @Expose
    private String option3;
    @SerializedName("option3_img")
    @Expose
    private String option3Img;
    @SerializedName("option4")
    @Expose
    private String option4;
    @SerializedName("option4_img")
    @Expose
    private String option4Img;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("answer_img")
    @Expose
    private Object answerImg;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("a_id")
    @Expose
    private String aId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssignName() {
        return assignName;
    }

    public void setAssignName(String assignName) {
        this.assignName = assignName;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getUpload() {
        return upload;
    }

    public void setUpload(String upload) {
        this.upload = upload;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption1Img() {
        return option1Img;
    }

    public void setOption1Img(String option1Img) {
        this.option1Img = option1Img;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption2Img() {
        return option2Img;
    }

    public void setOption2Img(String option2Img) {
        this.option2Img = option2Img;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption3Img() {
        return option3Img;
    }

    public void setOption3Img(String option3Img) {
        this.option3Img = option3Img;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getOption4Img() {
        return option4Img;
    }

    public void setOption4Img(String option4Img) {
        this.option4Img = option4Img;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Object getAnswerImg() {
        return answerImg;
    }

    public void setAnswerImg(Object answerImg) {
        this.answerImg = answerImg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getaId() {
        return aId;
    }

    public void setaId(String aId) {
        this.aId = aId;
    }

}
