package in.maestros.education.activity.ParentDeshBoard.AttendanceAdapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.Assesment;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentAdapter;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentGetSet;
import in.maestros.education.activity.ParentDeshBoard.ViewAttendance;
import in.maestros.education.other.AppsContants;

public class AttendanceAdapter  extends RecyclerView.Adapter<AttendanceAdapter.ViewHolder> {

    Context context;


    List<AttendanceGetSet> dataAdapters;


    String strCatID="",strId="",strCatName="";









    public AttendanceAdapter(List<AttendanceGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public AttendanceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_adapter, parent, false);



        AttendanceAdapter.ViewHolder viewHolder = new AttendanceAdapter.ViewHolder(view);











        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AttendanceAdapter.ViewHolder Viewholder, int position) {

        final AttendanceGetSet dataAdapterOBJ = dataAdapters.get(position);







        Viewholder.txtStudent.setText(dataAdapterOBJ.getStudentName());


Viewholder.card.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.AttendanceId,dataAdapterOBJ.getId());


        editor.commit();


        context.startActivity(new Intent(context, ViewAttendance.class));
    }
});





    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }





    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtStudent,txtAttendaance;

        public CardView card;




        public ViewHolder(View itemView) {

            super(itemView);



            txtStudent = itemView.findViewById(R.id.txtStudent);
            txtAttendaance = itemView.findViewById(R.id.txtAttendaance);
            card = itemView.findViewById(R.id.card);



        }
    }
}