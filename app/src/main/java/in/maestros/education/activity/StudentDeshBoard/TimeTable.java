package in.maestros.education.activity.StudentDeshBoard;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;

import org.json.JSONObject;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.FragmentStudent.TimeTableAdapter;
import in.maestros.education.activity.FragmentStudent.TimeTableResponse;
import in.maestros.education.other.AppsContants;

public class TimeTable extends AppCompatActivity {


    String userid="",L_ID="";

    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView txtClass,txtSubject,txtStartTime,txtEndTime,txtTeacher,txtDay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table);

//        txtClass = findViewById(R.id.txtClass);
//        txtSubject = findViewById(R.id.txtSubject);
//        txtStartTime = findViewById(R.id.txtStartTime);
//        txtEndTime = findViewById(R.id.txtEndTime);
//        txtTeacher = findViewById(R.id.txtTeacher);
        recyclerView = findViewById(R.id.timeTableList);
        progressBar = findViewById(R.id.progressBar);


        ShowTimeTable();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES,Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME,"");
        L_ID= AppsContants.sharedpreferences.getString(AppsContants.L_ID,"");

        ImageView backImage = findViewById(R.id.back);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }




    public void ShowTimeTable(){

        progressBar.setVisibility(View.VISIBLE);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES,Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME,"");
        L_ID = AppsContants.sharedpreferences.getString(AppsContants.L_ID,"");


        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=student_time_table")
                .addBodyParameter("user_id","51")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObjectList(TimeTableResponse.class, new ParsedRequestListener<List<TimeTableResponse>>() {
                    @Override
                    public void onResponse(List<TimeTableResponse> response) {

                        progressBar.setVisibility(View.GONE);



                        if (response.size()>0){


                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            recyclerView.setAdapter(new TimeTableAdapter(response));




                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        progressBar.setVisibility(View.GONE);

                        Toast.makeText(getApplicationContext(),":"+anError.getLocalizedMessage(),Toast.LENGTH_LONG).show();

                    }
                });





    }
}
