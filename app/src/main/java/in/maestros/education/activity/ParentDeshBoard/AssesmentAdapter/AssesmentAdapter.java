package in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.ResultAdapter.ResultAdapter;
import in.maestros.education.activity.ParentDeshBoard.ResultAdapter.ResultGetSet;
import in.maestros.education.activity.StudentDeshBoard.MyResultDetails;

public class AssesmentAdapter extends RecyclerView.Adapter<AssesmentAdapter.ViewHolder> {

        Context context;


        List<AssesmentGetSet> dataAdapters;


        String strCatID="",strId="",strCatName="";









public AssesmentAdapter(List<AssesmentGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

        }

@Override
public AssesmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.assesment_adapter, parent, false);



        AssesmentAdapter.ViewHolder viewHolder = new AssesmentAdapter.ViewHolder(view);











        return viewHolder;
        }

@Override
public void onBindViewHolder(AssesmentAdapter.ViewHolder Viewholder, int position) {

final AssesmentGetSet dataAdapterOBJ = dataAdapters.get(position);




        // Toast.makeText(context, ""+dataAdapterOBJ.getQuestion(), Toast.LENGTH_SHORT).show();


        Viewholder.txtStudent.setText(dataAdapterOBJ.getStudentName());
        Viewholder.txtAssesment.setText(dataAdapterOBJ.getAssesment());
        Viewholder.txtTeacher.setText(dataAdapterOBJ.getTeacherName());
        Viewholder.txtSchool.setText(dataAdapterOBJ.getSchoolName());






        }

@Override
public int getItemCount() {

        return dataAdapters.size();
        }





class ViewHolder extends RecyclerView.ViewHolder {

    public TextView txtStudent,txtAssesment,txtTeacher,txtSchool;




    public ViewHolder(View itemView) {

        super(itemView);



        txtStudent = itemView.findViewById(R.id.txtStudent);
        txtAssesment = itemView.findViewById(R.id.txtAssesment);
        txtTeacher = itemView.findViewById(R.id.txtTeacher);
        txtSchool = itemView.findViewById(R.id.txtSchool);



    }
}



}

