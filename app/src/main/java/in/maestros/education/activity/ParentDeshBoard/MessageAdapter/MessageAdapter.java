package in.maestros.education.activity.ParentDeshBoard.MessageAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentAdapter;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentGetSet;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    Context context;


    List<MessageGetSet> dataAdapters;


    String strCatID="",strId="",strCatName="";









    public MessageAdapter(List<MessageGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_adapter, parent, false);



        MessageAdapter.ViewHolder viewHolder = new MessageAdapter.ViewHolder(view);











        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MessageAdapter.ViewHolder Viewholder, int position) {

        final MessageGetSet dataAdapterOBJ = dataAdapters.get(position);




        // Toast.makeText(context, ""+dataAdapterOBJ.getQuestion(), Toast.LENGTH_SHORT).show();


        Viewholder.txtMessage.setText(dataAdapterOBJ.getMessage());
        Viewholder.txtTeacher.setText(dataAdapterOBJ.getSentbyTeacher());







    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }





    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtMessage,txtTeacher;



        public ViewHolder(View itemView) {

            super(itemView);




            txtMessage = itemView.findViewById(R.id.txtMessage);
            txtTeacher = itemView.findViewById(R.id.txtTeacher);



        }
    }



}

