package in.maestros.education.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.maestros.education.R;
import in.maestros.education.activity.PopupShowResultActivity;
import in.maestros.education.model.SchoolResultModel;
import in.maestros.education.other.AppsContants;


/**
 * Created by w7 on 3/6/2017.
 */

public class SchoolResultAdapter extends RecyclerView.Adapter<SchoolResultAdapter.MyViewHolder> {
    private ArrayList<SchoolResultModel> arrayList;
    Context c;
    String first = "";

    public SchoolResultAdapter(Context c, ArrayList<SchoolResultModel> arrayList) {
        this.arrayList = arrayList;
        this.c = c;


    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView sno, schoolname, viewtext, productprice;
        ImageView profileimage;
        LinearLayout lineartimeleft;
        RatingBar ratinbar;

        public MyViewHolder(View view) {
            super(view);

            sno = (TextView) view.findViewById(R.id.sno);
            schoolname = (TextView) view.findViewById(R.id.schoolname);
            viewtext=(TextView)view.findViewById(R.id.viewtext);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listresultitem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final SchoolResultModel listModel1 = arrayList.get(position);
        holder.schoolname.setText(listModel1.getAssignment());
     //   holder.action.setText(listModel1.getDatetime());
        holder.sno.setText(listModel1.getAction());
holder.viewtext.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        AppsContants.sharedpreferences = c.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.TOTAL_QUESTIONS, listModel1.gettotal_qs());
        editor.putString(AppsContants.CORRECT_ANSWERE, listModel1.getcorrect_answere());
        editor.putString(AppsContants.WRONG_ANSWERE, listModel1.getwrong_ans());
        editor.putString(AppsContants.SKIPPED_QUESTION, listModel1.getskipped());
        editor.putString(AppsContants.SCORE, listModel1.getscore());
        editor.commit();


        Intent vv=new Intent(c,PopupShowResultActivity.class);
        c.startActivity(vv);
    }
});



    }


    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }
}
