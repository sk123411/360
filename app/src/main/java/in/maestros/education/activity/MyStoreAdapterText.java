package in.maestros.education.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;


/**
 * Created by Edwin on 18/01/2015.
 */

public class MyStoreAdapterText extends RecyclerView.Adapter<MyStoreAdapterText.ViewHolder> {


    List<NatureMyLocationText> mItems;
    Context a;
    SharedPreferences sharedpreferences;
    public static final String CHAT_ID = "chatidKey";
    public static final String CHAT_NAME = "chatnameKey";
    public static final String MyPREFERENCES = "MyPrefs";


    public MyStoreAdapterText(Context a, List<NatureMyLocationText> products) {
        super();
        mItems = new ArrayList<NatureMyLocationText>();
        this.mItems = products;
        this.a = a;


    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_card_mylocation, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final NatureMyLocationText nature = mItems.get(i);
        viewHolder.location.setText(nature.getSubject());
        //  viewHolder.status.setText(nature.getSubject());
        //viewHolder.status.setVisibility(View.INVISIBLE);

        viewHolder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // viewHolder.status.setVisibility(View.VISIBLE);
            }
        });
        viewHolder.location
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AppsContants.sharedpreferences = a.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                        editor1.putString(AppsContants.subject_id, nature.getSubject_id());
                        editor1.putString(AppsContants.SUBJECT, nature.getSubject());
                        editor1.putString(AppsContants.CLASS, nature.getClasss());
                        editor1.putString(AppsContants.STATUS, nature.getstatus());
                        editor1.commit();
                        if (nature.getstatus().equals("0")){
                            Toast.makeText(a, "Zero Que in this Subject", Toast.LENGTH_SHORT).show();


                        }else {
                            Intent i = new Intent(a, ShowAllQuestionDetailsActivity.class);
                            a.startActivity(i);
                        }




                    }
                });
        //viewHolder.minimum.setVisibility(View.GONE);
     /*   viewHolder.minimum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(a,"next",Toast.LENGTH_LONG).show();


                AppsContants.sharedpreferences = a.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                editor1.putString(AppsContants.ASSI_ID, nature.getId());

                editor1.commit();


                Intent i=new Intent(a,QuesAnsActivity.class);
                a.startActivity(i);
            }
        });*/


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ;
        public Button location;
        // public Button status;
        // public Button minimum;
        // public TextView delivery;
        // public TextView payment;
        // public TextView amount;


        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            location = (Button) itemView.findViewById(R.id.b1);
            // status = (Button) itemView.findViewById(R.id.b2);
            // minimum = (Button) itemView.findViewById(R.id.b3);
            // delivery = (TextView) itemView.findViewById(R.id.tvtime);
            // payment = (TextView) itemView.findViewById(R.id.tvpay);
            // amount = (TextView) itemView.findViewById(R.id.tvamount);


        }

        @Override
        public void onClick(View v) {


        }
    }
}


