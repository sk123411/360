package in.maestros.education.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import in.maestros.education.R;
import in.maestros.education.activity.FragmentStudent.MyTaskFragment;
import in.maestros.education.activity.FragmentStudent.ProfileFragment;
import in.maestros.education.fragment.AboutFragment;
import in.maestros.education.fragment.ContactUsFragment;
import in.maestros.education.fragment.PrivicyPolicyFragment;
import in.maestros.education.fragment.TermsConditionsFragment;
import in.maestros.education.other.AppsContants;

public class NavigationActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private static String TAG = NavigationActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;

    private static boolean isHelpShown = false;
    protected static boolean isInfoShown = false;
    protected static boolean isMainShown = false;
    private ImageView profileimg;
    private static boolean isAShown = false;
    private static boolean isBShown = false;
    private String profileimage="";
    String userid="";
    private static enum Fragments {
        MAIN,
        HELP,
        INFO,
        A,
        B,
        TASK

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_navigation);


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        profileimage=AppsContants.sharedpreferences.getString(AppsContants.PROFILEIMAGE,"");
        userid = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");


        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        profileimg=(ImageView)findViewById(R.id.profileimg);
       /* if(!profileimage.equals("")) {
            Picasso.with(NavigationActivity.this).load(profileimage).placeholder(R.drawable.ic_profile).into(profileimg);
        }*/
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
        displayView(0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_search) {
          //  Toast.makeText(getApplicationContext(), "Search action is selected!", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.title_home);
                break;
            case 1:
                fragment = new AboutFragment();
                title = getString(R.string.nav_item_friends);
                break;
            case 6:
                fragment = new ContactUsFragment();
                title = getString(R.string.nav_11);
                break;

            case 4:
                fragment = new ProfileFragment();
                title = "Profile";
                break;

            case 7:
                fragment = new TermsConditionsFragment();
                title = getString(R.string.nav_22);
                break;
            case 8:
                fragment = new PrivicyPolicyFragment();
                title = getString(R.string.nav_33);
                break;
            case 5:
                fragment = new ResultFragment();
                title = getString(R.string.nav_17);
                break;
            case 2:
                startActivity(new Intent(NavigationActivity.this,SchoolActivity.class));
             //   fragment = new MessagesFragment();
                title = getString(R.string.nav_item_notifications);
                break;
            case 3:
                fragment = new AFragment();
                title = getString(R.string.nav_item_a);
                break;
            case 9: LoginManager.getInstance().logOut();


                android.app.AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new android.app.AlertDialog.Builder(NavigationActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    builder = new android.app.AlertDialog.Builder(NavigationActivity.this);
                }
                builder.setTitle(NavigationActivity.this.getResources().getString(R.string.app_name))
                        .setMessage("Confirm logout?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(final DialogInterface dialog, int which) {

                                final ProgressDialog progressDialog = new ProgressDialog(NavigationActivity.this);
                                progressDialog.setMessage("Logging You Out.....");
                                progressDialog.show();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                    @Override
                                    public void run() {

                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                        editor.putString(AppsContants.U_ID, "");
                                        editor.putString(AppsContants.USER_NAME, "");
                                        editor.putString(AppsContants.LNAME, "");
                                        editor.putString(AppsContants.EMAIL, "");
                                        editor.putString(AppsContants.CLASS, "");
                                        editor.commit();

                                        Intent i = new Intent(NavigationActivity.this, SplashActivity.class);
                                        startActivity(i);
                                        finishAffinity();

                                    }
                                }, 2000);
                                dialog.dismiss();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        })
                        .show();


                break;
            case 10:
                fragment = new MyTaskFragment();
                title = getString(R.string.title_home);
                break;











        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public final void onBackPressed() {
        if (isMainShown) {
            // We're in the MAIN Fragment.
           // finish();

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(NavigationActivity.this);

            // Setting Dialog Title
            alertDialog.setTitle("360Educating...");

            // Setting Dialog Message
            alertDialog.setMessage("Are you sure you want to Exit..");

            // Setting Icon to Dialog

            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {

                    System.exit(0);

                }
            });

            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // Write your code here to invoke NO event
                    //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                    dialog.cancel();

                }
            });

            // Showing Alert Message
            alertDialog.show();


        } else {
            // We're somewhere else, reload the MAIN Fragment.
            showFragment(Fragments.MAIN);
        }
    }

    private final void showFragment(final Fragments FragmentType) {

        isHelpShown = false;
        isInfoShown = false;
        isMainShown = false;
        isAShown = false;
        isBShown = false;


        final FragmentTransaction ft =
                getSupportFragmentManager().beginTransaction();

    /*
    Replace whatever is in the fragment_container view with this
    fragment, and add the transaction to the back stack so the user can
    navigate back.
    */
        switch (FragmentType) {
            case HELP: {
                ft.replace(R.id.container_body, new FriendsFragment());
                isHelpShown = true;
                break;
            }
            case INFO: {
                ft.replace(R.id.container_body, new MessagesFragment());
                isInfoShown = true;
                break;
            }

            case MAIN: {
                ft.replace(R.id.container_body, new HomeFragment());
                isMainShown = true;
                break;
            }
            case A: {
                ft.replace(R.id.container_body, new AFragment());
                isAShown = true;
                break;
            }
            case B: {
                ft.replace(R.id.container_body, new BFragment());
                isBShown = true;
                break;
            }


        }

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        // Finalize the transaction...
        ft.commit();

        getSupportFragmentManager().executePendingTransactions();
    }
}