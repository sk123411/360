package in.maestros.education.activity.ParentDeshBoard.ViewAttendanceAdapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.AttendanceAdapter.AttendanceAdapter;
import in.maestros.education.activity.ParentDeshBoard.AttendanceAdapter.AttendanceGetSet;
import in.maestros.education.activity.ParentDeshBoard.ViewAttendance;
import in.maestros.education.other.AppsContants;

public class ViewAttendanceAdapter extends RecyclerView.Adapter<ViewAttendanceAdapter.ViewHolder> {

    Context context;


    List<ViewAttendanceGetSet> dataAdapters;


    String strCatID="",strId="",strCatName="";









    public ViewAttendanceAdapter(List<ViewAttendanceGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public ViewAttendanceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewattendance_adapter, parent, false);



        ViewAttendanceAdapter.ViewHolder viewHolder = new ViewAttendanceAdapter.ViewHolder(view);











        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewAttendanceAdapter.ViewHolder Viewholder, int position) {

        final ViewAttendanceGetSet dataAdapterOBJ = dataAdapters.get(position);







        Viewholder.txtDate.setText(dataAdapterOBJ.getAttendanceDate());
        Viewholder.txtDay.setText(dataAdapterOBJ.getAttendanceDay());
        Viewholder.txtMonth.setText(dataAdapterOBJ.getAttendanceMonth());







    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }





    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtDate,txtDay,txtMonth;




        public ViewHolder(View itemView) {

            super(itemView);



            txtDate = itemView.findViewById(R.id.txtDate);
            txtDay = itemView.findViewById(R.id.txtDay);
            txtMonth = itemView.findViewById(R.id.txtMonth);



        }
    }
}