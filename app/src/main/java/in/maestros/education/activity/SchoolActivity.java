package in.maestros.education.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

public class SchoolActivity extends Activity{
    String[] level = {"Select School level", "Primary", "Secondary"};
    String[] state = {"Select state", "Abuja", "Anambra", "Bayelsa", "Benue", "Borno", "CrossRiver", "Delta", "Ebonyi", "Edo", "Ekiti", "Enugu", "Gombe", "Imo", "Jigawa", "Kaduna", "Kano", "Katsina", "Kebbi", "Kogi", "Kwara", "Lagos", "Nasarawa", "Niger", "Ogun", "Ondo", "Osun", "Oyo", "Plateau", "Rivers", "Sokoto", "Taraba", "Yobe", "Zamfara"};
    private Spinner spin, spin2;
    private RelativeLayout levelrtv;
    private String statename="",schoollevel="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.activity_school);
        AppsContants.sharedpreferences=getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);


        levelrtv=(RelativeLayout)findViewById(R.id.levelrtv);
        spin = (Spinner) findViewById(R.id.spinner1);


        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, state);
        aa.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0) {

                    statename=parent.getItemAtPosition(position).toString();
                    SharedPreferences.Editor editor=AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.STATENAME,statename);
                    editor.commit();
                    Toast.makeText(SchoolActivity.this, ""+statename, Toast.LENGTH_SHORT).show();
                    levelrtv.setVisibility(View.VISIBLE);
                }else if(position==0) {
                    levelrtv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);
        spin2 = (Spinner) findViewById(R.id.spinner2);
        spin2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                schoollevel=parent.getItemAtPosition(position).toString();
                SharedPreferences.Editor editor=AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.SCHOOLLEVEL,schoollevel);
                editor.commit();
                if(position!=0) {
                    startActivity(new Intent(SchoolActivity.this, ListSchoolActivity.class));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, level);
        aa.setDropDownViewResource(android.R.layout.simple_list_item_1);
        //Setting the ArrayAdapter data on the Spinner
        spin2.setAdapter(aa2);
    }


}
