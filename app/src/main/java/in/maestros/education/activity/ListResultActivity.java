package in.maestros.education.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.adapter.SchoolResultAdapter;
import in.maestros.education.model.SchoolResultModel;
import in.maestros.education.other.AppsContants;

public class ListResultActivity extends AppCompatActivity {
   private RecyclerView mRecyclerView;
   private ArrayList<SchoolResultModel> products;
    private ConnectionDetector cd;
    private ProgressDialog pDialog;
    static JSONArray contactsstate = null;
    private String SUBJECT_ID="",U_ID="";
    private SchoolResultAdapter cAdapter;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_school);
        AppsContants.sharedpreferences=getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);

        SUBJECT_ID=AppsContants.sharedpreferences.getString(AppsContants.SUBJECT_ID,"");
        U_ID=AppsContants.sharedpreferences.getString(AppsContants.U_ID,"");
        products= new ArrayList<SchoolResultModel>();
        mRecyclerView = (RecyclerView)findViewById(R.id.listschoolrecycle);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        mRecyclerView.setHasFixedSize(true);
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        schoollist("");
    }
    public void schoollist(String str_com_id){
        cd = new ConnectionDetector(ListResultActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(ListResultActivity.this, "noNetworkConnection", Toast.LENGTH_LONG).show();
        }
        else {
            new Followerjasontask(str_com_id).execute();
        }
    }
    private class Followerjasontask extends AsyncTask<String, Void, String> {
        String jsonStr;
        String result = "";
        String jsoprofile;
        public Followerjasontask(String strprofile) {
            this.jsoprofile = strprofile;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ListResultActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
//            pDialog.show();
//            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        protected String doInBackground(String... voids) {
            // TODO Auto-generated method stub
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://360educating.com/Android/process.php?action=show_result");
            //HttpPost post = new HttpPost(HttpUrlPath.urlPath+HttpUrlPath.actionShow_post);

            try {
                List<NameValuePair> nameValuePair=new ArrayList <NameValuePair>();
                nameValuePair.add(new BasicNameValuePair("user_id",U_ID));
                nameValuePair.add(new BasicNameValuePair("subject",SUBJECT_ID));

              //  nameValuePair.add(new BasicNameValuePair("school_lavel",schoollevel));
                post.setEntity(new UrlEncodedFormEntity(nameValuePair));
                HttpResponse response = client.execute(post);
                jsonStr = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + jsonStr);
                contactsstate = new JSONArray(jsonStr);

                for (int i = 0; i < contactsstate.length(); i++) {
                    JSONObject c = contactsstate.getJSONObject(i);
                    String a1=c.getString("id");
                    String a2=c.getString("user_id");
                    String a3=c.getString("sess_id");
                    String a4=c.getString("class");
                    String a5=c.getString("assign_name");
                    String a6=c.getString("correct_ans");
                    String a7=c.getString("wrong_ans");
                    String a8=c.getString("test_date");
                    String a9=c.getString("test_time");
                    String a10=c.getString("total_qs");
                    String a11=c.getString("skipped");
                    String a12=c.getString("score");

                    products.add(new SchoolResultModel(a7,a1,a6,"","","","","","","","","","",""));
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            //  progressBar.setVisibility(View.GONE);
//            if (pDialog.isShowing())
//                pDialog.dismiss();
            // Toast.makeText(getActivity(),jsonStr+ "hello", Toast.LENGTH_SHORT).show();
            cAdapter = new SchoolResultAdapter(ListResultActivity.this,products);

            mRecyclerView.setAdapter(cAdapter);
        }
    }
}
