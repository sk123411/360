package in.maestros.education.activity;

/**
 * Created by Ravi on 29/07/15.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.login.widget.ProfilePictureView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.adapter.NavigationDrawerAdapter;
import in.maestros.education.model.NavDrawerItem;
import in.maestros.education.other.AppsContants;

public class FragmentDrawer extends Fragment {

    private static String TAG = FragmentDrawer.class.getSimpleName();

    private RecyclerView recyclerView;
    private ImageView profileimg;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerAdapter adapter;
    private String Name11;
    private View containerView;
    private static String[] titles = null;
    private FragmentDrawerListener drawerListener;
    private String facbokImage = "";
    private ProfilePictureView facebookImage;

    public FragmentDrawer() {

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public static List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();


        // preparing navigation drawer items
        for (int i = 0; i < titles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(titles[i]);
            data.add(navItem);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // drawer labels
        titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        Name11 = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        facbokImage = AppsContants.sharedpreferences.getString(AppsContants.FACEBOOK_PROFILE, "");
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        profileimg = (ImageView) layout.findViewById(R.id.profileimg);
        facebookImage = (ProfilePictureView) layout.findViewById(R.id.picture);
        adapter = new NavigationDrawerAdapter(getActivity(), getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                drawerListener.onDrawerItemSelected(view, position);
                mDrawerLayout.closeDrawer(containerView);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        SignUpjsontask task = new SignUpjsontask();
        task.execute();
        return layout;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }


    public class SignUpjsontask extends AsyncTask<String, Void, String> {

        String Errormessage;
        String UserRegisterID, u_fname, u_lname, u_emails, u_gend, u_class, u_pfname, u_plname, u_state, u_add,
                u_email1, u_pno, u_state1, u_add1, u_email2, u_pno1, u_month, u_dt, u_year, u_img, t_lname, t_fname;
        boolean IsError = false;
        String result = "";
        String u_email;
        String u_pass;
        String u_lat;
        String u_lon;
        String u_class_id = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // loader.setVisibility(View.VISIBLE);
        }

        protected String doInBackground(String... arg0) {

            HttpClient htppclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(HttpUrlPath.urlPath + HttpUrlPath.actionUpdateProfile);
            try {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();


                nameValuePair.add(new BasicNameValuePair("id", Name11));
                // nameValuePair.add(new BasicNameValuePair("lname",s_lname));
                // nameValuePair.add(new BasicNameValuePair("email",s_email));
                // nameValuePair.add(new BasicNameValuePair("password",s_password));
                //nameValuePair.add(new BasicNameValuePair("class",s_ss));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));


                HttpResponse response = htppclient.execute(httppost);
                String object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);
                JSONObject js = new JSONObject(object);

                if (js.has("id")) {
                    UserRegisterID = js.getString("id");
                    u_fname = js.getString("fname");
                    u_lname = js.getString("lname");
                    //  u_emails = js.getString("email");

                    u_gend = js.getString("gender");
                    u_class = js.getString("class");
                    u_class_id = js.getString("class_id");
                    u_pfname = js.getString("p_fname");

                    u_plname = js.getString("p_lname");
                    u_state = js.getString("state");
                    u_add = js.getString("address");


                    u_email1 = js.getString("email1");
                    u_pno = js.getString("phno");

                    t_fname = js.getString("t_fname");
                    t_lname = js.getString("t_lname");

                    u_state1 = js.getString("state1");

                    u_add1 = js.getString("address1");
                    u_email2 = js.getString("email2");
                    u_pno1 = js.getString("phno1");

                    u_month = js.getString("month");
                    u_dt = js.getString("dt");
                    u_year = js.getString("year");


                    u_img = js.getString("img");


                    Errormessage = js.getString("result");
                    //user_mobile=js.getString("number");

                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }


        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            // loader.setVisibility(View.GONE);
            //Toast.makeText(Register.this,UserRegisterID+ "rohit "+Errormessage,Toast.LENGTH_LONG).show();
            if (!IsError) {

                if (Errormessage.equals("successfully")) {

                    if (UserRegisterID != null) {
                        if (!UserRegisterID.equals("")) {

                            //   Toast.makeText(getActivity(), Errormessage, Toast.LENGTH_SHORT).show();

                            AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                            editor1.putString(AppsContants.CLASS_ID, u_class_id);
                            editor1.commit();


                            if (!facbokImage.equals("")) {
                                profileimg.setVisibility(View.GONE);
                                facebookImage.setVisibility(View.VISIBLE);
                                facebookImage.setProfileId(facbokImage);
                            }

                            if(!u_img.equals(""))
                            {
                                facebookImage.setVisibility(View.GONE);
                                profileimg.setVisibility(View.VISIBLE);
                                Picasso.with(getActivity()).load(u_img).into(profileimg);
                            }

                          /*  if (!u_img.equals("")) {
                                Picasso.with(getActivity()).load(u_img).into(profileimg);
                            }*/
                            Toast.makeText(getActivity(), UserRegisterID + "     " + u_class_id, Toast.LENGTH_LONG).show();


                        }

                    }
                } else {
                    Toast.makeText(getActivity(), "please enter correct username and password", Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(getActivity(), "please try again...", Toast.LENGTH_SHORT).show();


            }
        }

    }


}
