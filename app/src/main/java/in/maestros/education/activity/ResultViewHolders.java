package in.maestros.education.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import in.maestros.education.R;

public class ResultViewHolders extends RecyclerView.ViewHolder {

    public TextView countryName;
    public ImageView countryPhoto;

    public ResultViewHolders(View itemView) {
        super(itemView);

        countryName = (TextView)itemView.findViewById(R.id.tc1);
        countryPhoto = (ImageView)itemView.findViewById(R.id.tc);
    }


}