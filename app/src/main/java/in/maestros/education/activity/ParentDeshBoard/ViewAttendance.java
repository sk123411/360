package in.maestros.education.activity.ParentDeshBoard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.HttpUrlPath;
import in.maestros.education.activity.ParentDeshBoard.AttendanceAdapter.AttendanceAdapter;
import in.maestros.education.activity.ParentDeshBoard.AttendanceAdapter.AttendanceGetSet;
import in.maestros.education.activity.ParentDeshBoard.ViewAttendanceAdapter.ViewAttendanceAdapter;
import in.maestros.education.activity.ParentDeshBoard.ViewAttendanceAdapter.ViewAttendanceGetSet;
import in.maestros.education.other.AppsContants;

public class ViewAttendance extends AppCompatActivity {
    String userid="",L_ID="",strName="",Name11="",Name12="",Name13="";




    TextView txtName,txtClass,txtSubject,txtDate,txtAction;

ProgressBar progressBar;

    RecyclerView recyclerview1;
    List<ViewAttendanceGetSet> arrayList;
    ViewAttendanceAdapter adapter;



    ImageView back;

    RecyclerView.LayoutManager layoutManagerOfrecyclerView;
    RecyclerView.Adapter recyclerViewadapter;


    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance);


        txtName= findViewById(R.id.txtName);
        txtClass= findViewById(R.id.txtClass);
        txtSubject= findViewById(R.id.txtSubject);
        txtDate= findViewById(R.id.txtDate);
        progressBar= findViewById(R.id.progressBar);
        // txtAction= findViewById(R.id.txtAction);
        recyclerview1 = (RecyclerView) findViewById(R.id.recyclerview1);

       /* txtAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Result.this, MyResultDetails.class));
            }
        });*/

        back= findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewAttendance.this,Attendance.class));
            }
        });
        ShowAttendance();


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.AttendanceId, "");



        layoutManagerOfrecyclerView = new LinearLayoutManager(ViewAttendance.this);
        recyclerview1.setLayoutManager(layoutManagerOfrecyclerView);

        Log.e("gfjhgdfjgdfjh",L_ID);







    }


    public void ShowAttendance() {

        progressBar.setVisibility(View.VISIBLE);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        userid = AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.AttendanceId, "");



        Log.e("gfdjhdgfjgdfjgf",L_ID);
        AndroidNetworking.post(HttpUrlPath.urlPath + HttpUrlPath.actionParentViewAttendance)
                .addBodyParameter("id", L_ID)
                .setTag("Categories")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        try {



                            arrayList = new ArrayList<>();
                            progressBar.setVisibility(View.GONE);

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                Log.e("jasgterrr", response.getJSONObject(i) + "");


                                ViewAttendanceGetSet GetSet = new ViewAttendanceGetSet();
                                GetSet.setId(jsonObject.getString("id"));
                                GetSet.setAttendanceDate(jsonObject.getString("attendance_date"));
                                GetSet.setAttendanceDay(jsonObject.getString("attendance_day"));
                                GetSet.setAttendanceMonth(jsonObject.getString("attendance_month"));
                                arrayList.add(GetSet);
                                Log.e("dfhjhsjh", jsonObject.getString("id"));






                            }
                            adapter = new ViewAttendanceAdapter(arrayList, ViewAttendance.this);
                            recyclerview1.setAdapter(adapter);
                             progressBar.setVisibility(View.GONE);


                        } catch (Exception ex) {
                            progressBar.setVisibility(View.GONE);

                            Log.e("tracing", ex.getMessage());
                            progressBar.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressBar.setVisibility(View.GONE);

                        Log.e("tracing", anError.toString());


                    }
                });

    }
}
