package in.maestros.education.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.RecoverySystem;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.TeacherDeshBoard.TeacherDeshBoard;
import in.maestros.education.other.AppsContants;

public class LoginActivity extends AppCompatActivity {

    Button login;
    TextView newuser, forget;
    private ProgressDialog pDialog;
    EditText email, pass;
    String s_email = "", s_pass = "", strUserType = "";

    RadioGroup radioGroup1;



    ProgressBar progress;

    RadioButton radioParent, radioStudent,radioTeacher;


    String Name11="",FirstName="",L_ID="",strEmail="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        email = (EditText) findViewById(R.id.email);
        pass = (EditText) findViewById(R.id.pass);
        progress =  findViewById(R.id.progress);
        radioParent = (RadioButton) findViewById(R.id.radioParent);
        radioStudent = (RadioButton) findViewById(R.id.radioStudent);
        radioTeacher = (RadioButton) findViewById(R.id.radioTeacher);


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        Name11 = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        L_ID = AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");
        FirstName = AppsContants.sharedpreferences.getString(AppsContants.FName, "");
        strEmail = AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");


        newuser = (TextView) findViewById(R.id.newuser);
        forget = (TextView) findViewById(R.id.forget);


        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, Forgetactivity.class));
            }
        });

        newuser = (TextView) findViewById(R.id.newuser);
        newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(i);

            }
        });



        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {


                if(radioStudent.isChecked()){

                    strUserType="std";


                    Log.e("rhshg",strUserType);
                }

                else if(radioParent.isChecked()){
                    strUserType="parnt";

                    Log.e("rhshg",strUserType);
                }
                else if (radioTeacher.isChecked()){
                    strUserType="techr";


                    Log.e("rhshg",strUserType);
                }
                else{

                }

            }
        });


        login = (Button) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strEmail = email.getText().toString().trim();
                s_pass = pass.getText().toString().trim();


                Log.e("jhieuorrfer", strUserType);




                if (strEmail.equals("")) {

                    Toast.makeText(LoginActivity.this, "Enter Email", Toast.LENGTH_SHORT).show();
                } else if (s_pass.equals("")) {


                    Toast.makeText(LoginActivity.this, "Enter Password", Toast.LENGTH_SHORT).show();
                } else {

                    Login();
                }
            }
        });
    }


    public void Login() {


        progress.setVisibility(View.VISIBLE);


        Log.e("gfierygahgc", strEmail);
        Log.e("gfierygahgc", s_pass);
        Log.e("gfierygahgc", strUserType);


        Log.e("tyujtyhgb ", HttpUrlPath.actionLogin);

        AndroidNetworking.post(HttpUrlPath.actionLogin)
                .addBodyParameter("email", strEmail)
                .addBodyParameter("password", s_pass)
                .addBodyParameter("log_type", strUserType)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            Log.e("fhgoiuoghsj", response.toString());
                            String str = response.getString("result");
                            int intetedge=0;

                            Log.e("jdfhjksiuerug", response.getString("result"));
                            if (str.equals("successful")) {
                                progress.setVisibility(View.GONE);




                                Log.e("fgiergf",str);

                                if (strUserType.equals("parnt")) {

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.L_ID, response.getString("id"));
                                    editor.putString(AppsContants.U_ID, response.getString("child_id"));
                                    editor.putString(AppsContants.USER_NAME, response.getString("first_name"));
                                    editor.putString(AppsContants.EMAIL, response.getString("email"));
                                    editor.commit();

                                    startActivity(new Intent(LoginActivity.this, ParentActivity.class));

                                } else if (strUserType.equals("std")) {

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.L_ID, response.getString("id"));
                                    editor.putString(AppsContants.USER_NAME, response.getString("fname"));
                                    editor.putString(AppsContants.EMAIL, response.getString("email"));
                                    editor.putString(AppsContants.CLASS, response.getString("class"));
                                    editor.putString(AppsContants.SCHOOLNAME, response.getString("school_id"));
                                    editor.commit();


                                    startActivity(new Intent(LoginActivity.this, StudentDashBoard.class));

                                } else {
/*
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.L_ID, response.getString("id"));
                                    editor.putString(AppsContants.U_ID, "");
                                    editor.putString(AppsContants.USER_NAME, response.getString("fname"));
                                    editor.putString(AppsContants.EMAIL, response.getString("email"));
                                    editor.commit();*/

                                    startActivity(new Intent(LoginActivity.this, TeacherDeshBoard.class));
                                    //Toast.makeText(LoginActivity.this, "error", Toast.LENGTH_SHORT).show();

                                }

                                Log.e("wertwetef", response.getString("id"));
                                finish();


                            } else {


                                Toast.makeText(LoginActivity.this, "" + str, Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception ex) {
                            progress.setVisibility(View.GONE);

                            Log.e("hjsdfahfasdjhfasd", ex.getMessage());

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progress.setVisibility(View.GONE);

                        Log.e("hjsdfahfasdjhfasd", anError.toString());

                    }

                });


    }
}

