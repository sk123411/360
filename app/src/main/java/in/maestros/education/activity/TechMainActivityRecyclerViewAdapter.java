package in.maestros.education.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;

public class TechMainActivityRecyclerViewAdapter extends RecyclerView.Adapter<TechRecyclerViewHolders> {

    private List<TechItemObject> itemList;
    private Context context;

    public TechMainActivityRecyclerViewAdapter(Context context, List<TechItemObject> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public TechRecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_tech, null);
        TechRecyclerViewHolders rcv = new TechRecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(TechRecyclerViewHolders holder, int position) {
       // holder.countryName.setText(itemList.get(position).getName());
        holder.countryPhoto.setImageResource(itemList.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}
