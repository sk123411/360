package in.maestros.education.activity.StudentDeshBoard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultDetailResponse {

    @SerializedName("Class_name")
    @Expose
    private String className;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("result_id")
    @Expose
    private String resultId;
    @SerializedName("Subject_name")
    @Expose
    private String subjectName;
    @SerializedName("Assignment")
    @Expose
    private String assignment;
    @SerializedName("Total_Question")
    @Expose
    private String totalQuestion;
    @SerializedName("Correct_Answer")
    @Expose
    private String correctAnswer;
    @SerializedName("Wrong_Answer")
    @Expose
    private String wrongAnswer;
    @SerializedName("Test_date")
    @Expose
    private String testDate;
    @SerializedName("Test_Time")
    @Expose
    private String testTime;
    @SerializedName("Score")
    @Expose
    private String score;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(String totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getWrongAnswer() {
        return wrongAnswer;
    }

    public void setWrongAnswer(String wrongAnswer) {
        this.wrongAnswer = wrongAnswer;
    }

    public String getTestDate() {
        return testDate;
    }

    public void setTestDate(String testDate) {
        this.testDate = testDate;
    }

    public String getTestTime() {
        return testTime;
    }

    public void setTestTime(String testTime) {
        this.testTime = testTime;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

}