package in.maestros.education.activity.ParentDeshBoard.AttendanceAdapter;

public class AttendanceGetSet {

    public String Id;
    public String StudentName;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }
}
