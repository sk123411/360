package in.maestros.education.model;

/**
 * Created by SONU on 25/09/15.
 */
public class SchoolResultModel {

    // Getter and Setter model for recycler view items
    private String assignment;
    private String datetime;
    private String action;
    private String id;
    private String user_id;
    private String session_id;
    private String cls;
    private String correct_answere;
    private String wrong_ans;
    private String test_date;
    private String test_time;
    private String total_qs;
    private String skipped;
    private String score;


    public SchoolResultModel(String assignment, String datetime, String action,String id,String user_id,String session_id,String cls,String correct_answere,String wrong_ans,String test_date,String test_time,String total_qs,String skipped,String  score) {


        this.assignment=assignment;
        this.datetime = datetime;
        this.action=action;
        this.id=id;
        this.user_id=user_id;
        this.session_id=session_id;
        this.cls=cls;
        this.correct_answere=correct_answere;
        this.wrong_ans=wrong_ans;
        this.test_date=test_date;
        this.test_time=test_time;
        this.total_qs=total_qs;
        this.skipped=skipped;
        this.score=score;



    }
    public void setid(String id) {
        this.id = id;
    }
    public void setuser_id(String user_id) {
        this.user_id = user_id;
    }
    public void setsession_id(String session_id) {
        this.session_id = session_id;
    }
    public void setcls(String cls) {
        this.cls = cls;
    }
    public void setcorrect_answere(String correct_answere) {
        this.correct_answere = correct_answere;
    }
    public String getcorrect_answere() {
        return correct_answere;
    }

    public void setwrong_ans(String wrong_ans) {
        this.wrong_ans = wrong_ans;
    } public void settest_date(String test_date) {
        this.test_date = test_date;
    } public void settest_time(String test_time) {
        this.test_time = test_time;
    }




    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }




    public String gettotal_qs() {
        return total_qs;
    }

    public void settotal_qs(String total_qs) {
        this.total_qs = total_qs;
    }

    public String getskipped() {
        return skipped;
    }

    public void setskipped(String skipped) {
        this.skipped = skipped;
    }

    public String getwrong_ans() {
        return wrong_ans;
    }

    public String getscore() {
        return score;
    }
}
