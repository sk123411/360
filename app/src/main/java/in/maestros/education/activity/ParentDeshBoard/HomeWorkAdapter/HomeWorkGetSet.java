package in.maestros.education.activity.ParentDeshBoard.HomeWorkAdapter;

public class HomeWorkGetSet {

    public String Id;
    public String HMClass;
    public String Subject;
    public String Name;
    public String HomeWork;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getHMClass() {
        return HMClass;
    }

    public void setHMClass(String HMClass) {
        this.HMClass = HMClass;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHomeWork() {
        return HomeWork;
    }

    public void setHomeWork(String homeWork) {
        HomeWork = homeWork;
    }

    public String getHMDate() {
        return HMDate;
    }

    public void setHMDate(String HMDate) {
        this.HMDate = HMDate;
    }

    public String HMDate;
}
