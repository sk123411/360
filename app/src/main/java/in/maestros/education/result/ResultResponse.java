package in.maestros.education.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class ResultResponse{
    @SerializedName(("id"))
    public String id;
    @SerializedName("sess_id")
    public String sess_id;
    @SerializedName("class")

    public String classNumber;
    @SerializedName("subject")

    public String subject;
    @SerializedName("assign_name")

    public String assign_name;
    @SerializedName("total_qs")

    public String total_qs;
    @SerializedName("correct_ans")

    public String correct_ans;
    @SerializedName("wrong_ans")

    public String wrong_ans;
    @SerializedName("test_date")

    public String test_date;
    @SerializedName("test_time")

    public String test_time;
    @SerializedName("level")

    public String level;
    @SerializedName("topper")

    public String topper;
    @SerializedName("user_id")

    public String user_id;
    @SerializedName("skipped")

    public String skipped;
    @SerializedName("score")

    public String score;
    @SerializedName("ClassName")
    public String className;
    @SerializedName("Subject")
    public String subject2;
    @SerializedName("first_name")

    public String first_name;
    @SerializedName("last_name")

    public String last_name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSess_id() {
        return sess_id;
    }

    public void setSess_id(String sess_id) {
        this.sess_id = sess_id;
    }

    public String getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(String classNumber) {
        this.classNumber = classNumber;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAssign_name() {
        return assign_name;
    }

    public void setAssign_name(String assign_name) {
        this.assign_name = assign_name;
    }

    public String getTotal_qs() {
        return total_qs;
    }

    public void setTotal_qs(String total_qs) {
        this.total_qs = total_qs;
    }

    public String getCorrect_ans() {
        return correct_ans;
    }

    public void setCorrect_ans(String correct_ans) {
        this.correct_ans = correct_ans;
    }

    public String getWrong_ans() {
        return wrong_ans;
    }

    public void setWrong_ans(String wrong_ans) {
        this.wrong_ans = wrong_ans;
    }

    public String getTest_date() {
        return test_date;
    }

    public void setTest_date(String test_date) {
        this.test_date = test_date;
    }

    public String getTest_time() {
        return test_time;
    }

    public void setTest_time(String test_time) {
        this.test_time = test_time;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTopper() {
        return topper;
    }

    public void setTopper(String topper) {
        this.topper = topper;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSkipped() {
        return skipped;
    }

    public void setSkipped(String skipped) {
        this.skipped = skipped;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubject2() {
        return subject2;
    }

    public void setSubject2(String subject2) {
        this.subject2 = subject2;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
