package in.maestros.education.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.maestros.education.R;
import in.maestros.education.activity.ListResultActivity;
import in.maestros.education.model.ResultModel;
import in.maestros.education.other.AppsContants;


/**
 * Created by w7 on 3/6/2017.
 */

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.MyViewHolder> {
    private ArrayList<ResultModel> arrayList;
    Context c;
    String first = "";

    public ResultAdapter(Context c, ArrayList<ResultModel> arrayList) {
        this.arrayList = arrayList;
        this.c = c;


    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;


        public MyViewHolder(View view) {
            super(view);


            name = (TextView) view.findViewById(R.id.textname);


        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_resultlist, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final ResultModel listModel1 = arrayList.get(position);

        holder.name.setText(listModel1.getcat_name());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AppsContants.sharedpreferences = c.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.SUBJECT_ID,listModel1.getid() );
                editor.putString(AppsContants.SUBJECT_NAME, listModel1.getcat_name());
                editor.putString(AppsContants.STATUS, listModel1.getstatus());
                editor.commit();

                Intent name=new Intent(c, ListResultActivity.class);
                c.startActivity(name);
            }
        });
    }


    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }
}
