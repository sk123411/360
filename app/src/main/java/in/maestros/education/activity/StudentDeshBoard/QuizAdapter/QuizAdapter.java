package in.maestros.education.activity.StudentDeshBoard.QuizAdapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.MyResultAdapter.MyResultGetSet;
import in.maestros.education.activity.QuestionResultActivity;
import in.maestros.education.activity.StudentDeshBoard.QuestionAdapter.QuestionAdapter;
import in.maestros.education.activity.StudentDeshBoard.QuestionAdapter.QuestionGetSet;
import in.maestros.education.activity.StudentDeshBoard.QuestionsListActivity;
import in.maestros.education.activity.StudentDeshBoard.QuizActivity;
import in.maestros.education.model.SubmitResponse;
import in.maestros.education.other.AppsContants;
import io.paperdb.Paper;

public class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.ViewHolder> {

    Context context;
    String L_ID;

    List<QuizGetSet> dataAdapters;
    AppCompatActivity appCompatActivity;


    private ArrayList<String> questionIDs = new ArrayList<>();
    private ArrayList<String> answer = new ArrayList<>();


    public QuizAdapter(List<QuizGetSet> getDataAdapter, Context context, AppCompatActivity appCompatActivity) {
        this.dataAdapters = getDataAdapter;
        this.context = context;
        this.appCompatActivity = appCompatActivity;

    }

    @Override
    public QuizAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quiz_adapter, parent, false);


        ViewHolder viewHolder = new ViewHolder(view);


        // QuizAdapter.ViewHolder viewHolder = new QuizAdapter.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final QuizAdapter.ViewHolder Viewholder, final int position) {

        final QuizGetSet dataAdapterOBJ = dataAdapters.get(position);


        L_ID = AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

        Viewholder.txtQuestion.setText(dataAdapterOBJ.getQuestion());
        Viewholder.radio1.setText(dataAdapterOBJ.getOption1());
        Viewholder.radio2.setText(dataAdapterOBJ.getOption2());
        Viewholder.radio3.setText(dataAdapterOBJ.getOption3());
        Viewholder.radio4.setText(dataAdapterOBJ.getOption4());
        int postition = position + 1;
        Viewholder.txtSerialNo.setText("S.no "+ postition);


        final String[] answerTag = {""};
        final String[] answerVal = {""};

        Viewholder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {


                switch (radioGroup.getId()) {

                    case R.id.radio1:

                        answerTag[0] = Viewholder.radio1.getTag().toString();
                        answerVal[0] = dataAdapterOBJ.getOption1();

                        break;
                    case R.id.radio2:

                        answerTag[0] = Viewholder.radio2.getTag().toString();
                        answerVal[0] = dataAdapterOBJ.getOption2();

                        break;

                    case R.id.radio3:

                        answerTag[0] = Viewholder.radio3.getTag().toString();
                        answerVal[0] = dataAdapterOBJ.getOption3();

                        break;

                    case R.id.radio4:

                        answerTag[0] = Viewholder.radio4.getTag().toString();
                        answerVal[0] = dataAdapterOBJ.getOption4();

                        break;


                }


            }
        });


        if (Viewholder.radio1.isChecked()){
            answerVal[0] = dataAdapterOBJ.getOption1();
        }else if(Viewholder.radio2.isChecked()){
            answerVal[0] = dataAdapterOBJ.getOption2();
        }else if (Viewholder.radio3.isChecked()){
            answerVal[0] = dataAdapterOBJ.getOption3();

        }else {
            answerVal[0] = dataAdapterOBJ.getOption4();

        }



        int lastItem = dataAdapters.size() - 1;
        if (position == lastItem) {
            Viewholder.submitButtton.setText("Final submit");

        }



        Viewholder.submitButtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                questionIDs.add(dataAdapterOBJ.getId());
                answer.add(answerVal[0]);


                int lastItem = dataAdapters.size() - 1;


                if (appCompatActivity instanceof QuizActivity) {

                    if (position == lastItem) {
                        Toast.makeText(appCompatActivity, "You reached the end of the quiz", Toast.LENGTH_LONG).show();


                        String ids = "";
                        String answers = "";
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            ids = String.join(",", questionIDs);
                            answers = String.join(",", answer);

                        }


                        final String finalAnswers = answers;
                        final String finalIds = ids;
                        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=submit_answer")
                                .addBodyParameter("question_id", ids)
                                .addBodyParameter("answer", answers)
                                .addBodyParameter("time",QuizActivity.completedTimeText)
                                .addBodyParameter("total_qs", String.valueOf(dataAdapters.size()))
                                .build().getAsObject(SubmitResponse.class, new ParsedRequestListener<SubmitResponse>() {
                            @Override
                            public void onResponse(SubmitResponse response) {


                                Paper.init(context);
                                Paper.book().write("RESULT",response);

                                context.startActivity(new Intent(view.getContext(), QuestionResultActivity.class));



                               // openDialog(response, view.getContext());


                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });


                    } else {


                        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=quiz")
                                .addBodyParameter("ques_id", dataAdapterOBJ.getId())
                                .addBodyParameter("std_id", L_ID)
                                .addBodyParameter(answerTag[0], answerVal[0])
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {


                                        try {
                                            String result = response.getString("result");


                                            Toast.makeText(view.getContext(), "" + "Answer submitted successfully", Toast.LENGTH_SHORT).show();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onError(ANError anError) {

                                    }
                                });

                        QuizActivity.questionPager.scrollToPosition(position + 1);
                    }
                } else if (appCompatActivity instanceof QuestionsListActivity) {

                    if (position == lastItem) {
                        Toast.makeText(appCompatActivity, "You reached the end of the quiz", Toast.LENGTH_LONG).show();
                        Viewholder.submitButtton.setText("Final submit");

                        String ids = "";
                        String answers = "";
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            ids = String.join(",", questionIDs);
                            answers = String.join(",", answer);

                        }


                        final String finalAnswers = answers;
                        final String finalIds = ids;
                        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=submit_answer")
                                .addBodyParameter("question_id", ids)
                                .addBodyParameter("answer", answers)
                                .addBodyParameter("time",QuestionsListActivity.completedTimeText)
                                .addBodyParameter("total_qs", String.valueOf(dataAdapters.size()))
                                .build().getAsObject(SubmitResponse.class, new ParsedRequestListener<SubmitResponse>() {
                            @Override
                            public void onResponse(SubmitResponse response) {



                                Paper.init(appCompatActivity);
                                Paper.book().write("RESSULT",response);
                                context.startActivity(new Intent(appCompatActivity,QuestionResultActivity.class));

                                // openDialog(response, view.getContext());


                            }

                            @Override
                            public void onError(ANError anError) {


                                Log.d("DDDDDDDDDDDXXXXX", "::" + anError.toString());
                            }
                        });


                    } else {


                        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=quiz")
                                .addBodyParameter("ques_id", dataAdapterOBJ.getId())
                                .addBodyParameter("std_id", L_ID)
                                .addBodyParameter(answerTag[0], answerVal[0])
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {


                                        try {
                                            String result = response.getString("result");


                                            Toast.makeText(view.getContext(), "" + "Answer submitted successfully", Toast.LENGTH_SHORT).show();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onError(ANError anError) {

                                    }
                                });

                        QuestionsListActivity.questionPager.scrollToPosition(position + 1);
                    }

                }


            }

            private void openDialog(SubmitResponse response, Context context) {


                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.submit_answer_dialog);
                dialog.show();

                TextView qTitle = dialog.findViewById(R.id.questionTitle);
                TextView qTotalQues = dialog.findViewById(R.id.questionTotal);
                TextView qCorrect = dialog.findViewById(R.id.questionCorrectAnswer);
                TextView qWrong = dialog.findViewById(R.id.questionWrongAnswer);
                TextView qTime = dialog.findViewById(R.id.questionTime);
                TextView qScore = dialog.findViewById(R.id.questionScore);



                Button doneButton = dialog.findViewById(R.id.doneButton);


                qTitle.setText(response.getResult());

                qTotalQues.setText("Total Questions: " + response.getTotalQs());
                qCorrect.setText("Correct Answer: " + response.getCorrectAns().toString());
                qWrong.setText("Wrong Answer: " + response.getWrongAnswer().toString());
                qScore.setText("Score: " + response.getScore());
                qTime.setText("Time: " + response.getTime());
                qScore.setText("Score: " + response.getCorrectAns());



                doneButton.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                                appCompatActivity.finish();

                                }
                        }
                );


            }
        });


        Viewholder.skipButton.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int lastItem = dataAdapters.size() - 1;


                        if (appCompatActivity instanceof QuizActivity) {


                            if (position == lastItem) {


                            } else {
                                QuizActivity.questionPager.scrollToPosition(position + 1);
                                answer.add("@");

                            }
                        }else {


                            if (position == lastItem) {


                            } else {
                                QuestionsListActivity.questionPager.scrollToPosition(position + 1);
                                answer.add("@");

                            }


                        }

                    }
                }
        );


        //Viewholder.txtQuestion.setText(dataAdapterOBJ.getQuestion());
    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtSerialNo, txtQuestion;

        RadioGroup radioGroup;
        RadioButton radio1, radio2, radio3, radio4;
        RadioGroup optionsRadioGroup;
        public ImageView imgCat;
        public CardView cardCat;
        public Button submitButtton, skipButton;

        public ViewHolder(View itemView) {

            super(itemView);


            cardCat = itemView.findViewById(R.id.cardCat);
            txtQuestion = itemView.findViewById(R.id.txtQuestion);
            txtSerialNo = itemView.findViewById(R.id.txtSerialNo);
            radio1 = itemView.findViewById(R.id.radio1);
            radio2 = itemView.findViewById(R.id.radio2);
            radio3 = itemView.findViewById(R.id.radio3);
            radio4 = itemView.findViewById(R.id.radio4);
            radioGroup = itemView.findViewById(R.id.radioGroup);
            submitButtton = itemView.findViewById(R.id.submitButton);
            skipButton = itemView.findViewById(R.id.skipButton);

        }
    }


  /*  public void InsertQuiz() {


        AndroidNetworking.post("https://360educated.com/Android/process.php?action=insert_quiz")
                .addBodyParameter("user_id", L_ID)
                .addBodyParameter("subject", strLession)
                .addBodyParameter("class", strPassword)
                .addBodyParameter("ques_id", strId)
                .addBodyParameter("user_ans", strPassword)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {


                            String str = response.getString("result");
                            if (str.equals("successful")) {

                                Toast.makeText(QuizActivity.this, "", Toast.LENGTH_SHORT).show();










                                finish();
                                finish();


                            } else {


                                Toast.makeText(QuizActivity.this, "" + str, Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception ex) {

                            Log.e("hjsdfahfasdjhfasd", ex.getMessage());

                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("hjsdfahfasdjhfasd", anError.toString());

                    }

                });*/
}

