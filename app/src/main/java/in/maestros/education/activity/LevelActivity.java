package in.maestros.education.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

public class LevelActivity extends AppCompatActivity {
    ImageView back;
    String CAT_NAME;
    TextView sub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        CAT_NAME = AppsContants.sharedpreferences.getString(AppsContants.CAT_NAME, "");
        back=(ImageView)findViewById(R.id.back);
        sub=(TextView)findViewById(R.id.sub_b) ;
        sub.setText(CAT_NAME);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
