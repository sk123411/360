package in.maestros.education.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.Forgetactivity;
import in.maestros.education.activity.HttpUrlPath;
import in.maestros.education.activity.LoginActivity;
import in.maestros.education.activity.NavigationActivity;
import in.maestros.education.activity.NetConnection;
import in.maestros.education.activity.ParentActivity;
import in.maestros.education.activity.SignupActivity;
import in.maestros.education.activity.StProfileActivity;
import in.maestros.education.activity.TeachcerActivity;
import in.maestros.education.model.TopperlistResultModel;
import in.maestros.education.other.AppsContants;


/**
 * Created by w7 on 3/6/2017.
 */

public class TopperlistAdapter extends RecyclerView.Adapter<TopperlistAdapter.MyViewHolder> {
    private ArrayList<TopperlistResultModel> arrayList;
    Context c;
    String first = "";

    public TopperlistAdapter(Context c, ArrayList<TopperlistResultModel> arrayList) {
        this.arrayList = arrayList;
        this.c = c;


    }




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView classs, score, name;


        public MyViewHolder(View view) {
            super(view);

            classs = (TextView) view.findViewById(R.id.textclass);
            score = (TextView) view.findViewById(R.id.scoretext);
            name = (TextView) view.findViewById(R.id.textfname);


        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_topperlist, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final TopperlistResultModel listModel1 = arrayList.get(position);

        holder.classs.setText(listModel1.getclasss());
        holder.score.setText(listModel1.getscore());
        holder.name.setText(listModel1.getfname());





    }


    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }
}







































  /*  Button login;
    TextView newuser,forget;
    private ProgressDialog pDialog;
    EditText email,pass;
    String s_email,s_pass;

    private RadioGroup radioGroup1;
    private RadioButton radioParent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        email=(EditText)findViewById(R.id.email);
        pass=(EditText)findViewById(R.id.pass);
        radioParent=(RadioButton) findViewById(R.id.radioParent);


        newuser=(TextView)findViewById(R.id.newuser);
        forget=(TextView)findViewById(R.id.forget);


        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, Forgetactivity.class));
            }
        });

        newuser=(TextView)findViewById(R.id.newuser);
        newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i= new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(i);

            }
        });




        login=(Button)findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                *//*int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);

                Toast.makeText(LoginActivity.this,
                        radioButton.getText(), Toast.LENGTH_SHORT).show();*//*



               *//*Intent i= new Intent(LoginActivity.this,NavigationActivity.class);
               startActivity(i);
*//*
                s_email=email.getText().toString().trim();
                s_pass=pass.getText().toString().trim();

                // Toast.makeText(getActivity(), MainActivityForFragment.s_latitude+"Please"+MainActivityForFragment.s_longitude, Toast.LENGTH_SHORT).show();
                boolean isError = false;
                if (s_email.equals("")) {
                    isError = true;
                    email.requestFocus();
                    Toast.makeText(LoginActivity.this, "Please enter email address", Toast.LENGTH_SHORT).show();

                }
                else if (s_pass.equals("")) {
                    isError = true;
                    pass.requestFocus();
                    Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();

                }  else if(!isError){

                    if (NetConnection.isConnected(LoginActivity.this)) {
                        // progressBar.setVisibility(View.VISIBLE);

                        LoginActivity.LoginJsonTask task = new LoginActivity.LoginJsonTask(s_email,s_pass);
                        task.execute();

                    }
                    else {
                        //  Toast.makeText(LoginActivity.this, "Internet connection error", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }

public class LoginJsonTask extends AsyncTask<String, Void, String> {

    String Errormessage;
    String UserRegisterID;
    boolean IsError = false;
    String result="";


    String s_email,s_cls,s_name,s_lnam,s_gender,s_mobile,s_age,s_race;

    String  s_p_fname ="",
            s_t_fname="",
            s_dt="";
    String object;
    String semail="", spwd="";

    public LoginJsonTask(String ss_email, String ss_pwd)
    {
        this.semail = ss_email;
        this.spwd = ss_pwd;
    }

    @Override

    protected void onPreExecute() {
        super.onPreExecute();
        // progressBar.setVisibility(View.VISIBLE);
        // loader.setVisibility(View.VISIBLE);
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

    }
    protected String doInBackground(String... arg0) {

        DefaultHttpClient htppclient=new DefaultHttpClient();
        //   HttpPost httppost=new HttpPost(HttpUrlPath.urlPath+HttpUrlPath.actionLogin);
        HttpPost httppost=new HttpPost(HttpUrlPath.urlPath + HttpUrlPath.actionLogin);


        try{
            List<NameValuePair> nameValuePair=new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("email",semail));
            nameValuePair.add(new BasicNameValuePair("password",spwd));
            // nameValuePair.add(new BasicNameValuePair("gcm_id", MainActivityForFragment.regId));
            //nameValuePair.add(new BasicNameValuePair("latitude",MainActivityForFragment.s_latitude));
            //nameValuePair.add(new BasicNameValuePair("longitude",MainActivityForFragment.s_longitude));


            httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));
            HttpResponse response = htppclient.execute(httppost);
            object = EntityUtils.toString(response.getEntity());
            System.out.println("#####object registration=" + object);
            JSONObject js = new JSONObject(object);


            // String s_email,s_gender,s_password,s_firstname,s_lastname,s_dob,s_mobile,s_country,s_city;



            if (js.has("id")) {
                UserRegisterID = js.getString("id");
                s_name = js.getString("fname");
                s_lnam=js.getString("lname");
                s_email=js.getString("email");
                s_cls=js.getString("class");
                s_p_fname = js.getString("p_fname");
                s_t_fname=js.getString("t_fname");
                s_dt=js.getString("dt");

                //s_city=js.getString("city");
                Errormessage = js.getString("result");


            } else if (js.has("result")) {
                Errormessage = js.getString("result");

                Log.e("dhrthrthrth",UserRegisterID);

            }

        }
        catch (Exception e){
            IsError = true;
            Log.v("22", "22" + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }



    protected void onPostExecute(String result1){
        super.onPostExecute(result1);
        if (pDialog.isShowing())
            pDialog.dismiss();
        // loader.setVisibility(View.GONE);
        //progressBar.setVisibility(View.GONE);
        //Toast.makeText(getActivity(),"f_n="+s_dob,Toast.LENGTH_LONG).show();
        if(!IsError){


            Log.e("fjfhfhhhfhh",IsError+"");
            if(Errormessage.equals("successful")){


                Log.e("it87oi8ki","uj6t8"+UserRegisterID);
                if (UserRegisterID != null) {

                    Log.v("22", "22" + UserRegisterID);
                    if (!UserRegisterID.equals("")) {

                        Log.e("jhdvhkhk","amit");

                        //  Toast.makeText(LoginActivity.this,"your login has been "+Errormessage,Toast.LENGTH_SHORT).show();

                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                        editor.putString(AppsContants.L_ID, UserRegisterID);
                        editor.putString(AppsContants.USER_NAME, s_name);
                        editor.putString(AppsContants.LNAME, s_lnam);
                        editor.putString(AppsContants.EMAIL, s_email);
                        editor.putString(AppsContants.CLASS, s_cls);
                        editor.commit();


                        if (s_dt.equals("")){




                            Log.e("hsjiguf",s_dt+"");
                            Intent i= new Intent(LoginActivity.this, StProfileActivity.class);
                            startActivity(i);
                            finish();

                        }else {


                            if (s_p_fname.equals("")){
                                Intent i= new Intent(LoginActivity.this, ParentActivity.class);
                                startActivity(i);
                                finish();

                            }else {

                                if (s_t_fname.equals("")){
                                    Intent i= new Intent(LoginActivity.this, TeachcerActivity.class);
                                    startActivity(i);
                                    finish();

                                }else {

                                    Intent i= new Intent(LoginActivity.this, NavigationActivity.class);
                                    startActivity(i);
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                                    editor1.putString(AppsContants.U_ID, UserRegisterID);

                                    editor1.commit();

                                    finish();

                                }
                            }



                        }




                    }

                }
            }else {


                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("360Educating...");

                // Setting Dialog Message
                alertDialog.setMessage("Please activate your account with your email..");

                // Setting Icon to Dialog

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {

                        dialog.dismiss();

                    }
                });

                // Showing Alert Message
                alertDialog.show();


                // Toast.makeText(LoginActivity.this, Errormessage,Toast.LENGTH_SHORT).show();


            }


        }else {
            // Toast.makeText(LoginActivity.this, "please try again...",Toast.LENGTH_SHORT).show();


        }

    }


}
}
*/