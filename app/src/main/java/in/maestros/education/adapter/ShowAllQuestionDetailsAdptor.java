package in.maestros.education.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.NatureMyLocationText;
import in.maestros.education.activity.QuesAnsActivity;
import in.maestros.education.other.AppsContants;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ShowAllQuestionDetailsAdptor extends  RecyclerView.Adapter<ShowAllQuestionDetailsAdptor.ViewHolder> {




    List<NatureMyLocationText> mItems;
    Context a;
    SharedPreferences sharedpreferences;
    public static final String CHAT_ID = "chatidKey";
    public static final String CHAT_NAME = "chatnameKey";
    public static final String MyPREFERENCES = "MyPrefs" ;


    public ShowAllQuestionDetailsAdptor(Context a, List<NatureMyLocationText> products) {
        super();
        mItems = new ArrayList<NatureMyLocationText>();
        this.mItems = products;
        this.a = a;

        /*
        NatureItem nature = new NatureItem();
       // nature.setName("The Great Barrier Reef");
       // nature.setDes("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt" +
               //       "ut labore et dolore magna aliqua. Ut enim ad minim veniam.");
        nature.setThumbnail(R.drawable.ic_launcher);
        mItems.add(nature);

        nature = new NatureItem();
        //nature.setName("Grand Canyon");
       // nature.setDes("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt" +
            //    "ut labore et dolore magna aliqua.");
        nature.setThumbnail(R.drawable.ic_launcher);
        mItems.add(nature);

        nature = new NatureItem();
       // nature.setName("Baltoro Glacier");
       // nature.setDes("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt" +
         //       "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.");
        nature.setThumbnail(R.drawable.ic_launcher);
        mItems.add(nature);

        nature = new NatureItem();
       // nature.setName("Iguazu Falls");
       // nature.setDes("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt" +
        //        "ut labore et dolore magna aliqua. Ut enim ad minim veniam.");
        nature.setThumbnail(R.drawable.ic_launcher);
        mItems.add(nature);


        nature = new NatureItem();
      //  nature.setName("Aurora Borealis");
       // nature.setDes("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt" +
       //               "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.");
        nature.setThumbnail(R.drawable.ic_launcher);
        mItems.add(nature);*/
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_card_showallquetionsdetails, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final NatureMyLocationText nature = mItems.get(i);
        viewHolder.location.setText(nature.getLevel());

        viewHolder.location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AppsContants.sharedpreferences = a.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                editor1.putString(AppsContants.LEVEL, nature.getLevel());
                editor1.putString(AppsContants.LEVEL_ID, nature.getLevel_id());
                editor1.putString(AppsContants.subject_id, nature.getSubject_id());
                editor1.commit();



               Intent i=new Intent(a,QuesAnsActivity.class);
                 a.startActivity(i);



            }
        });

        //viewHolder.minimum.setVisibility(View.GONE);
     /*   viewHolder.minimum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(a,"next",Toast.LENGTH_LONG).show();





                Intent i=new Intent(a,QuesAnsActivity.class);
                a.startActivity(i);
            }
        });*/



    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ;
       public Button location;
        public Button status;
       // public Button minimum;
       // public TextView delivery;
       // public TextView payment;
       // public TextView amount;




        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            location = (Button) itemView.findViewById(R.id.b1);
         //  status = (Button) itemView.findViewById(R.id.b2);
           // minimum = (Button) itemView.findViewById(R.id.b3);
           // delivery = (TextView) itemView.findViewById(R.id.tvtime);
           // payment = (TextView) itemView.findViewById(R.id.tvpay);
           // amount = (TextView) itemView.findViewById(R.id.tvamount);



        }

        @Override
        public void onClick(View v) {






        }
    }
}


