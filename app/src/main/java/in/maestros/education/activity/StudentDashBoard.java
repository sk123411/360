package in.maestros.education.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.facebook.FacebookSdk;

import in.maestros.education.FaceboookNewActivity;
import in.maestros.education.R;
import in.maestros.education.activity.StudentDeshBoard.ChooseLesson;
import in.maestros.education.activity.StudentDeshBoard.HomeWorkActivity;
import in.maestros.education.activity.StudentDeshBoard.MyResult2;
import in.maestros.education.activity.StudentDeshBoard.Question;
import in.maestros.education.activity.StudentDeshBoard.SchemeWorkActivity;
import in.maestros.education.activity.StudentDeshBoard.TimeTable;
import in.maestros.education.other.AppsContants;

public class StudentDashBoard extends AppCompatActivity {


ImageView imgOption;
CardView cardMyResult,cardLesson,cardTimeTable,cardQuestions,cardWork,cardProfile;
    protected static boolean isMainShown = false;

    LinearLayout cardHomeWork;
String profileimage="",userid="",L_ID="",Name11="",Name12="",Name13="";

    private static boolean isHelpShown = false;
    protected static boolean isInfoShown = false;
    private ImageView profileimg;
    private static boolean isAShown = false;
    private static boolean isBShown = false;


    private static enum Fragments {
        MAIN,
        HELP,
        INFO,
        A,
        B,
        TASK;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_student_dash_board);


        cardMyResult=findViewById(R.id.cardMyResult);
        cardLesson=findViewById(R.id.cardLesson);
        cardTimeTable=findViewById(R.id.cardTimeTable);
        cardLesson=findViewById(R.id.cardLesson);
        cardQuestions=findViewById(R.id.cardQuestions);
        cardWork=findViewById(R.id.cardWork);
        cardHomeWork = findViewById(R.id.cardHomeWork);
        cardProfile = findViewById(R.id.cardProfile);


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        profileimage=AppsContants.sharedpreferences.getString(AppsContants.PROFILEIMAGE,"");
        userid = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        L_ID=AppsContants.sharedpreferences.getString(AppsContants.L_ID, "");

        Name11=AppsContants.sharedpreferences.getString(AppsContants.USER_NAME, "");
        Name12=AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        Name13=AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");






        imgOption = findViewById(R.id.imgOption);

        cardHomeWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StudentDashBoard.this, HomeWorkActivity.class));

            }
        });
        cardProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StudentDashBoard.this, UpdateStudentProfile.class));

            }
        });

        cardQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StudentDashBoard.this, Question.class));
            }
        });
        cardLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StudentDashBoard.this, ChooseLesson.class));
            }
        });



        cardTimeTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StudentDashBoard.this, TimeTable.class));
            }
        });


        cardMyResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StudentDashBoard.this, MyResult2.class));
            }
        });


        cardWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StudentDashBoard.this, SchemeWorkActivity.class));
            }
        });




        imgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final PopupMenu popup = new PopupMenu(StudentDashBoard.this,imgOption);

                popup.getMenuInflater().inflate(R.menu.home_menu,popup.getMenu());
                /*  popup.getMenuInflater().inflate(R.menu.contact_fragment_menues, popup.getMenu());*/

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(final MenuItem item) {


                        if (item.getItemId() == R.id.update_profile) {

                            startActivity(new Intent(StudentDashBoard.this, UpdateStudentProfile.class));


                        }  else if (item.getItemId() == R.id.logout) {





                            AlertDialog.Builder builder;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                builder = new AlertDialog.Builder(StudentDashBoard.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                            } else {
                                builder = new AlertDialog.Builder(StudentDashBoard.this);
                            }
                            builder.setTitle(StudentDashBoard.this.getResources().getString(R.string.app_name))
                                    .setMessage("Confirm logout?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                        public void onClick(final DialogInterface dialog, int which) {

                                            final ProgressDialog progressDialog = new ProgressDialog(StudentDashBoard.this);
                                            progressDialog.setMessage("Logging You Out.....");
                                            progressDialog.show();
                                            final Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                                @Override
                                                public void run() {

                                                    if (progressDialog.isShowing()) {
                                                        progressDialog.dismiss();
                                                    }
                                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                    editor.putString(AppsContants.U_ID, "");
                                                    editor.putString(AppsContants.L_ID, "");
                                                    editor.putString(AppsContants.USER_NAME, "");
                                                    editor.putString(AppsContants.LNAME, "");
                                                    editor.putString(AppsContants.EMAIL, "");
                                                    editor.putString(AppsContants.CLASS, "");
                                                    editor.commit();

                                                    Intent i = new Intent(StudentDashBoard.this, FaceboookNewActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(i);

                                                }
                                            }, 2000);
                                            dialog.dismiss();

                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            dialog.dismiss();

                                        }
                                    })
                                    .show();





                        }

                        return true;
                    };
                });
                popup.show();
            };
        });
              /*  final PopupMenu popup = new PopupMenu(StudentDashBoard.this, imgOption);

                popup.getMenuInflater().inflate(R.menu.home_menu, popup.getMenu());


                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(final MenuItem item) {


                        android.app.AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new android.app.AlertDialog.Builder(StudentDashBoard.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                        } else {
                            builder = new android.app.AlertDialog.Builder(StudentDashBoard.this);
                        }
                        builder.setTitle(StudentDashBoard.this.getResources().getString(R.string.app_name))
                                .setMessage("Confirm logout?")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                    public void onClick(final DialogInterface dialog, int which) {

                                        final ProgressDialog progressDialog = new ProgressDialog(StudentDashBoard.this);
                                        progressDialog.setMessage("Logging You Out.....");
                                        progressDialog.show();
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                            @Override
                                            public void run() {

                                                if (progressDialog.isShowing()) {
                                                    progressDialog.dismiss();
                                                }
                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                editor.putString(AppsContants.U_ID, "");
                                                editor.putString(AppsContants.L_ID, "");
                                                editor.putString(AppsContants.USER_NAME, "");
                                                editor.putString(AppsContants.LNAME, "");
                                                editor.putString(AppsContants.EMAIL, "");
                                                editor.putString(AppsContants.CLASS, "");
                                                editor.commit();

                                                Intent i = new Intent(StudentDashBoard.this, SplashActivity.class);
                                                startActivity(i);
                                                finishAffinity();

                                            }
                                        }, 2000);
                                        dialog.dismiss();

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();

                                    }
                                })
                                .show();

                        return true;
                };
            })

            ;
                popup.show();
        };



    });*/
    }

}

