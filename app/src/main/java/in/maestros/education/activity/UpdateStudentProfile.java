package in.maestros.education.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.nostra13.universalimageloader.utils.L;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.maestros.education.R;
import in.maestros.education.activity.StudentDeshBoard.UserProfileResponse;
import in.maestros.education.other.AppsContants;

public class UpdateStudentProfile extends AppCompatActivity {



    CircleImageView imageProfile;

     ImageView select_Img;

     File file;

     EditText sname,dob,class1,gender,email;
     Button btn_Update;



    String urlString = "";

    public static List<String> mDetials123 = new ArrayList<String>();


    File imageFile = null;


    String strname="",strGender="",strClass="",strEmail="",strPassword="",strConfirmPassword="",strCategory="",
            strDob="",Question="";

    List<String> arrayListId,strId;
    List<String> arrayListUserName;

    String L_ID="",Name11="",Name12="",strUserName="",SchoolName="",st_name="",s_email="";

    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_student__profile);


        btn_Update = findViewById(R.id.btn_Update);
        imageProfile = findViewById(R.id.imageProfile);
        select_Img = findViewById(R.id.select_Img);
        sname = findViewById(R.id.sname);
        dob = findViewById(R.id.dob);
        class1 = findViewById(R.id.class1);
        gender = findViewById(R.id.gender);
        email = findViewById(R.id.email);
        progressBar = findViewById(R.id.progressBarr);





        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);


        L_ID = AppsContants.sharedpreferences.getString(AppsContants.L_ID, "51");
        Name12 = AppsContants.sharedpreferences.getString(AppsContants.LNAME, "");
        SchoolName = AppsContants.sharedpreferences.getString(AppsContants.SCHOOLNAME, "");
        strEmail = AppsContants.sharedpreferences.getString(AppsContants.EMAIL, "");
        Name11 = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        Log.e("hgfdsjfgdsjfgd",L_ID);






        progressBar.setVisibility(View.VISIBLE);

        AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=student_update_profile")
                .addBodyParameter("id", L_ID).build()
                .getAsObject(UserProfileResponse.class, new ParsedRequestListener<UserProfileResponse>() {
                    @Override
                    public void onResponse(UserProfileResponse response) {
                    progressBar.setVisibility(View.GONE);


                        sname.setText(response.getFname());
                        dob.setText(response.getDob());
                        class1.setText(response.getClass_());
                        gender.setText(response.getGender());

                        email.setText(response.getEmail());

                        Picasso.with(UpdateStudentProfile.this).load(response.getPath()+response.getImg())
                                .placeholder(R.drawable.chosse_profile_pic).into(imageProfile);


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });




        btn_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TAGGGG", "clicked");
                strUserName = sname.getText().toString().trim();
                strEmail = email.getText().toString().trim();
                strDob = dob.getText().toString().trim();
                strClass = class1.getText().toString().trim();
                strGender = gender.getText().toString().trim();

                if (imageFile!=null) {

                    Log.d("TAGGGG", "run");



                    if (!strUserName.equals("") || !strEmail.equals("") || !strDob.equals("") || !strClass.equals("") || !strGender.equals("")) {

                        progressBar.setVisibility(View.VISIBLE);
                        AndroidNetworking.upload("https://360educated.com/360educated/api/process.php?action=student_update_profile")
                                .addMultipartFile("img", imageFile)
                                .addMultipartParameter("id","51")

                                .addMultipartParameter("fname",strUserName)
                                .addMultipartParameter("email",strEmail)
                                .addMultipartParameter("dob",strDob)
                                .addMultipartParameter("class",strClass)
                                .addMultipartParameter("gender",strGender).build()
                                .getAsObject(UserProfileResponse.class, new ParsedRequestListener<UserProfileResponse>() {
                                    @Override
                                    public void onResponse(UserProfileResponse response) {

                                        progressBar.setVisibility(View.GONE);


                                        Toast.makeText(getApplicationContext(),"profile updated",Toast.LENGTH_LONG).show();



                                    }

                                    @Override
                                    public void onError(ANError anError) {
                                        Log.d("TAGGGG", "err" + anError.getErrorDetail());

                                    }
                                });


                    }
                }else {

                    progressBar.setVisibility(View.VISIBLE);


                    AndroidNetworking.post("https://360educated.com/360educated/api/process.php?action=student_update_profile")
                            .addBodyParameter("id","51")
                            .addBodyParameter("fname",strUserName)
                            .addBodyParameter("email",strEmail)
                            .addBodyParameter("dob",strDob)
                            .addBodyParameter("class",strClass)
                            .addBodyParameter("gender",strGender).build()
                            .getAsObject(UserProfileResponse.class, new ParsedRequestListener<UserProfileResponse>() {
                                @Override
                                public void onResponse(UserProfileResponse response) {


                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(),"profile updated",Toast.LENGTH_LONG).show();








                                }

                                @Override
                                public void onError(ANError anError) {
                                    progressBar.setVisibility(View.GONE);

                                    Log.d("TAGGGG", "err" + anError.getMessage());

                                }
                            });





                }



                /*  progress.setVisibility(View.VISIBLE);*/
//                        new Thread(new Runnable() {
//                            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                            public void run() {
//
//                                UpdateProfile(mDetials123, HttpUrlPath.urlPath + HttpUrlPath.actionUpdateProfile);
//
//
//                            }
//                        }).start();


            }
        });







        mDetials123 = new ArrayList<>();

        sname.setText(Name12);
        email.setText(strEmail);


        select_Img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // selectImage();



                Dexter.withContext(UpdateStudentProfile.this)
                        .withPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                                ImagePicker.Companion.with(UpdateStudentProfile.this)
                                        .crop()	    			//Crop image(Optional), Check Customization for more option
                                        .compress(1024)			//Final image size will be less than 1 MB(Optional)
                                        .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                                        .start();

                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                            }
                        }).check();

            }
        });



    }


        public void selectImage() {

            final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

            AlertDialog.Builder builder = new AlertDialog.Builder(UpdateStudentProfile.this);
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, 1);
                    } else if (options[item].equals("Choose from Gallery")) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, 2);

                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        @Override
        public void onActivityResult ( int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {


                Uri uri = data.getData();
                imageProfile.setImageURI(uri);



               imageFile =  ImagePicker.Companion.getFile(data);









            }


















//            if (resultCode == RESULT_OK) {
//                if (requestCode == 1) {
//                    File f = new File(Environment.getExternalStorageDirectory().toString());
//                    for (File temp : f.listFiles()) {
//                        if (temp.getName().equals("temp.jpg")) {
//                            f = temp;
//                            break;
//                        }
//                    }
//                    try {
//                        Bitmap bitmap;
//                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
//
//                        bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
//                                bitmapOptions);
//
//                        imageProfile.setImageBitmap(bitmap);
//                        String path = android.os.Environment
//                                .getExternalStorageDirectory()
//                                + File.separator
//                                + "Phoenix" + File.separator + "default";
//                        f.delete();
//                        OutputStream outFile = null;
//                        File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
//                        try {
//                            outFile = new FileOutputStream(file);
//                            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
//                            outFile.flush();
//                            outFile.close();
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } else if (requestCode == 2) {
//
//                    Uri selectedImage = data.getData();
//                    String[] filePath = {MediaStore.Images.Media.DATA};
//                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
//                    c.moveToFirst();
//                    int columnIndex = c.getColumnIndex(filePath[0]);
//                    String picturePath = c.getString(columnIndex);
//                    mDetials123.add(picturePath);
//                    c.close();
//                    Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
//                    imageProfile.setImageBitmap(thumbnail);
//
//                }
//            }


        }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void UpdateProfile(final List<String> mDetials, String url) {
        List<File> mFiles = new ArrayList<>();
        for (int i = 0; i < mDetials.size(); i++) {
            String mFileLocation = (mDetials.get(i));
            File file = new File(mFileLocation);
            mFiles.add(file);
        }
        urlString = url;

        try {

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            List<FileBody> mFileBody = new ArrayList<>();
            for (int i = 0; i < mFiles.size(); i++) {
                FileBody bin1 = new FileBody(mFiles.get(i));
                mFileBody.add(bin1);
            }
            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
            reqEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            for (int i = 0; i < mFileBody.size(); i++) {
                reqEntity.addPart("image", mFileBody.get(i));
            }


            reqEntity.addTextBody("user_id", String.valueOf(L_ID), ContentType.TEXT_PLAIN);
            //reqEntity.addTextBody("mobile", strName, ContentType.TEXT_PLAIN.withCharset("UTF-8"));

            if (!strname.equals("")) {
                reqEntity.addTextBody("name", strUserName, ContentType.TEXT_PLAIN.withCharset("UTF-8"));
            }
            if (!strEmail.equals("")) {
                reqEntity.addTextBody("email", strEmail, ContentType.TEXT_PLAIN.withCharset("UTF-8"));
            }


            post.setEntity(reqEntity.build());
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);
            Log.e("iurehteiurhg", response_str);
            if (resEntity != null) {
                Log.i("RESPONSE", response_str);
                UpdateStudentProfile.this.runOnUiThread(new Runnable() {
                    public void run() {
                        try {

                            /* progress.setVisibility(View.GONE);*/
                            JSONObject js = new JSONObject(response_str);

                            if (js.getString("result").equalsIgnoreCase("successfully")) {






                                sname.setText(js.getString("p_fname"));
                                dob.setText(js.getString("dob"));
                                class1.setText(js.getString("class"));
                                gender.setText(js.getString("gender"));
                                email.setText(js.getString("email"));


                                Log.e("eruhfdsa", js.getString("email"));





                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.L_ID, js.getString("id"));
                                editor.putString(AppsContants.USER_NAME, js.getString("p_fname"));
                                editor.putString(AppsContants.Dob, js.getString("dob"));
                                editor.putString(AppsContants.CLASS, js.getString("class"));
                                editor.putString(AppsContants.Gender, js.getString("gender"));
                                editor.putString(AppsContants.EMAIL, js.getString("email"));
                                editor.commit();


                                Toast.makeText(UpdateStudentProfile.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(UpdateStudentProfile.this, "" + js.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            Log.e("Debug", "user_id " + e.getMessage(), e);
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception ex) {

            Log.e("Debug", "error:" + ex.getMessage(), ex);

            // dialog.dismiss();
        }
    }










    }




















