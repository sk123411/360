package in.maestros.education.activity.ParentDeshBoard.AssignmentAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.maestros.education.R;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentAdapter;
import in.maestros.education.activity.ParentDeshBoard.AssesmentAdapter.AssesmentGetSet;

public class AssignmentAdapter extends RecyclerView.Adapter<AssignmentAdapter.ViewHolder> {

    Context context;


    List<AssignmentGetSet> dataAdapters;


    String strCatID="",strId="",strCatName="";









    public AssignmentAdapter(List<AssignmentGetSet> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;

    }

    @Override
    public AssignmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.assignment_adapter, parent, false);



        AssignmentAdapter.ViewHolder viewHolder = new AssignmentAdapter.ViewHolder(view);











        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AssignmentAdapter.ViewHolder Viewholder, int position) {

        final AssignmentGetSet dataAdapterOBJ = dataAdapters.get(position);




        // Toast.makeText(context, ""+dataAdapterOBJ.getQuestion(), Toast.LENGTH_SHORT).show();


        Viewholder.txtStudent.setText(dataAdapterOBJ.getStudentName());
        Viewholder.txtClass.setText(dataAdapterOBJ.getCllass());
        Viewholder.txtAssignment.setText(dataAdapterOBJ.getAssignmentName());
        Viewholder.txtSubject.setText(dataAdapterOBJ.getSubject());






    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }





    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtStudent,txtClass,txtAssignment,txtSubject;




        public ViewHolder(View itemView) {

            super(itemView);



            txtStudent = itemView.findViewById(R.id.txtStudent);
            txtClass = itemView.findViewById(R.id.txtClass);
            txtAssignment = itemView.findViewById(R.id.txtAssignment);
            txtSubject = itemView.findViewById(R.id.txtSubject);



        }
    }
}