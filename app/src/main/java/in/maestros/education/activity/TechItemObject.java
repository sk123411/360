package in.maestros.education.activity;


public class TechItemObject {

    private String name;
    private int photo;

    
    public TechItemObject(String name, int photo) {
        this.name = name;
        this.photo = photo;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }
    public void setPhoto(int photo) {
        this.photo = photo;
    }


}
