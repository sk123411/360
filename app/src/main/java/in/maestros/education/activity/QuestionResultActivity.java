package in.maestros.education.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.maestros.education.R;
import in.maestros.education.model.SubmitResponse;
import io.paperdb.Paper;

public class QuestionResultActivity extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_result);


        Paper.init(getApplicationContext());
        SubmitResponse submitResponse = Paper.book().read("RESULT");


        TextView totalQuestionText = findViewById(R.id.questionTotal);
        TextView questionCorrectAnswer = findViewById(R.id.questionCorrectAnswer);
        TextView questionWrongAnswer = findViewById(R.id.questionWrongAnswer);
        TextView qScore = findViewById(R.id.questionScore);

        TextView questionTime = findViewById(R.id.questionTime);




        totalQuestionText.setText("Total Questions: " + submitResponse.getTotalQs());
        questionCorrectAnswer.setText("Correct Answer: " +submitResponse.getCorrectAns());
        questionWrongAnswer.setText("Wrong Answer: " + submitResponse.getWrongAnswer().toString());
        qScore.setText("Score: " + submitResponse.getCorrectAns());

        questionTime.setText("Time: " +submitResponse.getTime());

        Button submit = findViewById(R.id.doneDoneButton);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                startActivity(new Intent(getApplicationContext(),StudentDashBoard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

            }
        });


        ImageView backImage = findViewById(R.id.back);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(),StudentDashBoard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));



    }
}