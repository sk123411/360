package in.maestros.education.activity.ParentDeshBoard.ResultAdapter;

public class ResultGetSet {


    public String Id;

    public String getClas() {
        return Clas;
    }

    public void setClas(String clas) {
        Clas = clas;
    }

    public String Clas;
    public String Subject;
    public String StudentName;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }




    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getTestDate() {
        return TestDate;
    }

    public void setTestDate(String testDate) {
        TestDate = testDate;
    }

    public String TestDate;

}
