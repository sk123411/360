package in.maestros.education.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import in.maestros.education.R;
import in.maestros.education.other.AppsContants;

public class ViewProfileActivity extends AppCompatActivity {


    List<String> categories = new ArrayList<String>();
    List<String> categories1 = new ArrayList<String>();
    static JSONArray contactsstate1 = null;
    LinearLayout ll11;
    String ss_class = "";

    String s_fname = "", s_email="",s_lname = "", s_dob = "", s_class = "", s_gender = "", p_fname = "", p_lname = "", p_state = "", p_address = "", p_number = "", p_email = "", t_fname = "", t_lname = "", t_state = "", t_address = "", t_number = "", t_email = "";


    public static List<String> img_array = new ArrayList<String>();
    List<Uri> mDetials123 = new ArrayList<Uri>();
    long length = 0;
    private static int RESULT_LOAD_IMG = 10;
    Gallery gallery;
    private TextView messageText;
    private Button uploadButton, btnselectpic;
    private ProgressDialog dialog = null;
    private String filepath = "";
    int FLAG = 0;

    String class_type = "";

    ImageLoader imageLoader;

    TextView dob, class1, gender, sname;
    EditText email, pflname, state, add, number, email1, tname, state1, add1, num, email2;
    String Name11="",profileimage="";
    Spinner spsp1;
    ImageView img, edit, save, back;
    private ImageView profileimg;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        ll11 = (LinearLayout) findViewById(R.id.ll11);
        ll11.setVisibility(View.GONE);

        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        progressBar.setVisibility(View.GONE);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        Name11 = AppsContants.sharedpreferences.getString(AppsContants.U_ID, "");
        profileimage=AppsContants.sharedpreferences.getString(AppsContants.FACEBOOK_PROFILE,"");
        s_email=AppsContants.sharedpreferences.getString(AppsContants.EMAIL,"");


        Log.e("jhsdyfgdgf",Name11);

        profileimg=(ImageView)findViewById(R.id.profileimg);
       // Picasso.with(ViewProfileActivity.this).load(profileimage).into(profileimg);

     /* if(!profileimage.equals("")) {
          Picasso.with(ViewProfileActivity.this).load(profileimage).into(profileimg);
        }else {
            Picasso.with(ViewProfileActivity.this).load(profileimage).into(profileimg);
        }*/
           // Toast.makeText(ViewProfileActivity.this, profileimg, Toast.LENGTH_SHORT).show();
        sname = (TextView) findViewById(R.id.sname);
        dob = (TextView) findViewById(R.id.dob);
        class1 = (TextView) findViewById(R.id.class1);
        gender = (TextView) findViewById(R.id.gender);

        spsp1 = (Spinner) findViewById(R.id.spsp1);
        email = (EditText) findViewById(R.id.email);

        pflname = (EditText) findViewById(R.id.pflname);
        state = (EditText) findViewById(R.id.state);
        add = (EditText) findViewById(R.id.add);
        number = (EditText) findViewById(R.id.number);
        email1 = (EditText) findViewById(R.id.email1);

        tname = (EditText) findViewById(R.id.tname);
        state1 = (EditText) findViewById(R.id.state1);
        add1 = (EditText) findViewById(R.id.add1);
        num = (EditText) findViewById(R.id.num);
        email2 = (EditText) findViewById(R.id.email2);


        /*spsp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ss_class = categories.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        sname.setFocusable(false);
        dob.setFocusable(false);
        class1.setFocusable(false);
        gender.setFocusable(false);
        email.setFocusable(false);

        pflname.setFocusable(false);
        state.setFocusable(false);
        add.setFocusable(false);
        number.setFocusable(false);
        email1.setFocusable(false);

        tname.setFocusable(false);
        state1.setFocusable(false);
        add1.setFocusable(false);
        num.setFocusable(false);
        email2.setFocusable(false);

        img = (ImageView) findViewById(R.id.img);
        edit = (ImageView) findViewById(R.id.edit);
        save = (ImageView) findViewById(R.id.save);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent iop = new Intent(ViewProfileActivity.this, NavigationActivity.class);
                startActivity(iop);
                finishAffinity();
            }
        });


        if (NetConnection.isConnected(ViewProfileActivity.this)) {

            SignUpjsontask11 task = new SignUpjsontask11();
            task.execute();

        } else {

            Toast.makeText(ViewProfileActivity.this, "Internet connection error", Toast.LENGTH_LONG).show();
        }


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               // spsp1.setVisibility(View.VISIBLE);
               // class1.setVisibility(View.GONE);


                ll11.setVisibility(View.VISIBLE);


               /* sname.setFocusableInTouchMode(false);
                sname.setFocusable(false);
                sname.setFocusableInTouchMode(true);
                sname.setFocusable(true);*/

               /* dob.setFocusableInTouchMode(false);
                dob.setFocusable(false);
                dob.setFocusableInTouchMode(true);
                dob.setFocusable(true);

                class1.setFocusableInTouchMode(false);
                class1.setFocusable(false);
                class1.setFocusableInTouchMode(true);
                class1.setFocusable(true);

                gender.setFocusableInTouchMode(false);
                gender.setFocusable(false);
                gender.setFocusableInTouchMode(true);
                gender.setFocusable(true);*/

                email.setFocusableInTouchMode(false);
                email.setFocusable(false);
                email.setFocusableInTouchMode(true);
                email.setFocusable(true);

                pflname.setFocusableInTouchMode(false);
                pflname.setFocusable(false);
                pflname.setFocusableInTouchMode(true);
                pflname.setFocusable(true);

                state.setFocusableInTouchMode(false);
                state.setFocusable(false);
                state.setFocusableInTouchMode(true);
                state.setFocusable(true);

                add.setFocusableInTouchMode(false);
                add.setFocusable(false);
                add.setFocusableInTouchMode(true);
                add.setFocusable(true);

                number.setFocusableInTouchMode(false);
                number.setFocusable(false);
                number.setFocusableInTouchMode(true);
                number.setFocusable(true);

                email1.setFocusableInTouchMode(false);
                email1.setFocusable(false);
                email1.setFocusableInTouchMode(true);
                email1.setFocusable(true);

                tname.setFocusableInTouchMode(false);
                tname.setFocusable(false);
                tname.setFocusableInTouchMode(true);
                tname.setFocusable(true);

                state1.setFocusableInTouchMode(false);
                state1.setFocusable(false);
                state1.setFocusableInTouchMode(true);
                state1.setFocusable(true);

                add1.setFocusableInTouchMode(false);
                add1.setFocusable(false);
                add1.setFocusableInTouchMode(true);
                add1.setFocusable(true);

                num.setFocusableInTouchMode(false);
                num.setFocusable(false);
                num.setFocusableInTouchMode(true);
                num.setFocusable(true);

                email2.setFocusableInTouchMode(false);
                email2.setFocusable(false);
                email2.setFocusableInTouchMode(true);
                email2.setFocusable(true);
                edit.setVisibility(View.GONE);
                save.setVisibility(View.VISIBLE);


            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spsp1.setVisibility(View.GONE);
                class1.setVisibility(View.VISIBLE);
                sname.setFocusable(false);
                dob.setFocusable(false);
                class1.setFocusable(false);
                gender.setFocusable(false);
                email.setFocusable(false);


                pflname.setFocusable(false);
                state.setFocusable(false);
                add.setFocusable(false);
                number.setFocusable(false);
                email1.setFocusable(false);

                tname.setFocusable(false);
                state1.setFocusable(false);
                add1.setFocusable(false);
                num.setFocusable(false);
                email2.setFocusable(false);
                class_type = ss_class;


                s_fname = sname.getText().toString().trim();
                s_email = email.getText().toString().trim();
                s_gender = gender.getText().toString().trim();
                p_fname = pflname.getText().toString().trim();
                p_state = state.getText().toString().trim();
                p_address = add.getText().toString().trim();
                p_email = email1.getText().toString().trim();
                p_number = number.getText().toString().trim();
                t_fname = tname.getText().toString().trim();
                t_state = state1.getText().toString().trim();
                t_address = add1.getText().toString().trim();
                t_number = num.getText().toString().trim();
                t_email = email2.getText().toString().trim();


                dialog = ProgressDialog.show(ViewProfileActivity.this, "", "Uploading file...", true);
                new Thread(new Runnable() {
                    public void run() {
                        doFileUpload(mDetials123);
                    }
                }).start();

                edit.setVisibility(View.VISIBLE);
                save.setVisibility(View.GONE);
            }
        });


        if (NetConnection.isConnected(ViewProfileActivity.this)) {
            progressBar.setVisibility(View.VISIBLE);
            SignUpjsontask task = new SignUpjsontask();
            task.execute();

        } else {

            Toast.makeText(ViewProfileActivity.this, "Internet connection error", Toast.LENGTH_LONG).show();
        }


        //-----------------------------------start---------------------------------------------


        uploadButton = (Button) findViewById(R.id.uploadButton);
        messageText = (TextView) findViewById(R.id.messageText);
        btnselectpic = (Button) findViewById(R.id.button_selectpic);


        gallery = (Gallery) findViewById(R.id.gallery);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc().imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(
                this).defaultDisplayImageOptions(defaultOptions).memoryCache(
                new WeakMemoryCache());
        ImageLoaderConfiguration config = builder.build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);


        btnselectpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);


            }
        });
   /*     uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/


        //-------------------------------------end------------------------------------------------------

    }


    @Override
    public void onBackPressed() {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs

        Intent iop = new Intent(ViewProfileActivity.this, NavigationActivity.class);
        startActivity(iop);

        finishAffinity();

    }

    //-----------------------------------start---------------------------------------------

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                mDetials123 = new ArrayList<Uri>();
                img_array = new ArrayList<String>();
                mDetials123.add(selectedImage);

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                filepath = cursor.getString(columnIndex);
                cursor.close();

                File file = new File(filepath);

                length = file.length();

                length = length / (1024 * 1024);
                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.PROFILEIMAGE, filepath);
                editor.commit();
                messageText.setText(filepath);
                img_array.add(filepath);
                //  messageText.setText(img_array.size()+"");


                // final GalleryImageAdapter galleryImageAdapter= new GalleryImageAdapter(ImageUpload.this,imageLoader,img_array);
                // gallery.setAdapter(galleryImageAdapter);


            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }


    @SuppressWarnings("deprecation")
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private void doFileUpload(List<Uri> mDetials) {
        List<File> mFiles = new ArrayList<>();
        for (int i = 0; i < mDetials.size(); i++) {
            String mFileLocation = getPath(mDetials.get(i));
            File file = new File(mFileLocation);
            mFiles.add(file);
        }
        String urlString = "http://360educating.com/Android/process.php?action=profile_updates";
        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            List<FileBody> mFileBody = new ArrayList<>();
            for (int i = 0; i < mFiles.size(); i++) {
                FileBody bin1 = new FileBody(mFiles.get(i));
                mFileBody.add(bin1);
            }
            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();
            reqEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            for (int i = 0; i < mFileBody.size(); i++) {
                reqEntity.addPart("uploadedfile" + i, mFileBody.get(i));
            }


            reqEntity.addTextBody("size", "" + mDetials.size(), ContentType.TEXT_PLAIN);


            reqEntity.addTextBody("id", Name11, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("class", class_type, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("fname", s_fname, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("email",s_email , ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("gender", s_gender, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("p_fname", p_fname, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("state", p_state, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("address", p_address, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("email1", p_email, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("phno", p_number, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("t_fname", t_fname, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("state1", t_state, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("address1", t_address, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("email2", t_email, ContentType.TEXT_PLAIN);
            reqEntity.addTextBody("phno1", t_number, ContentType.TEXT_PLAIN);

            post.setEntity(reqEntity.build());
            HttpResponse response = client.execute(post);
            HttpEntity resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            if (resEntity != null) {
                Log.i("RESPONSE", response_str);
                dialog.dismiss();
                runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            // Toast.makeText(getApplicationContext(), response_str, Toast.LENGTH_LONG).show();

                            String Errormessage = "";
                            String u_class_id = "", UserRegisterID = "", u_fname = "", u_lname = "", u_emails = "", u_gend = "", u_class = "", u_pfname = "", u_plname = "", u_state = "", u_add = "",
                                    u_email1 = "", u_pno = "", u_state1 = "", u_add1 = "", u_email2 = "", u_pno1 = "", u_month = "", u_dt = "", u_year = "", u_img = "", t_lname = "", t_fname = "";


                            JSONObject js = new JSONObject(response_str);

                            if (js.has("id")) {
                                UserRegisterID = js.getString("id");
                                u_fname = js.getString("fname");
                                u_lname = js.getString("lname");
                                 u_emails = js.getString("email");

                                u_gend = js.getString("gender");
                                u_class = js.getString("class");

                                u_class_id = js.getString("class_id");
                                u_pfname = js.getString("p_fname");

                                u_plname = js.getString("p_lname");
                                u_state = js.getString("state");
                                u_add = js.getString("address");


                                u_email1 = js.getString("email1");
                                u_pno = js.getString("phno");

                                t_fname = js.getString("t_fname");
                                t_lname = js.getString("t_lname");

                                u_state1 = js.getString("state1");

                                u_add1 = js.getString("address1");
                                u_email2 = js.getString("email2");
                                u_pno1 = js.getString("phno1");

                                u_month = js.getString("month");
                                u_dt = js.getString("dt");
                                u_year = js.getString("year");


                                u_img = js.getString("img");


                                Errormessage = js.getString("result");
                                //user_mobile=js.getString("number");

                            } else if (js.has("result")) {
                                Errormessage = js.getString("result");

                            }


                            if (Errormessage.equals("successfully")) {

                                if (UserRegisterID != null) {
                                    if (!UserRegisterID.equals("")) {

                                        //   Toast.makeText(ViewProfileActivity.this, Errormessage, Toast.LENGTH_SHORT).show();

                                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                                        editor1.putString(AppsContants.CLASS_ID, u_class_id);
                                        editor1.commit();


                                        sname.setText(u_fname + " " + u_lname);
                                        dob.setText(u_dt + "" + u_month + "" + u_year);
                                        class1.setText(u_class);
                                        gender.setText(u_gend);
                                        email.setText(u_emails);


                                        pflname.setText(u_plname);
                                        state.setText(u_state);
                                        add.setText(u_add);
                                        number.setText(u_pno);
                                        email1.setText(u_email1);


                                        tname.setText(t_fname);
                                        state1.setText(u_state1);
                                        add1.setText(u_add1);
                                        num.setText(u_pno1);
                                        email2.setText(u_email2);

                                        Picasso.with(ViewProfileActivity.this).load(u_img).into(img);

                                        //Toast.makeText(ViewProfileActivity.this, "Your login has been "+Errormessage,Toast.LENGTH_LONG).show();


                                        ll11.setVisibility(View.GONE);


                                    }

                                }
                            } else {
                                Toast.makeText(ViewProfileActivity.this,Errormessage, Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception ex) {
            Log.e("Debug", "error: " + ex.getMessage(), ex);
            dialog.dismiss();
        }
    }


    //-------------------------------------end------------------------------------------------------


    public class SignUpjsontask extends AsyncTask<String, Void, String> {

        String Errormessage;
        String UserRegisterID, u_fname, u_lname, u_emails, u_gend, u_class, u_pfname, u_plname, u_state, u_add,
                u_email1, u_pno, u_state1, u_add1, u_email2, u_pno1, u_month, u_dt, u_year, u_img, t_lname, t_fname;
        boolean IsError = false;
        String result = "";
        String u_class_id = "";
        String u_pass;
        String u_lat;
        String u_lon;
        String u_name;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);
        }

        protected String doInBackground(String... arg0) {

            HttpClient htppclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(HttpUrlPath.urlPath + HttpUrlPath.actionUpdateProfile);
            try {
                List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();


                nameValuePair.add(new BasicNameValuePair("id", Name11));
                // nameValuePair.add(new BasicNameValuePair("lname",s_lname));
                // nameValuePair.add(new BasicNameValuePair("email",s_email));
                // nameValuePair.add(new BasicNameValuePair("password",s_password));
                //nameValuePair.add(new BasicNameValuePair("class",s_ss));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));


                HttpResponse response = htppclient.execute(httppost);
                String object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);
                JSONObject js = new JSONObject(object);

                if (js.has("id")) {
                    UserRegisterID = js.getString("id");
                    u_fname = js.getString("fname");
                    u_lname = js.getString("lname");
                    u_emails = js.getString("email");

                    u_gend = js.getString("gender");
                    u_class = js.getString("class");


                    u_class_id = js.getString("class_id");


                    u_pfname = js.getString("p_fname");

                    u_plname = js.getString("p_lname");
                    u_state = js.getString("state");
                    u_add = js.getString("address");


                    u_email1 = js.getString("email1");
                    u_pno = js.getString("phno");

                    t_fname = js.getString("t_fname");
                    t_lname = js.getString("t_lname");

                    u_state1 = js.getString("state1");

                    u_add1 = js.getString("address1");
                    u_email2 = js.getString("email2");
                    u_pno1 = js.getString("phno1");

                    u_month = js.getString("month");
                    u_dt = js.getString("dt");
                    u_year = js.getString("year");


                    u_img = js.getString("img");


                    Errormessage = js.getString("result");
                    //user_mobile=js.getString("number");

                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }


        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);
            // loader.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            //Toast.makeText(Register.this,UserRegisterID+ "rohit "+Errormessage,Toast.LENGTH_LONG).show();
            if (!IsError) {

                if (Errormessage.equals("successfully")) {

                    if (UserRegisterID != null) {
                        if (!UserRegisterID.equals("")) {

                            //  Toast.makeText(ViewProfileActivity.this, Errormessage, Toast.LENGTH_SHORT).show();

                            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor1 = AppsContants.sharedpreferences.edit();
                            editor1.putString(AppsContants.CLASS_ID, u_class_id);
                            editor1.commit();


                            sname.setText(u_fname);
                            dob.setText(u_dt + "" + u_month + "" + u_year);
                            class1.setText(u_class);
                            gender.setText(u_gend);
                            email.setText(u_emails);


                            pflname.setText(u_pfname);
                            state.setText(u_state);
                            add.setText(u_add);
                            number.setText(u_pno);
                            email1.setText(u_email1);


                            tname.setText(t_fname);
                            state1.setText(u_state1);
                            add1.setText(u_add1);
                            num.setText(u_pno1);
                            email2.setText(u_email2);
                            if (!u_img.equals("")) {
                                Picasso.with(ViewProfileActivity.this).load(u_img).into(img);
                            }
                            //Toast.makeText(ViewProfileActivity.this, "Your login has been "+Errormessage,Toast.LENGTH_LONG).show();


                        }

                    }
                } else {
                    Toast.makeText(ViewProfileActivity.this, Errormessage, Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(ViewProfileActivity.this, "please try again...", Toast.LENGTH_SHORT).show();


            }
        }

    }


    public class SignUpjsontask11 extends AsyncTask<String, Void, String> {


        String Errormessage;
        String UserRegisterID;
        boolean IsError = false;
        String result = "";
        String p_user_login;
        String p_user_nicename;
        String p_display_name;
        String p_password;
        String s_latitude;
        String a1 = "", a2 = "", a3 = "", a4 = "", a5 = "";
        String object;

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            // progressBar.setVisibility(View.VISIBLE);
            // loader.setVisibility(View.VISIBLE);


        }

        protected String doInBackground(String... arg0) {

            HttpClient htppclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(HttpUrlPath.urlPath + HttpUrlPath.actionClasses);
            // HttpPost httppost=new HttpPost("http://maestrossoftware.com/WEBSOFT/education/Android/process.php?action=classes");
            try {

                HttpResponse response = htppclient.execute(httppost);
                object = EntityUtils.toString(response.getEntity());
                System.out.println("#####object registration=" + object);

                contactsstate1 = new JSONArray(object);

                for (int i = 0; i < contactsstate1.length(); i++) {
                    JSONObject c = contactsstate1.getJSONObject(i);
                    a1 = c.getString("id");
                    a2 = c.getString("classname");
                    categories.add(a2);
                    categories1.add(a1);


                }
               /* JSONObject js = new JSONObject(object);
                if (js.has("ID")) {
                    UserRegisterID = js.getString("ID");
                    p_user_login = js.getString("user_login");
                    p_user_nicename = js.getString("user_nicename");
                    p_display_name = js.getString("display_name");
                    Errormessage = js.getString("result");
                   // p_mobilenumber = js.getString("number");
                   // s_latitude=js.getString("latitude");
                    //s_longitude=js.getString("longitude");

                } else if (js.has("result")) {
                    Errormessage = js.getString("result");

                }*/

            } catch (Exception e) {
                IsError = true;
                Log.v("22", "22" + e.getMessage());
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result1) {
            super.onPostExecute(result1);

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ViewProfileActivity.this, android.R.layout.simple_spinner_item, categories);

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spsp1.setAdapter(dataAdapter);


        }


    }


}
